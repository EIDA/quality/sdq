#!/usr/bin/python3
import argparse
import datetime
from datetime import datetime
import io
import os
import re
import requests
import sys
import json
import warnings
from copy import deepcopy as copy_deepcopy

import matplotlib.dates as mdates
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import statistics

# Obspy 
import obspy
from obspy import UTCDateTime
from obspy.clients.fdsn import Client as Client_FDSN
from obspy.clients.filesystem.sds import Client as Client_SDS
from obspy.imaging.scripts.scan import Scanner, parse_file_to_dict
from obspy.core.inventory.inventory import read_inventory
from obspy.io.xseed import Parser
from obspy.signal import PPSD
from obspy.imaging.cm import pqlx

# My functions
import quality
#from PDF_plot_FV import plot_PDF_FV
#from Spect_plot import plot_spectrogram 
#from PSD_plot import plot_temporal


from Test_wfcatalog import SDS_Extension



def main():   
    p = argparse.ArgumentParser(description='Process network, station, and date inputs.')
    p.add_argument("-sl", "--station_list", action="store", type=argparse.FileType('r'), required=True, dest="net_sta_list_name", help='List of station to be processed. For example: IV MILN')
    p.add_argument("-cwl", "--columns_wl", nargs='+', action=required_length(2, 2), type=int, default=[0, 1], dest="columns_wl", help="Set the two columns to be read in the list of waveform file (see option '--waveform_list'), respectively corresponding to: station code and network code")
    p.add_argument("-t", "--test", action="store_true", dest="test_tf", default=False, help="Just test the reading of station code and network code from the list then exit without proceeding into calculation. Default: False")
    p.add_argument("-ymd", "--startdate", type=str, required=True, help="Start date for analysis in format 'YYYY-MM-DD'")
    p.add_argument("-i", "--interval", type=str, default="day", help="Interval for analysis: 'hour', 'day', or 'month'")
    p.add_argument("-nu", "--num_units", type=int, help="Number of units for the specified interval")

    opts = p.parse_args()
          
    f_name = opts.net_sta_list_name.name
    
    st_process = open(f_name, "r")
    
    input_list = []
    for line in st_process:
        line = line.strip()
        if not line:
            continue
        
        s_line = line.split() 
        (netcode, stacode) = [s_line[abs(opts.columns_wl[0])], s_line[abs(opts.columns_wl[1])]]
        
        if opts.test_tf:
            print(netcode, stacode)
        if opts.test_tf:
            continue
        try:
            quality_streem(netcode,stacode,opts.startdate, opts.interval, opts.num_units)   
        except Exception as e:
            print(e)
    
    
def quality_streem(netcode, stacode, startdate, interval, num_units): 
    # Verbose 
    start_time_code = datetime.now()
    verbose = True 
    file_name = True
    
    # PARAMETRI NETCODE-STACODE PER LA RICHIESTA E CREO CARTELLA RISULTATI

    # Parametri da trovare, define, passare
    sds_access = False 
    sds_path = '/home/varchetta/mnt/SDS_SCP38'

    if sds_access:
        os.system('sshfs sismo@192.168.2.38:/media/SISMONAS/BACKUP_ARCHIVIO_SEISCOMP/archive' + '   '+  sds_path)
        client = SDS_Extension(sds_path)
        #client_sds_plus = SDS_Extension(sds_path)
    else:
        client = Client_FDSN("INGV")


    client_station = Client_FDSN("INGV")
    loccode = '*' 
    
    starttime_str = startdate
    interval_str = interval
    num_units = num_units
    
    # Creo cartella risultati
    if not os.path.isdir('results'): 
        os.mkdir('results')
    
    out_path = os.path.join('results', "{}.{}".format(netcode, stacode)) 
    if not os.path.isdir(out_path): 
        os.mkdir(out_path)
    
    # STARTIME & ENDTIME PER LA RICHIESTA E CREO IL PATH PER IL SALVATAGGIO DEI PLOT    
    starttime_utc, endtime_utc = quality.calculate_start_end_time(starttime_str, interval_str, num_units)
    print(f"Starttime (UTCDateTime): {starttime_utc}")
    print(f"Endtime (UTCDateTime): {endtime_utc}")
    
    
    # Creo Path per il salvataggio plot
    data_dir = os.path.join('results', "{}.{}".format(netcode, stacode), str(starttime_utc).split('T')[0])
    if not os.path.isdir(data_dir): 
        os.mkdir(data_dir)
    
    plot_dir = os.path.join(data_dir,'PLOT')
    if not os.path.isdir(plot_dir): 
        os.mkdir(plot_dir)
    
    file_dir = os.path.join(data_dir,'FILE')
    if not os.path.isdir(file_dir): 
        os.mkdir(file_dir)
    
    pdf_file_dir = os.path.join(file_dir,'PDF')
    if not os.path.isdir(pdf_file_dir): 
        os.mkdir(pdf_file_dir)
        
    pdf_date_file_dir = os.path.join(pdf_file_dir,str(starttime_utc).split('T')[0])  
    if not os.path.isdir(pdf_date_file_dir): 
        os.mkdir(pdf_date_file_dir)
    
    pdf_year_folder_dir = os.path.join(pdf_file_dir, "year")  
    if not os.path.isdir(pdf_year_folder_dir): 
        os.mkdir(pdf_year_folder_dir)  
    
    pdf_year_file_dir = os.path.join(pdf_year_folder_dir,str(starttime_utc.year))  
    if not os.path.isdir(pdf_year_file_dir): 
        os.mkdir(pdf_year_file_dir)    

    # # INVENTORY STAZIONI
    # scarico le info stazioni
    inv = client_station.get_stations(network=netcode, station=stacode, starttime=starttime_utc, endtime=endtime_utc, level = "response")

 
    print(inv)


    ### ESPRESSIONI CHANNEL CODE¶
    # Definisci le espressioni regolari per i canali accelerometrici e velocimetrici
    accelerometer_pattern = re.compile(r'^H[NGL]')
    velocimeter_pattern = re.compile(r'^[EH]H')
    
    # Variabili per le prime due lettere dei codici dei canali accelerometrici e velocimetrici
    bicode_acc = None
    bicode_vel = None
    #sensitivity_value_acc = None
    
    
    # Itera sui canali nell'Inventory
    for nt in inv:
        for st in nt:
            for ch in st:
                # Verifica se il channel code corrisponde all'espressione regolare per accelerometro
                if accelerometer_pattern.match(ch.code) and not bicode_acc:
                    bicode_acc = ch.code[:2]  # Prende le prime due lettere
                # Verifica se il channel code corrisponde all'espressione regolare per velocimetro
                elif velocimeter_pattern.match(ch.code) and not bicode_vel:
                    bicode_vel = ch.code[:2]  # Prende le prime due lettere
    
    # Stampa i risultati
    print(f"Accelerometer Bicode: {bicode_acc}")
    print(f"Velocimeter Bicode: {bicode_vel}")
    
    
    
    ## Itera sui canali nell'Inventory
    #for nt in inv:
    #    for st in nt:
    #        for ch in st:
    #            # Verifica se il channel code corrisponde all'espressione regolare per accelerometro
    #            if accelerometer_pattern.match(ch.code) and not bicode_acc:
    #                bicode_acc = ch.code[:2]  # Prende le prime due lettere
    #            # Verifica se il channel code corrisponde all'espressione regolare per velocimetro
    #            elif velocimeter_pattern.match(ch.code) and not bicode_vel:
    #                bicode_vel = ch.code[:2]  # Prende le prime due lettere
    #             
    #             
    #            if ch.code[:2] == bicode_acc:
    #               sensitivity_value_acc = channel./home/fabiovarchetta/quality/SCP38/response.instrument_sensitivity.value
    #               break  # Esci dal ciclo una volta trovato il canale desiderato
    ##
    ## Stampa i risultati
    #print(f"Accelerometer Bicode: {bicode_acc}")
    #print(f"Velocimeter Bicode: {bicode_vel}")
    #print(f"Sensitivity_acc: {sensitivity_value_acc}")
                    
                    
    # Itera attraverso i canali nell'Inventory
    for nt in inv:
        for st in nt:
            for ch in st:
                # Verifica se il channel code corrisponde a bicode_acc
                if ch.code.startswith(bicode_acc):
                    sensor_info_acc = ch.sensor.description
                    data_logger_info_acc = ch.data_logger.description
                # Verifica se il channel code corrisponde a bicode_vel
                elif ch.code.startswith(bicode_vel):
                    sensor_info_vel = ch.sensor.description
                    data_logger_info_vel = ch.data_logger.description
    
    print(f"Accelerometer Sensor: {sensor_info_acc}")
    print(f"Accelerometer recorder: {data_logger_info_acc}")
    
    print(f"Velocimeter Sensor: {sensor_info_vel}")
    print(f"Velocimeter recorder: {data_logger_info_vel}")
    
    
    desired_info = ["network", "station", "channel", "percent_availability", "num_gaps", "sum_gaps", "max_gap",  "sample_rms", "sample_stdev"]    
    # Lista per memorizzare i dati
    data_list = []
    
    if sds_access:
        wf_dc, fnset = client.get_wfcatalog(starttime_utc, endtime_utc, netcode, stacode, csegments=False,)
        # Lista di informazioni desiderate per ogni canale
        # Itera attraverso ciascun canale
        for channel in wf_dc:
            start_time = UTCDateTime(channel.get("start_time"))
            end_time = UTCDateTime(channel.get("end_time"))
            # Verifica se il canale rientra nell'intervallo di tempo specificato
            #if start_time >= starttime_utc and end_time <= endtime_utc:
            channel_data = {info: channel.get(info, None) for info in desired_info}
            data_list.append(channel_data)
            
            ## Crea un DataFrame da lista di dizionari
            #df = pd.DataFrame(data_list)
    else:
        # SCARICO PARAMETRI QUALITA'¶
        # Scarico i file json dove prendere i parametri di qualità #
        url_wfcatalog = 'https://webservices.ingv.it/eidaws/wfcatalog/1/query?network='+ netcode + '&station='+ stacode + '&starttime=' + str(starttime_utc) + '&endtime=' + str(endtime_utc) + '&include=all&csegments=true'
        # Lettura file json
        response = requests.get(url_wfcatalog)
        # Lettura parametri Qualità da file Json
        if response.status_code == 200:
            # Carica il contenuto JSON dalla risposta
            data = response.json()
       
            print(json.dumps(data, indent=2))
        else:
            # Stampa un messaggio di errore se la richiesta non è andata a buon fine
            print(f"Errore nella richiesta. Status code: {response.status_code}")
    # Itera attraverso ciascun canale
        for channel in data:
            start_time = UTCDateTime(channel.get("start_time"))
            end_time = UTCDateTime(channel.get("end_time"))
            # Verifica se il canale rientra nell'intervallo di tempo specificato
            #if start_time >= starttime_utc and end_time <= endtime_utc:
            channel_data = {info: channel.get(info, None) for info in desired_info}
            data_list.append(channel_data)
    
    # Crea un DataFrame da lista di dizionari
    df = pd.DataFrame(data_list)



    # Condizione su max_gap
    df['max_gap'] = df['max_gap'].fillna(0)
    
    
    # Aggiungi questa riga prima della stampa del DataFrame
    df.insert(df.columns.get_loc('percent_availability') + 1, 'perc_gaps', df['percent_availability'].apply(lambda x: round(100 - x)))
    
    
    # lavoro con forme d'onda
    
    #curr_sta = [opts.net_code, opts.sta_code, opts.loc_code, opts.bic_code + '*']
    curr_sta_acc = [netcode, stacode, loccode, bicode_acc + '*']
    curr_sta_vel= [netcode, stacode, loccode, bicode_vel + '*']
    
    
    #c_st = client.get_waveforms(curr_sta[0], curr_sta[1], curr_sta[2], curr_sta[3], starttime, endtime)
    c_st_acc = client.get_waveforms( curr_sta_acc[0], curr_sta_acc[1], curr_sta_acc[2], curr_sta_acc[3], starttime_utc, endtime_utc)
    c_st_vel = client.get_waveforms( curr_sta_vel[0], curr_sta_vel[1], curr_sta_vel[2], curr_sta_vel[3], starttime_utc, endtime_utc)
    
    
    
    # CONVERSIONE TRACCE
    #accelerometro da sensitivity mentre velocimetro con deconvoluzione
    sig_acc = copy_deepcopy(c_st_acc)
    dt_acc = (sig_acc[2].stats.delta)
    fnyq_acc = 1 / (2 * dt_acc)
    
    if verbose: print( '----- SM -----')
    if verbose: print( 'dt:' + str(dt_acc))
    if verbose: print( 'Nyquist:' + str(fnyq_acc))
    
    sig_acc.detrend('linear')
    sig_acc.taper(max_percentage=0.02,type='cosine', side='both')
    sig_acc.filter('bandpass',freqmin=0.02,freqmax=40)
    
    #sig_acc.detrend('demean')
    
    sig_acc.integrate()
    sig_acc.detrend('linear')
    #sig_acc.detrend('demean')
    sig_acc.taper(max_percentage=0.02,type='cosine', side='both')
    sig_acc.differentiate()
    
    #Aggiungere info stazioni per ricavare il valore della sensitivity
    select_component = ['E','N','Z']
    sig_acc.attach_response(inv)
    
    sensitivity_value_e = sig_acc.select(component = 'E')[0].stats.response.instrument_sensitivity.value
    sensitivity_value_n = sig_acc.select(component = 'N')[0].stats.response.instrument_sensitivity.value
    sensitivity_value_z = sig_acc.select(component = 'Z')[0].stats.response.instrument_sensitivity.value
    
    
    
    if sensitivity_value_e != sensitivity_value_n or sensitivity_value_e != sensitivity_value_z or sensitivity_value_n != sensitivity_value_z:
        print("Error: Sensitivity values are not all equal.")
        sys.exit()
    else:
        for i in range(len(sig_acc)):
            sig_acc[i].data = sig_acc[i].data/sensitivity_value_e
    
    
    ## Deconvoluzione ##
    # # Definisco il delta e la frequenza di nyquis/home/fabiovarchetta/quality/SCP38/t  per il pre-filtraggio
    sig_vel = copy_deepcopy(c_st_vel)
    
    dt_vel = (sig_vel[2].stats.delta)
    fnyq_vel = 1 / (2 * dt_vel)
    
    if verbose: print( '----- BB -----')
    if verbose: print( 'dt:' + str(dt_vel))
    if verbose: print( 'Nyquist:' + str(fnyq_vel))
    
    sig_vel.detrend('linear')
    pre_filt_vel = [0.001, 1 /40, .8 * fnyq_vel, fnyq_vel]
    
    sig_vel.remove_response(inventory = inv, pre_filt = pre_filt_vel, taper=True, taper_fraction=0.02, output = "VEL", plot = False, water_level = 60)
    
    
    metadata_acc = {'sensitivity': sensitivity_value_e}
    
    # # Primi PLOT # # DAYPLOT 
    # Chiamata alla funzione con le tracce di accelerazione
    day_dir = os.path.join(plot_dir,'Day_plot')
    if not os.path.isdir(day_dir):
        os.mkdir(day_dir)
    
    # Crea figure
    
    #sig_acc.select(component = 'E').plot(type='dayplot', interval=60, one_tick_per_line=True, color=['k', 'r'])
    select_component = ['E','N','Z']
            
    ##mio originale
    #for i in select_component:
    #   sig_acc.select(component = i).plot(type='dayplot', title=f'{netcode}.{stacode}.{channel}{i} {starttime_str[:10]}', interval=60, one_tick_per_line=True, color=['k', 'r'],show = True)
        
    
    
    # Lista per memorizzare i nomi dei file delle immagini
    output_filenames = []
    
    img_day_acc = quality.dayplot_images(sig_acc,select_component, output_filenames, day_dir)
    img_day_vel = quality.dayplot_images(sig_vel,select_component, output_filenames, day_dir)
    
    
    
    ## GAP PLOT
    gap_dir = os.path.join(plot_dir, 'GAP')
    if not os.path.isdir(gap_dir): 
        os.mkdir(gap_dir)
    
    
    
    for component in select_component:
        fig = quality.stream2figure(c_st_acc.select(component = component));
        fn_img = os.path.join(gap_dir, f'{netcode}.{stacode}.{c_st_acc.select(component = component)[0].stats.channel}.Gaps.png')    
        dummy = fig.savefig(fn_img);
    
    
    for component in select_component:
        fig = quality.stream2figure(c_st_vel.select(component = component));
        fn_img = os.path.join(gap_dir, f'{netcode}.{stacode}.{c_st_vel.select(component = component)[0].stats.channel}.Gaps.png')    
        dummy = fig.savefig(fn_img);
    
    
    ## PLOT PDF e Spectogram
    pdf_dir = os.path.join(plot_dir,'PDF')
    if not os.path.isdir(pdf_dir):
        os.mkdir(pdf_dir)
    
    file_psd_img = os.path.join(pdf_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.png')
    
    # # file npz per le pdf 
    file_pdf_date = os.path.join(pdf_date_file_dir,f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.npz')
    file_psd_cum =  os.path.join(pdf_year_file_dir,f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.npz')
        
            
    spec_dir = os.path.join(plot_dir,'Spectogram')
    if not os.path.isdir(spec_dir):
        os.mkdir(spec_dir)
    
    file_spect_img = os.path.join(spec_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.Spect.png')   
    
    psd_temp_dir = os.path.join(plot_dir,'PSD_temporal')
    if not os.path.isdir(psd_temp_dir):
        os.mkdir(psd_temp_dir)
    
    file_psd_temp_img = os.path.join(psd_temp_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PSD_temp.png')
    
    
    result_list_acc = []
    result_list_vel = []
    result_header = ['NETCODE','STACODE','Channel',"PSD[0.01-50 Hz]", "PSD_st[0.01-50 Hz]","PSD[0.01-0.1 Hz]", "PSD_st[0.01-0.1 Hz]",\
    "PSD[0.1-1 Hz]","PSD_st[0.1-1 Hz]", "PSD[1-5 Hz]","PSD_st[1-5 Hz]" ,"PSD[5-10 Hz]", "PSD_st[5-10 Hz]", "PSD[10-20 Hz]",\
    "PSD_st[10-20 Hz]","PSD[20-50 Hz]","PSD_st[20-50 Hz]"]
    sta_results_test = []
    for component in select_component:
        ppsd_acc = quality.ppsd_calculate(netcode,stacode,c_st_acc.select(component= component), metadata_acc, bicode_acc,inv,pdf_dir,spec_dir,\
        psd_temp_dir,result_list_acc, file_psd_img,file_spect_img,file_psd_temp_img,pdf_date_file_dir,pdf_year_file_dir,file_pdf_date,file_psd_cum)
        
    
    for component in select_component:
        ppsd_vel = quality.ppsd_calculate(netcode,stacode,c_st_vel.select(component= component), metadata_acc, bicode_acc,inv,pdf_dir,spec_dir,\
        psd_temp_dir,result_list_vel, file_psd_img,file_spect_img,file_psd_temp_img,pdf_date_file_dir,pdf_year_file_dir,file_pdf_date,file_psd_cum)
    
        
    #Psd_frame_acc = pd.DataFrame(result_list_acc, columns=result_header)   
    #Psd_frame_vel = pd.DataFrame(result_list_vel, columns=result_header)   
    
    
    # Ordina il DataFrame in base a 'channel' con priorità su bicode_acc
    # Assegna un punteggio di priorità a ciascun canale
    df['priority'] = df['channel'].apply(lambda x: 0 if x.startswith(bicode_acc) else 1)
    
    # Ordina il DataFrame in base a 'priority' e 'channel'
    df_sorted = df.sort_values(by=['priority', 'channel']).drop(columns='priority')
    
    # Stampa il DataFrame ordinato
    print(df_sorted)
    
    
    qc_metrics = df_sorted.to_numpy().tolist()
    
    
    ## Parametri RMS
    rms_all_acc_E = []
    rms_all_acc_N = []
    rms_all_acc_Z = []
    #rms_all_acc_E = RMS(c_st_acc.select(component = 'E'))
    
    rms_all_vel_E = []
    rms_all_vel_N = []
    rms_all_vel_Z = []
    
    for component in select_component:
        if component == 'E':
            rms_all_acc_E.append(quality.RMS_FV(c_st_acc.select(component = component)))
            rms_all_vel_E.append(quality.RMS_FV(c_st_vel.select(component = component)))
            print('Acc_E', rms_all_acc_E)
            print('Vel_E', rms_all_vel_E)
        if component == 'N':
            rms_all_acc_N.append(quality.RMS_FV(c_st_acc.select(component = component)))
            rms_all_vel_N.append(quality.RMS_FV(c_st_vel.select(component = component)))
            print('Acc_N', rms_all_acc_E)
            print('Vel_N', rms_all_vel_E)
                
        if component == 'Z':
            rms_all_acc_Z.append(quality.RMS_FV(c_st_acc.select(component = component)))
            rms_all_vel_Z.append(quality.RMS_FV(c_st_vel.select(component = component)))
            print('Acc_Z', rms_all_acc_Z)
            print('Vel_Z', rms_all_vel_Z)
            
    #####
    #row_lists = df_sorted.to_numpy().tolist()
    #out_string = [','.join(map(str, row)) for row in row_lists]
    
    
    
    #quality_param_tests = row_lists[0]+rms_all_acc_E[0] + psd_row_lists[0][3:]
    
    #table_results = pd.DataFrame([quality_param_tests], columns=table_header)
    
    
    rms_all_acc_E = rms_all_acc_E[0]
    rms_all_acc_N = rms_all_acc_N[0]
    rms_all_acc_Z = rms_all_acc_Z[0]
    
    print(rms_all_acc_E)
    print(rms_all_acc_N)
    print(rms_all_acc_Z)
    
    rms_all_vel_E = rms_all_vel_E[0]
    rms_all_vel_N = rms_all_vel_N[0]
    rms_all_vel_Z = rms_all_vel_Z[0]
    
    
    qc_metrics_acc_E = qc_metrics[0][3:]
    qc_metrics_acc_N = qc_metrics[1][3:]
    qc_metrics_acc_Z = qc_metrics[2][3:]
                                  
    qc_metrics_vel_E = qc_metrics[3][3:]
    qc_metrics_vel_N = qc_metrics[4][3:]
    qc_metrics_vel_Z = qc_metrics[5][3:]
    
    
    psd_row_acc_lists_E = result_list_acc[0][3:]
    psd_row_acc_lists_N = result_list_acc[1][3:]
    psd_row_acc_lists_Z = result_list_acc[2][3:]
                          
    psd_row_vel_lists_E = result_list_vel[0][3:]
    psd_row_vel_lists_N = result_list_vel[1][3:]
    psd_row_vel_lists_Z = result_list_vel[2][3:]
    
    qc_total_acc_E = qc_metrics_acc_E + rms_all_acc_E + psd_row_acc_lists_E
    qc_total_acc_N = qc_metrics_acc_N + rms_all_acc_N + psd_row_acc_lists_N
    qc_total_acc_Z = qc_metrics_acc_Z + rms_all_acc_Z + psd_row_acc_lists_Z
    
    qc_total_vel_E = qc_metrics_vel_E + rms_all_vel_E + psd_row_vel_lists_E
    qc_total_vel_N = qc_metrics_vel_N + rms_all_vel_N + psd_row_vel_lists_N
    qc_total_vel_Z = qc_metrics_vel_Z + rms_all_vel_Z + psd_row_vel_lists_Z
    
    
    qc_total_acc = {'E': 0, 'N': 0, 'Z': 0}
    qc_total_vel = {'E': 0, 'N': 0, 'Z': 0}
    for i in select_component:
        qc_total_acc[i] = eval('qc_metrics_acc_'+i) + eval('rms_all_acc_'+i) + eval('psd_row_acc_lists_'+i)
        qc_total_vel[i] = eval('qc_metrics_vel_'+i) + eval('rms_all_vel_'+i) + eval('psd_row_vel_lists_'+i)
    
    
    trans= 'NULL'
    
    table_header = ['NETCODE','STACODE','Channel','recorder','sensor','trans','day',\
    "perc_av","perc_gaps", "num_gaps","sum_gaps","max_gaps","quadratic_mean","standard_deviation",\
    "rms[0.01-50 Hz]","rms_st[0.01-50 Hz]","rms[0.01-0.1 Hz]","rms_st[0.01-0.1 Hz]","rms[0.1-1 Hz]","rms_st[0.1-1 Hz]","rms[1-5 Hz]",\
    "rms_st[1-5 Hz]" , "rms[5-10 Hz]","rms_st[5-10 Hz]","rms[10-20 Hz]","rms_st[10-20 Hz]","rms[20-50 Hz]","rms_st[20-50 Hz]",\
    "PSD[0.01-50 Hz]", "PSD_st[0.01-50 Hz]",  "PSD[0.01-0.1 Hz]",  "PSD_st[0.01-0.1 Hz]",  "PSD[0.1-1 Hz]","PSD_st[0.1-1 Hz]","PSD[1-5 Hz]",\
    "PSD_st[1-5 Hz]" , "PSD[5-10 Hz]","PSD_st[5-10 Hz]","PSD[10-20 Hz]","PSD_st[10-20 Hz]","PSD[20-50 Hz]","PSD_st[20-50 Hz]"]
    
       
    
    #header_string = ','.join(table_header)
        
    # Lista di liste contenenti i valori per ogni riga
    data_acc = []
    data_vel = []
    
    data_combined = []
    # Itera attraverso ogni riga del test
    for i in select_component:
        row_acc = [netcode, stacode, bicode_acc+i, sensor_info_acc, data_logger_info_acc, trans, starttime_utc.strftime('%Y-%m-%d')] + qc_total_acc[i]
        data_combined.append(row_acc)
    
    # Itera attraverso ogni riga del test per i dati di velocità
    for i in select_component:
        row_vel = [netcode, stacode, bicode_vel+i, sensor_info_vel, data_logger_info_vel, trans, starttime_utc.strftime('%Y-%m-%d')] + qc_total_vel[i]
        data_combined.append(row_vel)
    
    table_name = 'table_results_' + str(starttime_utc.year) + '.csv'        
    df_combined = pd.DataFrame(data_combined, columns = table_header)
        
        
    #df_combined.to_csv('output.csv', sep=';', index=False, )   
    
    df_combined.to_csv(table_name, sep=';', index=False, mode='a', header=not os.path.isfile(table_name))   
        
    end_time_code = datetime.now()
    
    
    print('START',start_time_code)  
    print('END',end_time_code)  
        
        
        
    lg_file = 'time_log.log'    
        
    str1 = start_time_code.strftime("START: %Y-%m-%dT%H:%M:%S") 
    str2 = end_time_code.strftime("STOP: %Y-%m-%dT%H:%M:%S")
        
    lg_file = open(lg_file,'a')
    txt = "*****\n\n%s\n%s\n\n*****\n" %(str1, str2)
    lg_file.write(txt)
    lg_file.close()

    ## 
    if sds_access:
        os.system('fusermount -u'+ '  ' +sds_path)








def required_length(nmin, nmax):
    class RequiredLength(argparse.Action):
        def __call__(self, parser, args, values, option_string=None):
            if not nmin <= len(values) <= nmax:
                msg = 'argument "{f}" requires between {nmin} and {nmax} arguments'.format(f=self.dest, nmin=nmin, nmax=nmax)
                raise argparse.ArgumentTypeError(msg)
            setattr(args, self.dest, values)
    return RequiredLength





if __name__ == "__main__":
    main()
 

import os
import json
import requests


import obspy
import obspy.clients.filesystem.sds as sds

from obspy import UTCDateTime as UTC
from obspy.clients.filesystem.sds import Client as Client_sds
from obspy.clients.fdsn import Client as Client_fdsn
from obspy.signal.quality_control import MSEEDMetadata 




#class SDS_Extension(sds.Client):
#    def get_wfcatalog_onfly(self,network=None, station=None, location=None, channel=None, starttime=None, endtime=None, csegments=False, format="json",\
#        granularity="day", include="sample", longestonly=True, minimumlength=3600.0,):
#        fnlist = self._get_filenames(network, station, location, channel,starttime, endtime)
#        wf_json = MSEEDMetadata(fnlist).get_json_meta()
#        wf_dict = json.loads(wf_json)
#        return wf_dict, wf_json, fnlist





class SDS_Extension(sds.Client):
    def get_wfcatalog(self, starttime, endtime, network=None, station=None, location=None, channel=None,csegments=False,\
        format="json", granularity="day", include="default",\
        longestonly=False, minimumlength=0.0,filename=None, **kwargs):
        
        network = "*" if network is None else network
        station = "*" if station is None else station
        location = "*" if location is None else location
        channel = "*" if channel is None else channel
        
        fnset = self._get_filenames(network, station, location, channel,starttime, endtime)

        nslc_set = set()
        for fn in fnset:
            i = "{}.{}.{}.{}".format(*fn.split("/")[-1].split(".")[0:4])
            nslc_set.update((i,))

        my_id = "test/id/pippo"
        my_prefix = "test/prefix:"

        wf_dc_list = []       
        for nslc in nslc_set:
            fn_nslc = [fn for fn in fnset if nslc in fn]
        
            wf_json = MSEEDMetadata(fn_nslc, id=my_id, prefix=my_prefix,add_c_segments=csegments).get_json_meta()
            wf_dict = json.loads(wf_json)
            wf_dc_list.append(wf_dict)
            
        return wf_dc_list, fnset
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
               
        




































#def get_wfcatalog_onfly(self,network=None, station=None, location=None, channel=None, starttime=None, endtime=None, csegments=False, format="json",\
#    granularity="day", include="sample", longestonly=True, minimumlength=3600.0,):
#    fnlist = self._get_filenames(network, station, location, channel,starttime, endtime)
#    wf_json = MSEEDMetadata(fnlist).get_json_meta()
#    wf_dict = json.loads(wf_json)
#    return wf_dict, wf_json, fnlist
    















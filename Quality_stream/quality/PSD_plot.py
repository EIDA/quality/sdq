def plot_temporal(self, period, color, legend=True, grid=True,
				  linestyle="-", marker=None, filename=None, show=True,
				  **temporal_restrictions):
	"""
	Plot the evolution of PSD value of one (or more) period bins over time.

	If a filename is specified the plot is saved to this file, otherwise
	a matplotlib figure is returned or shown.

	Additional keyword arguments are passed on to :meth:`_stack_selection`
	to restrict at which times PSD values are selected (e.g. to compare
	temporal evolution during a specific time span of each day).

	.. note::
		For example plots see the :ref:`Obspy Gallery <gallery>`.

	:type period: float (or list thereof)
	:param period: Period of PSD values to plot. The period bin with the
		central period that is closest to the specified value is selected.
		Multiple values can be specified in a list (``color`` option should
		then also be a list of color specifications, or left ``None``).
	:type color: matplotlib color specification (or list thereof)
	:param color: Color specification understood by :mod:`matplotlib` (or a
		list thereof in case of multiple periods to plot). ``None`` for
		default colors.
	:type grid: bool
	:param grid: Enable/disable grid in histogram plot.
	:type legend: bool
	:param legend: Enable/disable grid in histogram plot.
	:type linestyle: str
	:param linestyle: Linestyle for lines in the plot (see
		:func:`matplotlib.pyplot.plot`).
	:type marker: str
	:param marker: Marker for lines in the plot (see
		:func:`matplotlib.pyplot.plot`).
	:type filename: str
	:param filename: Name of output file
	:type show: bool
	:param show: Enable/disable immediately showing the plot.
	"""
	import matplotlib.pyplot as plt
# 	import obspy
	from obspy import UTCDateTime
	from obspy.imaging.util import _set_xaxis_obspy_dates
# 	import numpy as np

	try:
		len(period)
	except TypeError:
		periods = [period]
	else:
		periods = period

	if color is None:
		colors = [None] * len(periods)
	else:
		if len(periods) == 1:
			colors = [color]
		else:
			colors = color

	times = self._times_processed

	if temporal_restrictions:
		mask = ~self._stack_selection(**temporal_restrictions)
		times = [x for i, x in enumerate(times) if not mask[i]]
	else:
		mask = None

	fig, ax = plt.subplots()


	for period, color in zip(periods, colors):
		cur_color = color
		# extract psd values for given period
		psd_values, period_min, _, period_max = \
			self.extract_psd_values(period)

		# metto una soglia per evitare che spari giù le curve
		for i in range(len(psd_values)):
			if psd_values[i] < -1000:
				psd_values[i] =None
				
				
		if mask is not None:
			psd_values = [x for i, x in enumerate(psd_values)
						  if not mask[i]]
		# voglio sempre le label in  Hertz
		#label = "{:.2g}-{:.2g} [Hz]".format(1.0 / period_max, 1.0 / period_min)
		label = "{:.2g} [Hz]".format(1.0 / period)

		for i, (times_, psd_values) in enumerate(
				self._split_lists(times, psd_values)):
			# only label first line plotted for each period
			if i:
				label = None
			# older matplotlib raises when passing in `color=None`
			if color is None:
				if cur_color is None:
					color_kwargs = {}
				else:
					color_kwargs = {'color': cur_color}
			else:
				color_kwargs = {'color': color}
			times_ = [UTCDateTime(ns=t).matplotlib_date for t in times_]
			#times_ = [UTCDateTime(t).matplotlib_date for t in times_] quello vecchio
			#times_ = [UTCDateTime(t).matplotlib_date / 10**9 for t in times_]  #correzione
			line = ax.plot(times_, psd_values, label=label, ls=linestyle,
						   marker=marker, **color_kwargs)[0]
			# plot the next lines with the same color (we can't easily
			# determine the color beforehand if we rely on the color cycle,
			# i.e. when user doesn't specify colors explictly)
			cur_color = line.get_color()

	if legend:
		ax.legend()

	if grid:
		ax.grid()

	if self.special_handling is None:
		ax.set_ylabel('Amplitude [$m^2/s^4/Hz$] [dB]')
	else:
		ax.set_ylabel('PSD-Amplitude [$m^2/s^4/Hz$] [dB]')

	fig.autofmt_xdate()
	_set_xaxis_obspy_dates(ax)
	# set y limits
	ax.set_ylim(bottom=-200,top=-10)
	ax.set_title(self._get_plot_title())


	if filename is not None:
		plt.savefig(filename)
		plt.close()
	elif show:
		plt.draw()
		plt.show()
	else:
		plt.draw()
		return fig

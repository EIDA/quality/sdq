import io
import os
import re
import requests
import json
import warnings

import obspy
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
from obspy.imaging.scripts.scan import Scanner, parse_file_to_dict
from obspy.core.inventory.inventory import read_inventory
from obspy.io.xseed import Parser
from obspy.signal import PPSD
from obspy.imaging.cm import pqlx
from obspy.imaging.cm import obspy_sequential
from obspy.core.util import AttribDict




import numpy as np
import pandas as pd
import quality
import sys
import statistics
from copy import deepcopy as copy_deepcopy

import matplotlib.dates as mdates
import matplotlib.pylab as plt

from matplotlib.colors import LinearSegmentedColormap
from matplotlib.ticker import FormatStrFormatter
from matplotlib.patheffects import withStroke















import os
from numpy import genfromtxt

######################### import Peterson acc. curves ############################
main_path = os.getcwd()
HNM_P_name= 'HNM_PETERSON_dB_acc'
LNM_P_name= 'LNM_PETERSON_dB_acc'
path_HNM_P = os.path.join(main_path,HNM_P_name)
path_LNM_P = os.path.join(main_path,LNM_P_name)


AHNM_P = genfromtxt(path_HNM_P, delimiter=" ")
ALNM_P = genfromtxt(path_LNM_P, delimiter=" ")


def plot_PDF_FV(self, filename=None,show_coverage=True, show_histogram=True,show_staz_lim=False,
		 show_percentiles=False, percentiles=[0,100],
		 show_noise_models=True, grid=True, show=True,
		 max_percentage=None, period_lim=(0.01, 179), show_mode=False,
		 show_mean=False, cmap=obspy_sequential, cumulative=False,
		 cumulative_number_of_colors=20, xaxis_frequency=False,
		 show_earthquakes=None,period_psd=None, psd_l1=None, psd_l2=None):
	"""
	Plot the 2D histogram of the current PPSD.
	If a filename is specified the plot is saved to this file, otherwise
	a plot window is shown.

	.. note::
		For example plots see the :ref:`Obspy Gallery <gallery>`.

	:type filename: str, optional
	:param filename: Name of output file
	:type show_coverage: bool, optional
	:param show_coverage: Enable/disable second axes with representation of
			data coverage time intervals.
	:type show_percentiles: bool, optional
	:param show_percentiles: Enable/disable plotting of approximated
			percentiles. These are calculated from the binned histogram and
			are not the exact percentiles.
	:type show_histogram: bool, optional
	:param show_histogram: Enable/disable plotting of histogram. This
			can be set ``False`` e.g. to make a plot with only percentiles
			plotted. Defaults to ``True``.
	:type percentiles: list of ints
	:param percentiles: percentiles to show if plotting of percentiles is
			selected.
	:type show_noise_models: bool, optional
	:param show_noise_models: Enable/disable plotting of noise models.
	:type show_earthquakes: bool, optional
	:param show_earthquakes: Enable/disable plotting of earthquake models
		like in [ClintonHeaton2002]_ and [CauzziClinton2013]_. Disabled by
		default (``None``). Specify ranges (minimum and maximum) for
		magnitude and distance of earthquake models given as four floats,
		e.g. ``(0, 5, 0, 99)`` for magnitude 1.5 - 4.5 at a epicentral
		distance of 10 km. Note only 10, 100 and 3000 km distances and
		magnitudes 1.5 to 7.5 are available. Alternatively, a distance can
		be specified in last float of a tuple of three, e.g. ``(0, 5, 10)``
		for 10 km distance, or magnitude and distance can be specified in
		a tuple of two floats, e.g. ``(5.5, 10)`` for magnitude 5.5 at 10
		km distance.
	:type grid: bool, optional
	:param grid: Enable/disable grid in histogram plot.
	:type show: bool, optional
	:param show: Enable/disable immediately showing the plot. If
		``show=False``, then the matplotlib figure handle is returned.
	:type max_percentage: float, optional
	:param max_percentage: Maximum percentage to adjust the colormap. The
		default is 30% unless ``cumulative=True``, in which case this value
		is ignored.
	:type period_lim: tuple of 2 floats, optional
	:param period_lim: Period limits to show in histogram. When setting
		``xaxis_frequency=True``, this is expected to be frequency range in
		Hz.
	:type show_mode: bool, optional
	:param show_mode: Enable/disable plotting of mode psd values.
	:type show_mean: bool, optional
	:param show_mean: Enable/disable plotting of mean psd values.
	:type cmap: :class:`matplotlib.colors.Colormap`
	:param cmap: Colormap to use for the plot. To use the color map like in
		PQLX, [McNamara2004]_ use :const:`obspy.imaging.cm.pqlx`.
	:type cumulative: bool
	:param cumulative: Can be set to `True` to show a cumulative
		representation of the histogram, i.e. showing color coded for each
		frequency/amplitude bin at what percentage in time the value is
		not exceeded by the data (similar to the `percentile` option but
		continuously and color coded over the whole area). `max_percentage`
		is ignored when this option is specified.
	:type cumulative_number_of_colors: int
	:param cumulative_number_of_colors: Number of discrete color shades to
		use, `None` for a continuous colormap.
	:type xaxis_frequency: bool
	:param xaxis_frequency: If set to `True`, the x axis will be frequency
		in Hertz as opposed to the default of period in seconds.
	"""

	import matplotlib.pyplot as plt
	self._PPSD__check_histogram()
	fig = plt.figure()
	fig.ppsd = AttribDict()

	if show_coverage:
		ax = fig.add_axes([0.12, 0.3, 0.90, 0.6])
		ax2 = fig.add_axes([0.15, 0.17, 0.7, 0.04])
	else:
		ax = fig.add_subplot(111)

	if show_percentiles:
		# for every period look up the approximate place of the percentiles
		#for percentile in percentiles:
		for percentile in percentiles:
			periods, percentile_values = \
				self.get_percentile(percentile=percentile)
			if xaxis_frequency:
				xdata = 1.0 / periods
			else:
				xdata = periods
			ax.plot(xdata, percentile_values, color="black", zorder=8,linewidth=2,linestyle='dashed')

	if show_mode:
		periods, mode_ = self.get_mode()
		if xaxis_frequency:
			xdata = 1.0 / periods
		else:
			xdata = periods
		if cmap.name == "viridis":
			color = "0.8"
		else:
			color = "black"
		ax.plot(xdata, mode_, color=color, zorder=9)

	if show_mean:
		periods, mean_ = self.get_mean()
		if xaxis_frequency:
			xdata = 1.0 / periods
		else:
			xdata = periods
		if cmap.name == "viridis":
			color = "0.8"
		else:
			color = "black"
		ax.plot(xdata, mean_, color=color, zorder=9)

# di Default erano quelle di Peterson per velocimetro metto quelli di Cauzzi per accelerometro + Peterson per acc. da Marco Massa

	if show_noise_models:
		#for model in (AHNM, ALNM):
		#	periods = model[:,0]
		#	noise_model_C = model[:,1]
		#	if xaxis_frequency:
		#		xdata = 1.0 / periods
		#	else:
		#		xdata = periods
		#	ax.plot(xdata, noise_model_C, '0.4', linewidth=2, zorder=10)

		#if (self.channel.startswith('HN') or self.channel.startswith('HG')):
		#	AHNM_P = AHNM_P1
		#	ALNM_P = ALNM_P1
		#else:
		#	AHNM_P = AHNM_P2
		#	ALNM_P = ALNM_P2

		for model in (AHNM_P, ALNM_P):
			frequency = model[:,0]
			noise_model_P = model[:,1]
			if xaxis_frequency:
				xdata = frequency
			else:
				xdata = 1.0 / frequency
			ax.plot(xdata, noise_model_P, '0.4', linewidth=2, zorder=10,  linestyle='dashed')

		#ax.legend([noise_model_C, noise_model_P], ['Cauzzi et al. (2013)','Peterson et al. (1993)'], loc='lower left', frameon=False) <------ cancella???

	if show_staz_lim:
		xdata=1.0/period_psd
		#ax.plot(xdata, psd, '0.4', linewidth=2, color="black", zorder=10)
		ax.plot(xdata, psd_l1, '0.4', linewidth=2, zorder=10, color="black", linestyle='dashed')
		ax.plot(xdata, psd_l2, '0.4', linewidth=2, zorder=10, color="black", linestyle='dashed')

	if show_earthquakes is not None:
		if len(show_earthquakes) == 2:
			show_earthquakes = (show_earthquakes[0],
								show_earthquakes[0] + 0.1,
								show_earthquakes[1],
								show_earthquakes[1] + 1)
		if len(show_earthquakes) == 3:
			show_earthquakes += (show_earthquakes[-1] + 1, )
		min_mag, max_mag, min_dist, max_dist = show_earthquakes
		for key, data in earthquake_models.items():
			magnitude, distance = key
			frequencies, accelerations = data
			accelerations = np.array(accelerations)
			frequencies = np.array(frequencies)
			periods = 1.0 / frequencies
			# Eq.1 from Clinton and Cauzzi (2013) converts
			# power to density
			ydata = accelerations / (periods ** (-.5))
			ydata = 20 * np.log10(ydata / 2)
			if not (min_mag <= magnitude <= max_mag and
					min_dist <= distance <= max_dist and
					min(ydata) < self.db_bin_edges[-1]):
				continue
			xdata = periods
			if xaxis_frequency:
				xdata = frequencies
			ax.plot(xdata, ydata, '0.4', linewidth=2)
			leftpoint = np.argsort(xdata)[0]
			if not ydata[leftpoint] < self.db_bin_edges[-1]:
				continue
			ax.text(xdata[leftpoint],
					ydata[leftpoint],
					'M%.1f\n%dkm' % (magnitude, distance),
					ha='right', va='top',
					color='w', weight='bold', fontsize='x-small',
					path_effects=[withStroke(linewidth=3,
											 foreground='0.4')])

	if show_histogram:
		label = "[%]"
		if cumulative:
			label = "non-exceedance (cumulative) [%]"
			if max_percentage is not None:
				msg = ("Parameter 'max_percentage' is ignored when "
					   "'cumulative=True'.")
				warnings.warn(msg)
			max_percentage = 100
			if cumulative_number_of_colors is not None:
				cmap = LinearSegmentedColormap(
					name=cmap.name, segmentdata=cmap._segmentdata,
					N=cumulative_number_of_colors)
		elif max_percentage is None:
			# Set default only if cumulative is not True.
			max_percentage = 30

		fig.ppsd.cumulative = cumulative
		fig.ppsd.cmap = cmap
		fig.ppsd.label = label
		fig.ppsd.max_percentage = max_percentage
		fig.ppsd.grid = grid
		fig.ppsd.xaxis_frequency = xaxis_frequency
		if max_percentage is not None:
			color_limits = (0, max_percentage)
			fig.ppsd.color_limits = color_limits

		self._plot_histogram(fig=fig)

	ax.semilogx()
	if xaxis_frequency:
		ax.set_xlabel('Frequency [Hz]')
		ax.invert_xaxis()
	else:
		ax.set_xlabel('Period [s]')
	ax.set_xlim(period_lim)
	#ax.set_ylim(self.db_bin_edges[0], self.db_bin_edges[-1])
	ax.set_ylim(-180,-60)

	ax.set_ylabel('PSD-Amplitude [$m^2/s^4/Hz$] [dB]')

	ax.xaxis.set_major_formatter(FormatStrFormatter("%g"))
	ax.set_title(self._get_plot_title())

	if show_coverage:
		self._PPSD__plot_coverage(ax2)
		# emulating fig.autofmt_xdate():
		for label in ax2.get_xticklabels():
			label.set_ha("right")
			label.set_rotation(30)

	# Catch underflow warnings due to plotting on log-scale.
	with np.errstate(all="ignore"):
		if filename is not None:
			plt.savefig(filename)
			plt.close()
		elif show:
			plt.draw()
			plt.show()
		else:
			plt.draw()
			return fig



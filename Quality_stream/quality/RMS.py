def RMS(fname):
	
	from obspy import read
	import numpy as np
	
	tr_file = read(fname)
	tr_orig=tr_file.copy()
	number_of_samples = sum(tr.stats.npts for tr in tr_file.traces)
	npts = float(number_of_samples)

	# 0.01-50 Hz
	tr0 = tr_file.filter('bandpass', freqmin=0.01, freqmax=50, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_0 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr0.traces) / npts)
	sample_stdev_0 = np.sqrt(sum(((abs(tr.data) - sample_rms_0 ) ** 2).sum() for tr in tr0.traces) / npts)

	# 0.01-0.1 Hz
	tr_file=tr_orig.copy()
	tr1 = tr_file.filter('bandpass', freqmin=0.01, freqmax=0.1, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_1 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr1.traces) / npts)
	sample_stdev_1 = np.sqrt(sum(((abs(tr.data) - sample_rms_1 ) ** 2).sum() for tr in tr1.traces) / npts)

	# 0.1-1 Hz
	tr_file=tr_orig.copy()
	tr2 = tr_file.filter('bandpass', freqmin=0.1, freqmax=1, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_2 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr2.traces) / npts)
	sample_stdev_2 = np.sqrt(sum(((abs(tr.data) - sample_rms_2 ) ** 2).sum() for tr in tr2.traces) / npts)

	# 1-5 Hz
	tr_file=tr_orig.copy()
	tr3 = tr_file.filter('bandpass', freqmin=1, freqmax=5, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_3 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr3.traces) / npts)
	sample_stdev_3 = np.sqrt(sum(((abs(tr.data) - sample_rms_3 ) ** 2).sum() for tr in tr3.traces) / npts)

	# 5-10 Hz
	tr_file=tr_orig.copy()
	tr4 = tr_file.filter('bandpass', freqmin=5, freqmax=10, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_4 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr4.traces) / npts)
	sample_stdev_4 = np.sqrt(sum(((abs(tr.data) - sample_rms_4 ) ** 2).sum() for tr in tr4.traces) / npts)

	# 10-20 Hz
	tr_file=tr_orig.copy()
	tr5 = tr_file.filter('bandpass', freqmin=10, freqmax=20, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_5 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr5.traces) / npts)
	sample_stdev_5 = np.sqrt(sum(((abs(tr.data) - sample_rms_5 ) ** 2).sum() for tr in tr5.traces) / npts)

	# 20-50 Hz
	tr_file=tr_orig.copy()
	tr6 = tr_file.filter('bandpass', freqmin=20, freqmax=50, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_6 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr6.traces) / npts)
	sample_stdev_6 = np.sqrt(sum(((abs(tr.data) - sample_rms_6 ) ** 2).sum() for tr in tr6.traces) / npts)

	rms_all = [sample_rms_0,sample_stdev_0,sample_rms_1,sample_stdev_1,sample_rms_2,sample_stdev_2,\
				sample_rms_3,sample_stdev_3,sample_rms_4,sample_stdev_4,sample_rms_5,sample_stdev_5,\
				sample_rms_6,sample_stdev_6]

	return rms_all

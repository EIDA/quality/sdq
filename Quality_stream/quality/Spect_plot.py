# import obspy
from obspy.imaging.util import _set_xaxis_obspy_dates
# from obspy.imaging.cm import obspy_sequential
import numpy as np
import matplotlib.pyplot as plt
######################################################################################
# cambio scala di colore, default era cmap=obspy_sequential
# https://docs.obspy.org/packages/autogen/obspy.imaging.cm.html
from matplotlib import cm
from matplotlib.colors import ListedColormap#, LinearSegmentedColormap
from obspy.imaging.cm import pqlx
pqlx2 = cm.get_cmap(pqlx, 256)
newcolors = pqlx(np.linspace(0, 1, 256))
newcolors = newcolors[50:, :]
newcmp = ListedColormap(newcolors)
#######################################################################################

def plot_spectrogram(self, cmap=newcmp, clim=None, grid=True,filename=None, show=True):

    """
    Plot the temporal evolution of the PSD in a spectrogram-like plot.
    .. note::
    For example plots see the :ref:`Obspy Gallery <gallery>`.

    :type cmap: :class:`matplotlib.colors.Colormap`
    :param cmap: Specify a custom colormap instance. If not specified, then
        the default ObsPy sequential colormap is used.
    :type clim: list
    :param clim: Minimum/maximum dB values for lower/upper end of colormap.
        Specified as type ``float`` or ``None`` for no clipping on one end
        of the scale (e.g. ``clim=[-150, None]`` for a lower limit of
        ``-150`` dB and no clipping on upper end).
    :type grid: bool
    :param grid: Enable/disable grid in histogram plot.
    :type filename: str
    :param filename: Name of output file
    :type show: bool
    :param show: Enable/disable immediately showing the plot.
    """

    fig, ax = plt.subplots()
    quadmeshes = []
    yedges = 1.0 / self.period_xedges

    for times, psds in self._get_gapless_psd():
        xedges = [t.matplotlib_date for t in times] + \
                [(times[-1] + self.step).matplotlib_date]
        meshgrid_x, meshgrid_y = np.meshgrid(xedges, yedges)
        data = np.array(psds).T

        quadmesh = ax.pcolormesh(meshgrid_x, meshgrid_y, data, cmap=cmap,
                                     zorder=-1)
        quadmeshes.append(quadmesh)

    if clim is None:
        cmin = min(qm.get_clim()[0] for qm in quadmeshes)
        cmax = max(qm.get_clim()[1] for qm in quadmeshes)
        clim = (cmin, cmax)

    for quadmesh in quadmeshes:
        quadmesh.set_clim(*clim)

    cb = plt.colorbar(quadmesh, ax=ax)

    if grid:
        ax.grid()

    #if self.special_handling is None:
    cb.ax.set_ylabel('Amplitude [$m^2/s^4/Hz$] [dB]')
    #else:
        #cb.ax.set_ylabel('Amplitude [dB]')
    ax.set_ylabel('Frequency [Hz]')

    fig.autofmt_xdate()
    _set_xaxis_obspy_dates(ax)

    ax.set_yscale("log")
    ax.set_xlim(self.times_processed[0].matplotlib_date,(self.times_processed[-1] + self.step).matplotlib_date)
    #ax.set_ylim(yedges[0], yedges[-1])
    ax.set_ylim(50, 0.012)
    try:
        ax.set_facecolor('0.8')
    # mpl <2 has different API for setting Axes background color
    except AttributeError:
        ax.set_axis_bgcolor('0.8')

    fig.tight_layout()
    ax.set_title(self._get_plot_title())
    plt.subplots_adjust(top=0.9)

    if filename is not None:
         plt.savefig(filename)
         plt.close()
    elif show:
        plt.draw()
        plt.show()
    else:
        plt.draw()
        return fig

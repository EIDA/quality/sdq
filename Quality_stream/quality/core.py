
# This is a mini module for quality ...

# More documenation to follow ...

import io
import os
import re

import obspy
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
from obspy.imaging.scripts.scan import Scanner, parse_file_to_dict
from obspy.core.inventory.inventory import read_inventory
from obspy.io.xseed import Parser
from obspy.signal import PPSD
from obspy.imaging.cm import pqlx
from obspy.imaging.cm import obspy_sequential
from obspy.core.util import AttribDict



import numpy as np
from numpy import genfromtxt

import pandas as pd
import quality
import sys
import statistics
import warnings

from copy import deepcopy as copy_deepcopy

import matplotlib.dates as mdates
import matplotlib.pylab as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.ticker import FormatStrFormatter
from matplotlib.patheffects import withStroke

#from PDF_plot_FV import plot_PDF_FV
#from Spect_plot import plot_spectrogram	




#LAVORO CON IL DATETIME PER LA RICHIESTA
def calculate_start_end_time(starttime, interval, num_units=1):
    from datetime import datetime, timedelta
    
    # Converti la stringa starttime in un oggetto UTCDateTime
    start_utc = UTCDateTime(starttime)

    # Calcola l'endtime in base all'intervallo specificato e al numero di unità
    if interval == 'hour':
        end_utc = start_utc + timedelta(hours=num_units)
    elif interval == 'day':
        end_utc = start_utc + timedelta(days=num_units)
    elif interval == 'month':
        # Aggiungi un mese alla volta
        end_utc = start_utc + timedelta(days=30 * num_units)
    else:
        # Se l'intervallo specificato non è uno tra 'hour', 'day' o 'month', restituisci None
        return None

    # Restituisci starttime e endtime come oggetti UTCDateTime
    return start_utc, end_utc

def calculate_start_end_time_v2(starttime, interval, num_units=1):

    D_INTERVAL_TRANSLATE = {"hour": 60*60, "day": 24*60*60,"month": 30*24*60*60}

    if integral in keys(D_INTERVAL_TRANSLATE):
        interval_s = D_INTERVAL_TRANSLATE["interval"] * num_units
    else:
        interval_s = interval * num_units

    t1 = starttime 
    t2 = t1 + interval_s

    return t1, t2

def buffer2figure(buffer):
    scanner = Scanner(verbose=True)

    scanner.counter = parse_file_to_dict(
        scanner.data,
        scanner.samp_int,
        buffer,
        scanner.counter,
        format=None,
        verbose=False,
        quiet=False,
        ignore_links=False,
    )
    
    fig = scanner.plot(show=False);
    
    return fig

def stream2figure(st):
    buffer = io.BytesIO()
    st.write(buffer, format='MSEED')
    buffer.seek(0)
    
    return buffer2figure(buffer)


def dayplot_images(sig_data, select_component, output_filenames, day_dir):
    netcode = sig_data[0].stats.network
    stacode = sig_data[0].stats.station
    channel = sig_data[0].stats.channel[:2]
    starttime_str = str(sig_data[0].stats.starttime)

    # Itera attraverso le componenti
    for i in select_component:
        # Crea il titolo per l'immagine
        title = f'{netcode}.{stacode}.{channel}{i} {starttime_str[:10]}'
        
        # Crea il plot
        fig = sig_data.select(component=i).plot(type='dayplot', title=title, interval=60, one_tick_per_line=True, color=['k', 'r'], show=False)
        
        # Aggiungi l'immagine alla lista dei nomi dei file delle immagini
        output_filename = os.path.join(day_dir, f'{netcode}.{stacode}.{channel}{i}.{starttime_str[:10]}.dayPlot.png')
        output_filenames.append(output_filename)
        
        # Salva l'immagine
        fig.savefig(output_filename)
        fig.clear()
        # Chiudi la figura per liberare la memoria
        plt.close(fig)


######################### import Peterson acc. curves ############################
main_path = os.getcwd()
HNM_P_name= 'HNM_PETERSON_dB_acc'
LNM_P_name= 'LNM_PETERSON_dB_acc'
path_HNM_P = os.path.join(main_path,HNM_P_name)
path_LNM_P = os.path.join(main_path,LNM_P_name)


AHNM_P = genfromtxt(path_HNM_P, delimiter=" ")
ALNM_P = genfromtxt(path_LNM_P, delimiter=" ")


def plot_PDF_FV(self, filename=None,show_coverage=True, show_histogram=True,show_staz_lim=False,
		 show_percentiles=False, percentiles=[0,100],
		 show_noise_models=True, grid=True, show=True,
		 max_percentage=None, period_lim=(0.01, 179), show_mode=False,
		 show_mean=False, cmap=obspy_sequential, cumulative=False,
		 cumulative_number_of_colors=20, xaxis_frequency=False,
		 show_earthquakes=None,period_psd=None, psd_l1=None, psd_l2=None):
	"""
	Plot the 2D histogram of the current PPSD.
	If a filename is specified the plot is saved to this file, otherwise
	a plot window is shown.

	.. note::
		For example plots see the :ref:`Obspy Gallery <gallery>`.

	:type filename: str, optional
	:param filename: Name of output file
	:type show_coverage: bool, optional
	:param show_coverage: Enable/disable second axes with representation of
			data coverage time intervals.
	:type show_percentiles: bool, optional
	:param show_percentiles: Enable/disable plotting of approximated
			percentiles. These are calculated from the binned histogram and
			are not the exact percentiles.
	:type show_histogram: bool, optional
	:param show_histogram: Enable/disable plotting of histogram. This
			can be set ``False`` e.g. to make a plot with only percentiles
			plotted. Defaults to ``True``.
	:type percentiles: list of ints
	:param percentiles: percentiles to show if plotting of percentiles is
			selected.
	:type show_noise_models: bool, optional
	:param show_noise_models: Enable/disable plotting of noise models.
	:type show_earthquakes: bool, optional
	:param show_earthquakes: Enable/disable plotting of earthquake models
		like in [ClintonHeaton2002]_ and [CauzziClinton2013]_. Disabled by
		default (``None``). Specify ranges (minimum and maximum) for
		magnitude and distance of earthquake models given as four floats,
		e.g. ``(0, 5, 0, 99)`` for magnitude 1.5 - 4.5 at a epicentral
		distance of 10 km. Note only 10, 100 and 3000 km distances and
		magnitudes 1.5 to 7.5 are available. Alternatively, a distance can
		be specified in last float of a tuple of three, e.g. ``(0, 5, 10)``
		for 10 km distance, or magnitude and distance can be specified in
		a tuple of two floats, e.g. ``(5.5, 10)`` for magnitude 5.5 at 10
		km distance.
	:type grid: bool, optional
	:param grid: Enable/disable grid in histogram plot.
	:type show: bool, optional
	:param show: Enable/disable immediately showing the plot. If
		``show=False``, then the matplotlib figure handle is returned.
	:type max_percentage: float, optional
	:param max_percentage: Maximum percentage to adjust the colormap. The
		default is 30% unless ``cumulative=True``, in which case this value
		is ignored.
	:type period_lim: tuple of 2 floats, optional
	:param period_lim: Period limits to show in histogram. When setting
		``xaxis_frequency=True``, this is expected to be frequency range in
		Hz.
	:type show_mode: bool, optional
	:param show_mode: Enable/disable plotting of mode psd values.
	:type show_mean: bool, optional
	:param show_mean: Enable/disable plotting of mean psd values.
	:type cmap: :class:`matplotlib.colors.Colormap`
	:param cmap: Colormap to use for the plot. To use the color map like in
		PQLX, [McNamara2004]_ use :const:`obspy.imaging.cm.pqlx`.
	:type cumulative: bool
	:param cumulative: Can be set to `True` to show a cumulative
		representation of the histogram, i.e. showing color coded for each
		frequency/amplitude bin at what percentage in time the value is
		not exceeded by the data (similar to the `percentile` option but
		continuously and color coded over the whole area). `max_percentage`
		is ignored when this option is specified.
	:type cumulative_number_of_colors: int
	:param cumulative_number_of_colors: Number of discrete color shades to
		use, `None` for a continuous colormap.
	:type xaxis_frequency: bool
	:param xaxis_frequency: If set to `True`, the x axis will be frequency
		in Hertz as opposed to the default of period in seconds.
	"""

	import matplotlib.pyplot as plt
	self._PPSD__check_histogram()
	fig = plt.figure()
	fig.ppsd = AttribDict()

	if show_coverage:
		ax = fig.add_axes([0.12, 0.3, 0.90, 0.6])
		ax2 = fig.add_axes([0.15, 0.17, 0.7, 0.04])
	else:
		ax = fig.add_subplot(111)

	if show_percentiles:
		# for every period look up the approximate place of the percentiles
		#for percentile in percentiles:
		for percentile in percentiles:
			periods, percentile_values = \
				self.get_percentile(percentile=percentile)
			if xaxis_frequency:
				xdata = 1.0 / periods
			else:
				xdata = periods
			ax.plot(xdata, percentile_values, color="black", zorder=8,linewidth=2,linestyle='dashed')

	if show_mode:
		periods, mode_ = self.get_mode()
		if xaxis_frequency:
			xdata = 1.0 / periods
		else:
			xdata = periods
		if cmap.name == "viridis":
			color = "0.8"
		else:
			color = "black"
		ax.plot(xdata, mode_, color=color, zorder=9)

	if show_mean:
		periods, mean_ = self.get_mean()
		if xaxis_frequency:
			xdata = 1.0 / periods
		else:
			xdata = periods
		if cmap.name == "viridis":
			color = "0.8"
		else:
			color = "black"
		ax.plot(xdata, mean_, color=color, zorder=9)

# di Default erano quelle di Peterson per velocimetro metto quelli di Cauzzi per accelerometro + Peterson per acc. da Marco Massa

	if show_noise_models:
		#for model in (AHNM, ALNM):
		#	periods = model[:,0]
		#	noise_model_C = model[:,1]
		#	if xaxis_frequency:
		#		xdata = 1.0 / periods
		#	else:
		#		xdata = periods
		#	ax.plot(xdata, noise_model_C, '0.4', linewidth=2, zorder=10)

		#if (self.channel.startswith('HN') or self.channel.startswith('HG')):
		#	AHNM_P = AHNM_P1
		#	ALNM_P = ALNM_P1
		#else:
		#	AHNM_P = AHNM_P2
		#	ALNM_P = ALNM_P2

		for model in (AHNM_P, ALNM_P):
			frequency = model[:,0]
			noise_model_P = model[:,1]
			if xaxis_frequency:
				xdata = frequency
			else:
				xdata = 1.0 / frequency
			ax.plot(xdata, noise_model_P, '0.4', linewidth=2, zorder=10,  linestyle='dashed')

		#ax.legend([noise_model_C, noise_model_P], ['Cauzzi et al. (2013)','Peterson et al. (1993)'], loc='lower left', frameon=False) <------ cancella???

	if show_staz_lim:
		xdata=1.0/period_psd
		#ax.plot(xdata, psd, '0.4', linewidth=2, color="black", zorder=10)
		ax.plot(xdata, psd_l1, '0.4', linewidth=2, zorder=10, color="black", linestyle='dashed')
		ax.plot(xdata, psd_l2, '0.4', linewidth=2, zorder=10, color="black", linestyle='dashed')

	if show_earthquakes is not None:
		if len(show_earthquakes) == 2:
			show_earthquakes = (show_earthquakes[0],
								show_earthquakes[0] + 0.1,
								show_earthquakes[1],
								show_earthquakes[1] + 1)
		if len(show_earthquakes) == 3:
			show_earthquakes += (show_earthquakes[-1] + 1, )
		min_mag, max_mag, min_dist, max_dist = show_earthquakes
		for key, data in earthquake_models.items():
			magnitude, distance = key
			frequencies, accelerations = data
			accelerations = np.array(accelerations)
			frequencies = np.array(frequencies)
			periods = 1.0 / frequencies
			# Eq.1 from Clinton and Cauzzi (2013) converts
			# power to density
			ydata = accelerations / (periods ** (-.5))
			ydata = 20 * np.log10(ydata / 2)
			if not (min_mag <= magnitude <= max_mag and
					min_dist <= distance <= max_dist and
					min(ydata) < self.db_bin_edges[-1]):
				continue
			xdata = periods
			if xaxis_frequency:
				xdata = frequencies
			ax.plot(xdata, ydata, '0.4', linewidth=2)
			leftpoint = np.argsort(xdata)[0]
			if not ydata[leftpoint] < self.db_bin_edges[-1]:
				continue
			ax.text(xdata[leftpoint],
					ydata[leftpoint],
					'M%.1f\n%dkm' % (magnitude, distance),
					ha='right', va='top',
					color='w', weight='bold', fontsize='x-small',
					path_effects=[withStroke(linewidth=3,
											 foreground='0.4')])

	if show_histogram:
		label = "[%]"
		if cumulative:
			label = "non-exceedance (cumulative) [%]"
			if max_percentage is not None:
				msg = ("Parameter 'max_percentage' is ignored when "
					   "'cumulative=True'.")
				warnings.warn(msg)
			max_percentage = 100
			if cumulative_number_of_colors is not None:
				cmap = LinearSegmentedColormap(
					name=cmap.name, segmentdata=cmap._segmentdata,
					N=cumulative_number_of_colors)
		elif max_percentage is None:
			# Set default only if cumulative is not True.
			max_percentage = 30

		fig.ppsd.cumulative = cumulative
		fig.ppsd.cmap = cmap
		fig.ppsd.label = label
		fig.ppsd.max_percentage = max_percentage
		fig.ppsd.grid = grid
		fig.ppsd.xaxis_frequency = xaxis_frequency
		if max_percentage is not None:
			color_limits = (0, max_percentage)
			fig.ppsd.color_limits = color_limits

		self._plot_histogram(fig=fig)

	ax.semilogx()
	if xaxis_frequency:
		ax.set_xlabel('Frequency [Hz]')
		ax.invert_xaxis()
	else:
		ax.set_xlabel('Period [s]')
	ax.set_xlim(period_lim)
	#ax.set_ylim(self.db_bin_edges[0], self.db_bin_edges[-1])
	ax.set_ylim(-180,-60)

	ax.set_ylabel('PSD-Amplitude [$m^2/s^4/Hz$] [dB]')

	ax.xaxis.set_major_formatter(FormatStrFormatter("%g"))
	ax.set_title(self._get_plot_title())

	if show_coverage:
		self._PPSD__plot_coverage(ax2)
		# emulating fig.autofmt_xdate():
		for label in ax2.get_xticklabels():
			label.set_ha("right")
			label.set_rotation(30)

	# Catch underflow warnings due to plotting on log-scale.
	with np.errstate(all="ignore"):
		if filename is not None:
			plt.savefig(filename)
			plt.close()
		elif show:
			plt.draw()
			plt.show()
		else:
			plt.draw()
			return fig




# import obspy
from obspy.imaging.util import _set_xaxis_obspy_dates
# from obspy.imaging.cm import obspy_sequential

######################################################################################
# cambio scala di colore, default era cmap=obspy_sequential
# https://docs.obspy.org/packages/autogen/obspy.imaging.cm.html
from matplotlib import cm
from matplotlib.colors import ListedColormap#, LinearSegmentedColormap
from obspy.imaging.cm import pqlx
pqlx2 = cm.get_cmap(pqlx, 256)
newcolors = pqlx(np.linspace(0, 1, 256))
newcolors = newcolors[50:, :]
newcmp = ListedColormap(newcolors)
#######################################################################################

def plot_spectrogram(self, cmap=newcmp, clim=None, grid=True,filename=None, show=True):

    """
    Plot the temporal evolution of the PSD in a spectrogram-like plot.
    .. note::
    For example plots see the :ref:`Obspy Gallery <gallery>`.

    :type cmap: :class:`matplotlib.colors.Colormap`
    :param cmap: Specify a custom colormap instance. If not specified, then
        the default ObsPy sequential colormap is used.
    :type clim: list
    :param clim: Minimum/maximum dB values for lower/upper end of colormap.
        Specified as type ``float`` or ``None`` for no clipping on one end
        of the scale (e.g. ``clim=[-150, None]`` for a lower limit of
        ``-150`` dB and no clipping on upper end).
    :type grid: bool
    :param grid: Enable/disable grid in histogram plot.
    :type filename: str
    :param filename: Name of output file
    :type show: bool
    :param show: Enable/disable immediately showing the plot.
    """

    fig, ax = plt.subplots()
    quadmeshes = []
    yedges = 1.0 / self.period_xedges

    for times, psds in self._get_gapless_psd():
        xedges = [t.matplotlib_date for t in times] + \
                [(times[-1] + self.step).matplotlib_date]
        meshgrid_x, meshgrid_y = np.meshgrid(xedges, yedges)
        data = np.array(psds).T

        quadmesh = ax.pcolormesh(meshgrid_x, meshgrid_y, data, cmap=cmap,
                                     zorder=-1)
        quadmeshes.append(quadmesh)

    if clim is None:
        cmin = min(qm.get_clim()[0] for qm in quadmeshes)
        cmax = max(qm.get_clim()[1] for qm in quadmeshes)
        clim = (cmin, cmax)

    for quadmesh in quadmeshes:
        quadmesh.set_clim(*clim)

    cb = plt.colorbar(quadmesh, ax=ax)

    if grid:
        ax.grid()

    #if self.special_handling is None:
    cb.ax.set_ylabel('Amplitude [$m^2/s^4/Hz$] [dB]')
    #else:
        #cb.ax.set_ylabel('Amplitude [dB]')
    ax.set_ylabel('Frequency [Hz]')

    fig.autofmt_xdate()
    _set_xaxis_obspy_dates(ax)

    ax.set_yscale("log")
    ax.set_xlim(self.times_processed[0].matplotlib_date,(self.times_processed[-1] + self.step).matplotlib_date)
    #ax.set_ylim(yedges[0], yedges[-1])
    ax.set_ylim(50, 0.012)
    try:
        ax.set_facecolor('0.8')
    # mpl <2 has different API for setting Axes background color
    except AttributeError:
        ax.set_axis_bgcolor('0.8')

    fig.tight_layout()
    ax.set_title(self._get_plot_title())
    plt.subplots_adjust(top=0.9)

    if filename is not None:
         plt.savefig(filename)
         plt.close()
    elif show:
        plt.draw()
        plt.show()
    else:
        plt.draw()
        return fig


def plot_temporal(self, period, color, legend=True, grid=True,
				  linestyle="-", marker=None, filename=None, show=True,
				  **temporal_restrictions):
	"""
	Plot the evolution of PSD value of one (or more) period bins over time.

	If a filename is specified the plot is saved to this file, otherwise
	a matplotlib figure is returned or shown.

	Additional keyword arguments are passed on to :meth:`_stack_selection`
	to restrict at which times PSD values are selected (e.g. to compare
	temporal evolution during a specific time span of each day).

	.. note::
		For example plots see the :ref:`Obspy Gallery <gallery>`.

	:type period: float (or list thereof)
	:param period: Period of PSD values to plot. The period bin with the
		central period that is closest to the specified value is selected.
		Multiple values can be specified in a list (``color`` option should
		then also be a list of color specifications, or left ``None``).
	:type color: matplotlib color specification (or list thereof)
	:param color: Color specification understood by :mod:`matplotlib` (or a
		list thereof in case of multiple periods to plot). ``None`` for
		default colors.
	:type grid: bool
	:param grid: Enable/disable grid in histogram plot.
	:type legend: bool
	:param legend: Enable/disable grid in histogram plot.
	:type linestyle: str
	:param linestyle: Linestyle for lines in the plot (see
		:func:`matplotlib.pyplot.plot`).
	:type marker: str
	:param marker: Marker for lines in the plot (see
		:func:`matplotlib.pyplot.plot`).
	:type filename: str
	:param filename: Name of output file
	:type show: bool
	:param show: Enable/disable immediately showing the plot.
	"""
	import matplotlib.pyplot as plt
# 	import obspy
	from obspy import UTCDateTime
	from obspy.imaging.util import _set_xaxis_obspy_dates
# 	import numpy as np

	try:
		len(period)
	except TypeError:
		periods = [period]
	else:
		periods = period

	if color is None:
		colors = [None] * len(periods)
	else:
		if len(periods) == 1:
			colors = [color]
		else:
			colors = color

	times = self._times_processed

	if temporal_restrictions:
		mask = ~self._stack_selection(**temporal_restrictions)
		times = [x for i, x in enumerate(times) if not mask[i]]
	else:
		mask = None

	fig, ax = plt.subplots()


	for period, color in zip(periods, colors):
		cur_color = color
		# extract psd values for given period
		psd_values, period_min, _, period_max = \
			self.extract_psd_values(period)

		# metto una soglia per evitare che spari giù le curve
		for i in range(len(psd_values)):
			if psd_values[i] < -1000:
				psd_values[i] =None
				
				
		if mask is not None:
			psd_values = [x for i, x in enumerate(psd_values)
						  if not mask[i]]
		# voglio sempre le label in  Hertz
		#label = "{:.2g}-{:.2g} [Hz]".format(1.0 / period_max, 1.0 / period_min)
		label = "{:.2g} [Hz]".format(1.0 / period)

		for i, (times_, psd_values) in enumerate(
				self._split_lists(times, psd_values)):
			# only label first line plotted for each period
			if i:
				label = None
			# older matplotlib raises when passing in `color=None`
			if color is None:
				if cur_color is None:
					color_kwargs = {}
				else:
					color_kwargs = {'color': cur_color}
			else:
				color_kwargs = {'color': color}
			times_ = [UTCDateTime(ns=t).matplotlib_date for t in times_] # originale obspy
			#times_ = [UTCDateTime(t).matplotlib_date for t in times_] quello vecchio
			line = ax.plot(times_, psd_values, label=label, ls=linestyle,
						   marker=marker, **color_kwargs)[0]
			# plot the next lines with the same color (we can't easily
			# determine the color beforehand if we rely on the color cycle,
			# i.e. when user doesn't specify colors explictly)
			cur_color = line.get_color()

	if legend:
		ax.legend()

	if grid:
		ax.grid()

	if self.special_handling is None:
		ax.set_ylabel('Amplitude [$m^2/s^4/Hz$] [dB]')
	else:
		ax.set_ylabel('PSD-Amplitude [$m^2/s^4/Hz$] [dB]')

	fig.autofmt_xdate()
	_set_xaxis_obspy_dates(ax)
	# set y limits
	ax.set_ylim(bottom=-200,top=-10)
	ax.set_title(self._get_plot_title())


	if filename is not None:
		plt.savefig(filename)
		plt.close()
	elif show:
		plt.draw()
		plt.show()
	else:
		plt.draw()
		return fig


def ppsd_calculate(netcode,stacode, c_st_acc, metadata, bicode_acc,inv, pdf_dir,spec_dir,psd_temp_dir,result_list, file_psd_img,file_spect_img,file_psd_temp_img, pdf_date_file_dir,pdf_year_file_dir,file_pdf_date,file_psd_cum):
	if c_st_acc[0].stats.channel[:2] == bicode_acc:
		ppsd = PPSD(c_st_acc[0].stats, metadata, special_handling="ringlaser")
		condition_result = "IF"
	else:
		ppsd = PPSD(c_st_acc[0].stats, metadata=inv)
		condition_result = "ELSE"
		
		
	found = ppsd.add(c_st_acc)
	
	if found:
		file_pdf_date = os.path.join(pdf_date_file_dir,f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.npz')
		ppsd.save_npz(file_pdf_date)
		
	if found:
		file_psd_cum = os.path.join(pdf_year_file_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.npz')
		if os.path.isfile(file_psd_cum):
			try:
				ppsd.add_npz(file_psd_cum)
			except Exception as e:
				print("È cambiato qualche parametro rispetto alle tracce precedenti:", e)
				os.rename(file_psd_cum, os.path.join(os.path.dirname(file_psd_cum), 'old_' + os.path.basename(file_psd_cum)))
		else:
			try:
				# Qui salva il file se non esiste
				ppsd.save_npz(file_psd_cum)
			except Exception as e:
				print("Errore durante il salvataggio del file PSD cum:", e)

	        
	        
	        
	        
	        ##load PDF_day & PDF_cum
	        #psd_cum = PPSD.load_npz(file_psd_cum)
	        #ppsd = PPSD.load_npz(file_psd)
	
	        ##estrai limiti +-2sigma da PDF_cum
	        #periods = psd_cum.get_percentile(percentile=50)[0]
	        ##psd_m = psd_cum.get_percentile(percentile=50)[1]
	        #psd_l1 = psd_cum.get_percentile(percentile=2.28)[1] # corrispondono a +- 2sigma
	        #psd_l2 = psd_cum.get_percentile(percentile=97.72)[1]
	
	
	
	#load PDF_day & PDF_cum
	psd_cum = PPSD.load_npz(file_psd_cum)
	ppsd = PPSD.load_npz(file_pdf_date)
	
	
	#periods = ppsd.get_percentile(percentile=50)[0]
	periods = psd_cum.get_percentile(percentile=50)[0]
	psd_l1  = ppsd.get_percentile(percentile=2.28)[1] # corrispondono a +- 2sigma
	psd_l2  = ppsd.get_percentile(percentile=97.72)[1]

	
	
	
	
	if found:
		#file_psd_img = os.path.join(pdf_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.png')
		file_psd_img = os.path.join(pdf_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.png')
		with warnings.catch_warnings():
			warnings.simplefilter("ignore")
			#do something that raises a Warning
			plot_PDF_FV(self=ppsd,filename = file_psd_img, cmap=pqlx, period_lim=(0.01, 50), xaxis_frequency=True,show_staz_lim=True, period_psd=periods, psd_l1=psd_l1, psd_l2=psd_l2)
	else:
		if found:
			file_psd_img = os.path.join(pdf_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PDF.png')
			with warnings.catch_warnings():
				warnings.simplefilter("ignore")
				#do something that raises a Warning
				plot_PDF_FV(self=ppsd,filename = file_psd_img, cmap=pqlx, period_lim=(0.01, 50), xaxis_frequency=True)
	
	
	if found:
		#estrai mediana giornaliera
		periods = ppsd.get_percentile(percentile=50)[0] # la media è troppo influenzata da pochi dati
		psd_m = ppsd.get_percentile(percentile=50)[1]
		# calcola il valor medio della mediana in quell'intervallo di freq.
		PSD_0 = round(statistics.mean(psd_m[(periods >= 0.02) & (periods < 100)]), 2)     # 0.01-50 Hz	
		PSD_1 = round(statistics.mean(psd_m[(periods >= 10) & (periods < 100)]), 2)       # 0.01-0.1 Hz	
		PSD_2 = round(statistics.mean(psd_m[(periods >= 1) & (periods < 10)]), 2)         # 0.1-1 Hz	
		PSD_3 = round(statistics.mean(psd_m[(periods >= 0.2) & (periods < 1)]), 2)        # 1-5 Hz	
		PSD_4 = round(statistics.mean(psd_m[(periods >= 0.1) & (periods < 0.2)]), 2)      # 5-10 Hz	
		PSD_5 = round(statistics.mean(psd_m[(periods >= 0.05) & (periods < 0.1)]), 2)     # 10-20 Hz	
		PSD_6 = round(statistics.mean(psd_m[(periods >= 0.02) & (periods < 0.05)]), 2)    # 20-50 Hz
		# calcola standard deviation in quell'intervallo di freq.	
		PSD_0_st = round(statistics.stdev(psd_m[(periods >= 0.02) & (periods < 100)]), 2)     # 0.01-50 Hz
		PSD_1_st = round(statistics.stdev(psd_m[(periods >= 10) & (periods < 100)]), 2)       # 0.01-0.1 Hz
		PSD_2_st = round(statistics.stdev(psd_m[(periods >= 1) & (periods < 10)]), 2)         # 0.1-1 Hz
		PSD_3_st = round(statistics.stdev(psd_m[(periods >= 0.2) & (periods < 1)]), 2)        # 1-5 Hz
		PSD_4_st = round(statistics.stdev(psd_m[(periods >= 0.1) & (periods < 0.2)]), 2)      # 5-10 Hz
		PSD_5_st = round(statistics.stdev(psd_m[(periods >= 0.05) & (periods < 0.1)]), 2)     # 10-20 Hz
		PSD_6_st = round(statistics.stdev(psd_m[(periods >= 0.02) & (periods < 0.05)]), 2)    # 20-50 Hz
		PSD_level = [PSD_0, PSD_0_st, PSD_1, PSD_1_st, PSD_2, PSD_2_st, PSD_3, PSD_3_st,\
		PSD_4, PSD_4_st, PSD_5, PSD_5_st, PSD_6, PSD_6_st]
	else:
		PSD_level = [0 for i in range(14)]
		
	
	if found:
		file_spect_img = os.path.join(spec_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.Spect.png')
		plot_spectrogram(self= ppsd, filename = file_spect_img)	
	
	
	
	sta_results = [netcode,stacode, c_st_acc[0].stats.channel] + PSD_level 	
	#sta_results_test.append(PSD_level)	
	result_list.append(sta_results)
	
	if found:
		file_psd_temp_img = os.path.join(psd_temp_dir, f'{netcode}.{stacode}.{c_st_acc[0].stats.channel}.PSD_temp.png')
	try:
		plot_temporal(self= ppsd, period=[100, 10, 1,0.2,0.1,0.05,0.02], filename = file_psd_temp_img, color=['darkblue','blue','cyan','darkorange','red','purple'])
	except ValueError:
		pass
	
	
	
	print('The channel is:',c_st_acc[0].stats.channel[:2])
	print(condition_result)
	
	
	 #return found, plot_PDF_FV, result_list, result_header
	return found, plot_PDF_FV, result_list, ppsd
	
		

def RMS_FV(c_st_acc):
		
	tr_file = c_st_acc
	tr0_bkup=copy_deepcopy(c_st_acc)
	tr1_bkup=copy_deepcopy(c_st_acc)
	tr2_bkup=copy_deepcopy(c_st_acc)
	tr3_bkup=copy_deepcopy(c_st_acc)
	tr4_bkup=copy_deepcopy(c_st_acc)
	tr5_bkup=copy_deepcopy(c_st_acc)
	tr6_bkup=copy_deepcopy(c_st_acc)
	
	number_of_samples = sum(tr.stats.npts for tr in tr_file.traces)
	npts = float(number_of_samples)
	

	# 0.01-50 Hz
	tr0 = tr0_bkup.filter('bandpass', freqmin=0.01, freqmax=50, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_0 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr0.traces) / npts)
	sample_stdev_0 = np.sqrt(sum(((abs(tr.data) - sample_rms_0 ) ** 2).sum() for tr in tr0.traces) / npts)

	# 0.01-0.1 Hz
	tr1 = tr1_bkup.filter('bandpass', freqmin=0.01, freqmax=0.1, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_1 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr1.traces) / npts)
	sample_stdev_1 = np.sqrt(sum(((abs(tr.data) - sample_rms_1 ) ** 2).sum() for tr in tr1.traces) / npts)

	# 0.1-1 Hz
	tr2 = tr2_bkup.filter('bandpass', freqmin=0.1, freqmax=1, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_2 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr2.traces) / npts)
	sample_stdev_2 = np.sqrt(sum(((abs(tr.data) - sample_rms_2 ) ** 2).sum() for tr in tr2.traces) / npts)

	# 1-5 Hz
	tr3 = tr3_bkup.filter('bandpass', freqmin=1, freqmax=5, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_3 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr3.traces) / npts)
	sample_stdev_3 = np.sqrt(sum(((abs(tr.data) - sample_rms_3 ) ** 2).sum() for tr in tr3.traces) / npts)

	# 5-10 Hz
	tr4 = tr4_bkup.filter('bandpass', freqmin=5, freqmax=10, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_4 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr4.traces) / npts)	
	sample_stdev_4 = np.sqrt(sum(((abs(tr.data) - sample_rms_4 ) ** 2).sum() for tr in tr4.traces) / npts)

	# 10-20 Hz
	tr5 = tr5_bkup.filter('bandpass', freqmin=10, freqmax=20, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_5 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr5.traces) / npts)
	sample_stdev_5 = np.sqrt(sum(((abs(tr.data) - sample_rms_5 ) ** 2).sum() for tr in tr5.traces) / npts)

	# 20-50 Hz
	tr6 = tr6_bkup.filter('bandpass', freqmin=20, freqmax=50, corners=4, zerophase=False) # Butterworth-Bandpass
	sample_rms_6 = np.sqrt(sum((tr.data.astype(object) ** 2).sum() for tr in tr6.traces) / npts)
	sample_stdev_6 = np.sqrt(sum(((abs(tr.data) - sample_rms_6 ) ** 2).sum() for tr in tr6.traces) / npts)
	rms_all = [sample_rms_0,sample_stdev_0,sample_rms_1,sample_stdev_1,sample_rms_2,sample_stdev_2,\
	sample_rms_3,sample_stdev_3,sample_rms_4,sample_stdev_4,sample_rms_5,sample_stdev_5,\
	sample_rms_6,sample_stdev_6]
				
	return rms_all

#!/usr/bin/python3

from multiprocessing import Pool, Queue
import argparse
from os import mkdir as os_mkdir
from os import remove as os_remove
from os.path import isdir as os_path_isdir
from os.path import isfile as os_path_isfile
from sys import exit as sys_exit
from sys import argv as sys_argv
from copy import deepcopy as copy_deepcopy
import math
import matplotlib.dates as dates
import matplotlib.pylab as plt
import numpy as np
# ~ import operator
import pykooh
import re
import datetime
from sys import stderr as sys_stderr

# # import OBSPY functions
import obspy.signal
from obspy.core import UTCDateTime, read, Stream, event
from obspy.core.inventory.inventory import read_inventory
from obspy.core.event.catalog import read_events
from obspy.geodetics.base import gps2dist_azimuth as gps2DistAzimuth
from obspy.signal.cross_correlation import xcorr_max, correlate_template

# # import custom functions
from custom_def import subplot_obspy_with_cut_def
from custom_def import subplot_obspy_deconv
from custom_def import spectrum_amp_smooth
from custom_def import envelope_plot
from custom_def import spectrum_rms_integral
from custom_def import spectrum_with_bar_def
from custom_def import ITA10
from custom_def import ITA10_plot
from custom_def import Arias_Intensity
from custom_def import Arias_plot
from custom_def import sta_lta_arias_subplot
from custom_def import clipp_image

import warnings
warnings.filterwarnings("ignore")


# # Da fare: 
# # 1) Aggiungere le condizioni di qualita' th_freq e class nell'option Parser 

Qlog = Queue()
Qcsv = Queue()
Qhdr = Queue()
Qwrn = Queue()


def cli_exec_multi_event():
    # l'help della linea di comando
    synopsis = 'Make comparison between velocimetric and accelerometric waveforms from earthquake'
    epilog_str = 'Written Fabio Varchetta, Marco Massa, Rodolfo Puglia and Peter Danecek starting from an idea by Carlo Cauzzi'
    p = argparse.ArgumentParser(prog='cli_multi_event', description=synopsis, epilog=epilog_str)

    p.add_argument("-wl", "--waveform_list", action="store", type=argparse.FileType('r'), required=True, dest="id_list_name", help='List of waveform to be processed. For example: 30682211 2022-04-19 15:49:08 OPPE IV 126.9 127.6 3.8')
    p.add_argument("-cwl", "--columns_wl", nargs='+', action=required_length(3, 3), type=int, default=[0, 1, 2], dest="columns_wl", help="Set the three columns to be read in the list of waveform file (see option '--waveform_list'), respectively corresponding to: eventID, station code and network code")
    p.add_argument("-ko", "--konno_ohmachi_b", action="store", dest="b", type=int, default=10, help="Konno & Ohmachi B parameter for smoothing to detect fmin of filter. Default: 10")  
    p.add_argument("-tr", "--th_rms", action="store", dest="th_rms", type=int, default=10, help="Set threshold RMS value used on envelopes of signal and noise of accelerometric record. Default: 5")  
    # ~ p.add_argument("--tf", "--th_freq", nargs='+', action=required_length(2, 6), type=float, default=[0.3, 1, 3, 10], dest="th_freq", help="Set the limits of the band of frequencies where the ratio between the signal and noise integrals is calculated")
    p.add_argument("-tc", '--th_class', nargs='+', action=required_length(3, 3), type=int, default=[5, 10, 7], dest="th_class", help="Set the threshold to identify the quality of signal obtained from the selected frequencies band")
    p.add_argument("-snr", "--signal_noise_ratio", action="store", dest="snr", type=int, default=5, help="Set signal to noise ratio (SNR). Default: 5")  
    p.add_argument("-lff", "--low_freq_filt", action="store", dest="low_freq_filt", type=float, default=None, help="Low frequency boundary of the band-pass filter. WARNING: if used '--signal_noise_ratio' argument has no effect. Default: None")
    p.add_argument("-ip", "--instrument_period", action="store", dest="period", type=int, default=40, help="BB instrument period. Default: 40")
    p.add_argument("-pp", "--parallel_processes", action="store", dest="parallel_p", type=int, default=1, help="Number of processes to execute the code (in parallel when greater than 1). Default: 1")
    p.add_argument("-f", "--limit_low_freq_filt", action="store_true", dest="lim_lff", default=False, help="Limit the low frequency boundary of the band-pass filter (F): in case M < 4.5 and F < 0.2 Hz ==> F = 0.2 Hz; in case F > 0.4 Hz ==> F = 0.4 Hz. Default: 'False'")
    p.add_argument("-g", "--gen_fig", action="store_true", dest="gen_fig", default=False, help="Generate figures. Default: 'False'")
    p.add_argument("-t", "--test", action="store_true", dest="test_tf", default=False, help="Just test the reading of eventID, station code and network code from the list of waveform, then exit without proceeding into calculation. Default: False")
    p.add_argument("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Verbose mode (prints summary and other details). Default: False")
    p.add_argument("-psi", "--print-station-IDs", action="store_true", dest="print_ids", default=False, help="Verbose mode (prints just current identifiers: event-id, net-code, sta-code). Default: False")
    p.add_argument("-ar","--accelerometer_response", action = "store_true", dest = "acc_resp", default = False , help = "Remove response from accelerometer as reported in Station-XML file. If 'True' the accelerometer is just converted to physical units [m/s] using the 'overall-sensitivity' reported in Station-XML. Default: False")
    
    opts = p.parse_args() # args[0] e' l'argomento, opts.dest e' ciascuna opzione

    if opts.gen_fig:
        if os_path_isdir('results'):
            input('WARNING: folder "results" already exists! Press the <ENTER> key to continue...')
    
    # ~ print(opts)
    # ~ return
    th_freq = [0.3, 1, 5, 15] #opts.th_freq
    th_class = opts.th_class

    wrn_file = opts.id_list_name.name + '.wrn'
    if os_path_isfile(wrn_file): os_remove(wrn_file)
    log_file = opts.id_list_name.name + '.log'
    if os_path_isfile(log_file): os_remove(log_file)
    csv_file = opts.id_list_name.name + '.csv'
    if os_path_isfile(csv_file): os_remove(csv_file)
    hdr_insert = 1
    
    fh_log = open(log_file, 'w')
    fh_log.write(' '.join(sys_argv) + '\n')
    fh_log.close()
    
    f_name=opts.id_list_name.name
    #print(f_name)
    
    quakes = open(f_name, "r")
    # ~ dummy = next(quakes)
    
    input_list = []
    for line in quakes:
        line = line.strip()
        if not line: continue
        
        s_line = line.split()
        (eventid, station, network) = [s_line[abs(opts.columns_wl[0])], s_line[abs(opts.columns_wl[1])], s_line[abs(opts.columns_wl[2])]]
        if opts.verbose or opts.test_tf: print(eventid, network, station)
        #elif opts.print_ids: sys_stderr.write(eventid + ' ' + network + ' ' + station + '\n')
        if opts.test_tf: continue
        
        #if not line.strip(): continue
        
        #s_line = re.split(" +", line)
        #(eventid, station, network) = [s_line[abs(opts.columns_wl[0])], s_line[abs(opts.columns_wl[1])], s_line[abs(opts.columns_wl[2])]]
        #if opts.verbose or opts.test_tf: print(eventid, network, station)
        #if opts.test_tf: continue
        
        if opts.gen_fig:
            if not os_path_isdir('results'): os_mkdir('results')
            out_path = 'results/' + network + '.' + station + '/'
            if not os_path_isdir(out_path): os_mkdir(out_path)
    
        if opts.parallel_p >= 2: 

            input_list.append([eventid, network, station, opts.period, opts.b, opts.snr, th_freq, th_class, opts.th_rms, opts.low_freq_filt, opts.lim_lff, opts.gen_fig, opts.verbose, opts.acc_resp, opts.print_ids])

        elif opts.parallel_p == 1:

            err_code, out_string, out_hdr, out_table, out_wrn = quality_event(eventid, network, station, opts.period, opts.b, opts.snr, th_freq, th_class, opts.th_rms, opts.low_freq_filt, opts.lim_lff, opts.gen_fig, opts.verbose, opts.acc_resp, opts.print_ids)
            
            #print(out_wrn) 
            fh_wrn = open(wrn_file, 'a')
            for out_wrn_line in out_wrn:
                fh_wrn.write(out_wrn_line + '\n')
            fh_wrn.close()

            #print(out_string) 
            fh_log = open(log_file, 'a')
            fh_log.write(out_string + '\n')
            fh_log.close()
            
            #print(out_hdr)
            #print(out_table)
            if err_code == 0:
                fh_csv = open(csv_file, 'a')
                if hdr_insert == 1:
                    fh_csv.write(out_hdr + '\n')
                    hdr_insert = 0
                fh_csv.write(out_table)
                fh_csv.close()
             
    if opts.parallel_p >= 2:
        
        pool = Pool(processes=opts.parallel_p)
        starmap_res = pool.starmap(quality_event, input_list)
        pool.close()
        
        # ~ for value in starmap_res.get():
            # ~ print(value)
        
        fh_wrn = open(wrn_file, 'a')
        for i in range(Qwrn.qsize()):
            #print(Qwrn.get())
            fh_wrn.write(Qwrn.get() + '\n')
        fh_wrn.close()
        if os_path_isfile(wrn_file):
            # ~ #sorting wrn-file
            # ~ fh_wrn = open(wrn_file, 'r')
            # ~ lines_wrn = fh_wrn.readlines()
            # ~ lines_wrn_sort = sorted(lines_wrn)
            # ~ fh_wrn.close()
            # ~ fh_wrn_s = open(opts.id_list_name.name + '.sort.wrn','w')
            # ~ for line in lines_wrn_sort:
                # ~ fh_wrn_s.write(line)
            # ~ fh_wrn_s.close()
            #sorting wrn-file
            fh_wrn = open(wrn_file, 'r')
            lines_wrn = fh_wrn.readlines()
            lines_wrn_sort = sorted(lines_wrn[0:] , key=lambda line: int(line.split(',')[0][1:]))  # Modifica questa riga
            fh_wrn.close()
            fh_wrn_s = open(opts.id_list_name.name + '.sort.wrn','w')
            for line in lines_wrn_sort:
                fh_wrn_s.write(line)
            fh_wrn_s.close()

        fh_log = open(log_file, 'a')
        for i in range(Qlog.qsize()):
            #print(Qlog.get())
            fh_log.write(Qlog.get() + '\n')
        fh_log.close()
        if os_path_isfile(log_file):
            # ~ #sorting log-file
            # ~ fh_log = open(log_file, 'r')
            # ~ lines_log = fh_log.readlines()
            # ~ lines_log_sort = sorted(lines_log)
            # ~ fh_log.close()
            # ~ fh_log_s = open(opts.id_list_name.name + '.sort.log','w')
            # ~ for line in lines_log_sort:
                # ~ fh_log_s.write(line)
            # ~ fh_log_s.close()
            
            #sorting log-file
            fh_log = open(log_file, 'r')
            lines_log = fh_log.readlines()
            lines_log_sort = sorted(lines_log[1:], key=lambda line: int(line.split(',')[0][1:]))  
            fh_log.close()
            fh_log_s = open(opts.id_list_name.name + '.sort.log','w')
            for line in lines_log_sort:
                fh_log_s.write(line)
            
            fh_log_s.write(lines_log[0])
            fh_log_s.close()
            
            
        if Qhdr.qsize() >= 1:
            fh_csv = open(csv_file, 'w')
            for i in range(1):
                fh_csv.write(Qhdr.get() + '\n')
            for i in range(Qcsv.qsize()):
                fh_csv.write(Qcsv.get())
            fh_csv.close()
            # ~ # sorting csv-file
            # ~ fh_csv = open(csv_file, 'r')
            # ~ lines = fh_csv.readlines()
            # ~ lines_sort = sorted(lines[1:])
            # ~ fh_csv.close()
            # ~ fh_csv_s = open(opts.id_list_name.name + '.sort.csv','w')
            # ~ fh_csv_s.write(lines[0])
            # ~ for line in lines_sort:
                # ~ fh_csv_s.write(line)
            # ~ fh_csv_s.close()    
            # ~ # sorting csv-file
            fh_csv = open(csv_file, 'r')
            lines = fh_csv.readlines()
            # Ordina le righe basandoti sulla colonna contenente i valori numerici
            lines_sort = sorted(lines[1:], key=lambda line: int(line.split(',')[0]))  # Modifica questa riga
            fh_csv.close()
            fh_csv_s = open(opts.id_list_name.name + '.sort.csv', 'w')
            fh_csv_s.write(lines[0])
            for line in lines_sort:
                fh_csv_s.write(line)
            fh_csv_s.close()








                    
                    
def quality_event(eventid, network, station, period, b, snr, th_freq, th_class, th_rms, low_freq_filt, lim_lff, gen_fig, verbose, acc_resp, print_ids):
        
    try:


        # ~ network  = 'IV'
        # ~ station  = 'OPPE'
        # ~ b        = 10
        # ~ period   = 40
        # ~ snr      = 10
        # ~ th_freq  = [0.3, 1, 3, 10]
        # ~ th_class = 15
        # ~ th_rms    = 5
        # ~ gen_fig  = True
        # ~ verbose  = True 
        
        Ftitle = 'evID: ' + eventid + ' - netcode: ' + network + ' - stacode: ' + station
        wrn_var = []
        
        # Ricerca evento dal webservice e operazione di taglio considerando la Ptime
        url_eve = 'https://webservices.ingv.it/fdsnws/event/1/query?eventId=' + eventid + '&includearrivals=true&nodata=404'
        try: catalog = read_events(url_eve)
        except:
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: eventId=' + eventid + ' not found!'
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var
            
        
        # # Definizione caratteristiche evento (Mag, Lat,Long, profondita' e To=tempo origine) utili per l'operaizone di taglio
        evt = catalog[0]
        mag = evt.magnitudes[0].mag
        eve_lat = evt.origins[0].latitude
        eve_lon = evt.origins[0].longitude
        eve_depth = evt.origins[0].depth / 1000
        ot = UTCDateTime(evt.origins[0].time)

        # 0 condizione di qualita'- Verifica se l'evento e' stato rivisto in maniera manuale
        if  evt.origins[0].evaluation_mode == 'automatic' and evt.origins[0].evaluation_status == 'preliminary':
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: eventId=' + eventid + ' has "automatic"/"preliminary" location!'
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var
        
        
        # # Definizione coordinate stazione utili per il taglio  
        #url_st = 'https://webservices.ingv.it/fdsnws/station/1/query?network=' + network + '&station=' + station + '&channel=*&nodata=404'
        url_st = 'https://webservices.ingv.it/fdsnws/station/1/query?network=' + network + '&station=' + station + '&channel=*&level=channel&nodata=404'
        xml_st = read_inventory(url_st)
        sta_lat = xml_st[0][0].latitude
        sta_lon = xml_st[0][0].longitude

        # ~ x = event.WaveformStreamID(network_code=network, station_code=station, channel_code='EHZ')
        Ptime = None
        # ~ for pick in evt.picks:
            # ~ if pick.evaluation_mode == "manual":
                # ~ if pick.phase_hint == "P":
                    # ~ if pick.waveform_id == x: Ptime = UTCDateTime(pick.time)
        
        # ~ x = event.WaveformStreamID(network_code=network, station_code=station, channel_code='HHZ')
        # ~ Ptime = None
        # ~ for pick in evt.picks:
            # ~ if pick.evaluation_mode == "manual":
                # ~ if pick.phase_hint == "P":
                    # ~ if pick.waveform_id == x: Ptime = UTCDateTime(pick.time)
                        
            
        # # Fase di taglio applicando la parte di codice scritta da Rodolfo
        noise_cut_start = 200
        noise_cut_end = 200
              
        eve_depth_mod = eve_depth
        if not Ptime:
            a_dist = gps2DistAzimuth(sta_lat, sta_lon, eve_lat, eve_lon)
            epidist = a_dist[0]/1000 # azimuth e backazimuth sono rispettivamente a_dist[1] e a_dist[2]
            ipodist = math.sqrt(pow(epidist,2) + pow(eve_depth,2))
            min_cut = -32.54864+(0.0147805*mag)+(0.1946899*ipodist)
            max_cut = 67.102769+(21.616684*mag)+(0.3763951*ipodist)
            if eve_depth_mod > 35: eve_depth_mod = 35.0
            if int(eve_depth_mod) == 35:
                min_cut = min_cut - 30
                max_cut = max_cut + 60
            Ptime = ot + min_cut
            Etime = ot + max_cut
            start = Ptime - noise_cut_start
            end = ot + max_cut + noise_cut_end
            startN = start + 5
            endN = start + 185

          
        if verbose: print( 'Start time: ' + str(start))
        if verbose: print( 'End time: ' + str(end))


        # # Ricavo lo streem delle tracce e pre-processamento da EIDA
        url_data = 'https://webservices.ingv.it/fdsnws/dataselect/1/query?starttime=' + start.format_iris_web_service() + '&endtime=' + end.format_iris_web_service() + '&network=' +network + '&station=' + station + '&channel=*&nodata=404'
        streem = read(url_data).sort()
        streem = streem.detrend('linear')
        # ~ streem = streem.detrend('demean')
        streem.taper(max_percentage=0.02,type='cosine', side='both')
        # ~ streem = streem.filter('bandpass',freqmin=0.02,freqmax=50)
        
        # # Identifico dallo streem le tracce dell'evento registrate dall'Accelerometro e quelle dal Velocimetro
        tmpN = copy_deepcopy(streem)
        tmpE = copy_deepcopy(streem)

        streem_acc = copy_deepcopy(streem.select(channel='H[N,G,L]E'))
        streem_acc += copy_deepcopy(streem.select(channel='H[N,G,L]N'))
        streem_acc += copy_deepcopy(streem.select(channel='H[N,G,L]Z'))
        streem_vel = copy_deepcopy(streem.select(channel='[E,H]HE'))
        streem_vel += copy_deepcopy(streem.select(channel='[E,H]HN'))
        streem_vel += copy_deepcopy(streem.select(channel='[E,H]HZ'))

        # # 1 condizione di qualita' - Verifica delle 6 tracce
        if len(streem_acc) != 3:
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: the number of streams for the accelerometer "H[N,G,L]E" is not equal to 3!'
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var
        
        if len(streem_vel) != 3:
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: the number of streams for the velocimeter "[E,H]HE" is not equal to 3!'
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var
        
        ch_acc = streem_acc[0].stats.channel
        ch_vel = streem_vel[0].stats.channel
        
        
        # Cerca il canale nell'inventory utilizzando la funzione select
        matching_channels_acc = xml_st.select(channel=ch_acc)
        
        # Verifica se il canale è stato trovato
        
        depth_acc = matching_channels_acc[0][0][0].depth
            
        depth_vel = None 
        
        # Cerca il canale nell'inventory utilizzando la funzione select
        matching_channels_vel = xml_st.select(channel=ch_vel)
        
        # Verifica se il canale è stato trovato
        depth_vel = matching_channels_vel[0][0][0].depth
        
        
        #print(depth_acc)
        #print(depth_vel)
   
        ## Cerca il canale nell'inventory
        #depth_acc = None 
        #for nt in xml_st:
        #    for st in nt:
        #        for chl in st:
        #            if chl.code == ch_acc:
        #                depth_acc = chl.depth
        #                break
        #
        #
        #depth_vel = None 
        #for nt in xml_st:
        #    for st in nt:
        #        for chl in st:
        #            if chl.code == ch_vel:
        #                depth_vel= chl.depth
        #                break
        #
        #
        #print(depth_acc)
        #print(depth_vel)
       
       
       
        depth = [depth_acc,depth_vel]
       
       
        a = []
        for i in depth:
            if i is None:a.append(0)
            elif i >= 5:a.append(1)
            else:
                a.append(0)
        
        if sum(a)>=1:
            if verbose: print ('Depth_acc:' +str(depth[0]))
            if verbose: print('Depth_vel:'+str(depth[1]))
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: the accelerometer and velocimeter have not the same depth!'+' '+ ('Depth_acc:' +str(depth[0]))+' '+ ('Depth_vel:'+str(depth[1]))
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var
        else:
           if verbose: print('Depth conditions is verified: accelerometer and velocimeter have the same depth')
        
        
        
        if gen_fig:
            ev_dir = 'results/' + network + '.' + station + '/' + eventid
            if not os_path_isdir(ev_dir): os_mkdir(ev_dir)
            input_dir = ev_dir + '/'
    
        
        # # Definisco il delta e la frequenza di nyquist  per il pre-filtraggio
        dt_vel = (streem_vel[2].stats.delta)
        fnyq_vel = 1 / (2 * dt_vel)
        
        dt_acc = (streem_acc[2].stats.delta)
        fnyq_acc = 1 / (2 * dt_acc)
        
        if verbose: print( '----- SM -----')
        if verbose: print( 'dt:' + str(dt_acc))
        if verbose: print( 'Nyquist:' + str(fnyq_acc))
        
        if verbose: print( '----- BB -----')
        if verbose: print( 'dt:' + str(dt_vel))
        if verbose: print( 'Nyquist:' + str(fnyq_vel))
        
        
        dt_pre_filt = max([dt_vel,dt_acc])
        fnyq_pre_filt = 0.95 /(2*dt_pre_filt)
        
        
        # # Aggiungo padding per il filtraggio dello streem
        order = 3
        
        freq_min_streem = 0.02
        
        
        Tpad_streem = 3*order/freq_min_streem  # Formula di Boore 2005 
        pts2add_vel_streem = int(Tpad_streem/dt_vel/2) # punti da aggiungere - parte di codice di Rod 
        pts2add_acc_streem = int(Tpad_streem/dt_acc/2) # punti da aggiungere - parte di codice di Rod 
        
        # Aggiungo padding allo streem acc prima del filtraggio
        for i in range(3):
            streem_acc[i].data = np.pad(streem_acc[i].data, (int(pts2add_acc_streem), int(pts2add_acc_streem)), 'constant', constant_values=0)
        
        # Aggiungo padding allo streem vel prima del filtraggio
        for i in range(3):
            streem_vel[i].data = np.pad(streem_vel[i].data, (int(pts2add_vel_streem), int(pts2add_vel_streem)), 'constant', constant_values=0)
        
               
        streem_acc.filter('bandpass',freqmin=0.02,freqmax=fnyq_pre_filt)
        streem_vel.filter('bandpass',freqmin=0.02,freqmax=fnyq_pre_filt)      
        
        # Rimuovo il padding dallo streem acc dopo il filtraggio
        for i in range(3):
            streem_acc[i].data = streem_acc[i].data[pts2add_acc_streem:-pts2add_acc_streem]
        
        # Rimuovo il padding dallo streem vel dopo il filtraggio
        for i in range(3):
            streem_vel[i].data = streem_vel[i].data[pts2add_vel_streem:-pts2add_vel_streem]
        
        
               
        
        #plot con le finestre di taglio 
        if gen_fig:
            FigName = input_dir + '01_' + eventid + '.' + network + '.' + station + '_BB_SW_rawtraces_cutting.png'
            fig = subplot_obspy_with_cut_def(streem_acc, streem_vel, startN, endN, Ptime, Etime, FigName)


        #Effettuo la fase di taglio 

        # # Noise
        # # Taglio del noise e identificazione della finestra di noise dell'accelerometro e del velocimetro
        tmpN.trim(startN, endN)
        tmpN.detrend('linear')
        # ~ tmpN.taper(max_percentage=0.05,type='cosine', side='both')
        noi_acc_raw = copy_deepcopy(tmpN.select(channel='H[N,G,L]E'))
        noi_acc_raw += copy_deepcopy(tmpN.select(channel='H[N,G,L]N'))
        noi_acc_raw += copy_deepcopy(tmpN.select(channel='H[N,G,L]Z'))
        # ~ noi_acc_raw.taper(max_percentage=0.05,type='cosine', side='both')
        # ~ noi_acc_raw.integrate()
        # ~ noi_acc_raw.detrend('linear')
        # ~ noi_acc_raw.taper(max_percentage=0.05,type='cosine', side='both')
        # ~ noi_acc_raw.differentiate()
        noi_vel_raw = copy_deepcopy(tmpN.select(channel='[E,H]HE'))
        noi_vel_raw += copy_deepcopy(tmpN.select(channel='[E,H]HN'))
        noi_vel_raw += copy_deepcopy(tmpN.select(channel='[E,H]HZ'))

        # # Evento
        # # Taglio dell'evento e identificazione della finestra di noise dell'accelerometro e del velocimetro
        tmpE.trim(Ptime, Etime)
        tmpE.detrend('linear')
        # ~ tmpE.taper(max_percentage=0.05,type='cosine', side='both')        
        sig_acc_raw = copy_deepcopy(tmpE.select(channel='H[N,G,L]E'))
        sig_acc_raw += copy_deepcopy(tmpE.select(channel='H[N,G,L]N'))
        sig_acc_raw += copy_deepcopy(tmpE.select(channel='H[N,G,L]Z'))
        # ~ sig_acc_raw.taper(max_percentage=0.05,type='cosine', side='both')
        # ~ sig_acc_raw.integrate()
        # ~ sig_acc_raw.detrend('linear')
        # ~ sig_acc_raw.taper(max_percentage=0.05,type='cosine', side='both')
        # ~ sig_acc_raw.differentiate()
        sig_vel_raw = copy_deepcopy(tmpE.select(channel='[E,H]HE'))
        sig_vel_raw += copy_deepcopy(tmpE.select(channel='[E,H]HN'))
        sig_vel_raw += copy_deepcopy(tmpE.select(channel='[E,H]HZ'))

   
        
        # # 2 condizione di qualita' - Verifica del clip 
        # 85% MILN + BG event NO clipped
        clip = .70 * 2**(24-1) #fixed values depending on digitizer (24bit)
        pgcounts = sig_vel_raw.max()
        label = 'onscale'
        
        a = []
        for i in pgcounts:
            if i >= 0: a.append(-i)
            else:
                a.append(abs(i))
        
        
        if gen_fig:
            FigName2 = input_dir + '02_' + eventid + '.' + network + '.' + station + '_clipping_verifing.png'
            fig = clipp_image(sig_vel_raw,clip, pgcounts,a,FigName2,Ftitle)
        
        
        ratio_clip = []
        for peak in pgcounts:
            ratio_clip.append(abs(peak/clip))
            
            
        
        
        for i in ratio_clip:
            if i >= 1:
                err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: velocimetric signal clipped' #label switches to 'clipped' is the threshold is exceeded on any channel
                if verbose: print(err_var)
                Qlog.put(err_var)
                return 1, err_var, '', '', wrn_var
        
        


        # # Costruzione del filtro passa banda prima della deconvoluzione
        # ~ pre_filt_vel = [0.001, 1 / period, .8 * fnyq_vel, fnyq_vel]

        # # Scarico le informazioni della stazione per la deconvoluzione
        url_md = 'https://webservices.ingv.it/fdsnws/station/1/query?starttime=' + start.format_iris_web_service() + '&endtime=' + end.format_iris_web_service() +'&network=' + network + '&station=' + station + '&channel=*&level=response&nodata=404'
        resp = read_inventory(url_md)

        # # Deconvoluzione rimuovendo la risposta strumentale.
        # Accelerometri
        # Scommentare il blocco successivo e commentare il blocco seguente se si vuole effettuare la deconvoluzione anche per l'accelerometro
        sig_acc_raw_bkup = copy_deepcopy(sig_acc_raw)
        noi_acc_raw_bkup = copy_deepcopy(noi_acc_raw)
        
        #if print_ids: sys_stderr.write(eventid + ' ' + network + ' ' + station + '\n')
        if print_ids: print(eventid + ' ' + network + ' ' + station)
       # if station == 'SSY':
       #     sys_stderr.write('Warning: Hello\n')
        
        
        
        
        if acc_resp:
            streem_acc_from_acc = streem_acc.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "ACC", plot = False, water_level = 100)
            sig_acc_from_acc = sig_acc_raw.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "ACC", plot = False, water_level = 100)
            sig_vel_from_acc = sig_acc_raw_bkup.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "VEL", plot = False, water_level = 100)
            noi_acc_from_acc = noi_acc_raw.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "ACC", plot = False, water_level = 100)
            noi_vel_from_acc = noi_acc_raw_bkup.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "VEL", plot = False, water_level = 100)
            sensitivity_values_list= [-9999, -9999, -9999]
        else:
            streem_acc_from_acc = copy_deepcopy(streem_acc)
            sig_acc_from_acc = copy_deepcopy(sig_acc_raw)
            noi_acc_from_acc = copy_deepcopy(noi_acc_raw)
            streem_acc[0].attach_response(resp)
            streem_acc[1].attach_response(resp)
            streem_acc[2].attach_response(resp)
            sensitivity_value_e = streem_acc[0].stats.response.instrument_sensitivity.value
            sensitivity_value_n = streem_acc[1].stats.response.instrument_sensitivity.value
            sensitivity_value_z = streem_acc[2].stats.response.instrument_sensitivity.value
            streem_acc_from_acc[0].data = streem_acc_from_acc[0].data/sensitivity_value_e
            streem_acc_from_acc[1].data = streem_acc_from_acc[1].data/sensitivity_value_n
            streem_acc_from_acc[2].data = streem_acc_from_acc[2].data/sensitivity_value_z
            sig_acc_from_acc[0].data = sig_acc_from_acc[0].data/sensitivity_value_e
            sig_acc_from_acc[1].data = sig_acc_from_acc[1].data/sensitivity_value_n
            sig_acc_from_acc[2].data = sig_acc_from_acc[2].data/sensitivity_value_z
            noi_acc_from_acc[0].data = noi_acc_from_acc[0].data/sensitivity_value_e
            noi_acc_from_acc[1].data = noi_acc_from_acc[1].data/sensitivity_value_n
            noi_acc_from_acc[2].data = noi_acc_from_acc[2].data/sensitivity_value_z
            # Integro l'accelerazione per ottenere la velocità
            sig_vel_from_acc = copy_deepcopy(sig_acc_from_acc).integrate()
            noi_vel_from_acc = copy_deepcopy(noi_acc_from_acc).integrate()  
            
            sensitivity_values_list = [sensitivity_value_e, sensitivity_value_n, sensitivity_value_z]
            
            
     
        streem_acc_from_acc.integrate()
        streem_acc_from_acc.detrend('linear')    
        streem_acc_from_acc.taper(max_percentage=0.05,type='cosine', side='both')  
        streem_acc_from_acc.differentiate()    
            
        sig_acc_from_acc.integrate()
        sig_acc_from_acc.detrend('linear')
        sig_acc_from_acc.taper(max_percentage=0.05,type='cosine', side='both')
        sig_acc_from_acc.differentiate()
      
        sig_vel_from_acc.detrend('linear')
        sig_vel_from_acc.taper(max_percentage=0.05,type='cosine', side='both')
      
        noi_acc_from_acc.integrate()
        noi_acc_from_acc.detrend('linear')
        noi_acc_from_acc.taper(max_percentage=0.05,type='cosine', side='both')
        noi_acc_from_acc.differentiate()
      
        noi_vel_from_acc.detrend('linear')
        noi_vel_from_acc.taper(max_percentage=0.05,type='cosine', side='both')
            
        #print(sensitivity_values_list)
        
         
        
        # ~ streem_acc_from_acc = copy_deepcopy(streem_acc)
        # ~ sig_acc_from_acc = copy_deepcopy(sig_acc_raw)
        # ~ noi_acc_from_acc = copy_deepcopy(noi_acc_raw)
        # ~ streem_acc[0].attach_response(resp)
        # ~ streem_acc[1].attach_response(resp)
        # ~ streem_acc[2].attach_response(resp)
        # ~ sensitivity_value_e = streem_acc[0].stats.response.instrument_sensitivity.value
        # ~ sensitivity_value_n = streem_acc[1].stats.response.instrument_sensitivity.value
        # ~ sensitivity_value_z = streem_acc[2].stats.response.instrument_sensitivity.value
        # ~ streem_acc_from_acc[0].data = streem_acc_from_acc[0].data/sensitivity_value_e
        # ~ streem_acc_from_acc[1].data = streem_acc_from_acc[1].data/sensitivity_value_n
        # ~ streem_acc_from_acc[2].data = streem_acc_from_acc[2].data/sensitivity_value_z
        # ~ sig_acc_from_acc[0].data = sig_acc_from_acc[0].data/sensitivity_value_e
        # ~ sig_acc_from_acc[1].data = sig_acc_from_acc[1].data/sensitivity_value_n
        # ~ sig_acc_from_acc[2].data = sig_acc_from_acc[2].data/sensitivity_value_z
        # ~ noi_acc_from_acc[0].data = noi_acc_from_acc[0].data/sensitivity_value_e
        # ~ noi_acc_from_acc[1].data = noi_acc_from_acc[1].data/sensitivity_value_n
        # ~ noi_acc_from_acc[2].data = noi_acc_from_acc[2].data/sensitivity_value_z
        
        # ~ # Integro l'accelerazione per ottenere la velocità
        # ~ sig_vel_from_acc = copy_deepcopy(sig_acc_from_acc).integrate()
        # ~ noi_vel_from_acc = copy_deepcopy(noi_acc_from_acc).integrate()        
        
               
        #Velocimetri
        sig_vel_raw_bkup = copy_deepcopy(sig_vel_raw)
        noi_vel_raw_bkup = copy_deepcopy(noi_vel_raw)
        # ~ sig_acc_from_vel = sig_vel_raw.remove_response(inventory = resp, pre_filt = pre_filt_vel, output = "ACC", plot = False, water_level = 100)
        # ~ sig_vel_from_vel = sig_vel_raw_bkup.remove_response(inventory = resp, pre_filt = pre_filt_vel, output = "VEL", plot = False, water_level = 100)
        # ~ noi_acc_from_vel = noi_vel_raw.remove_response(inventory = resp, pre_filt = pre_filt_vel, output = "ACC", plot = False, water_level = 100)
        # ~ noi_vel_from_vel = noi_vel_raw_bkup.remove_response(inventory = resp, pre_filt = pre_filt_vel, output = "VEL", plot = False, water_level = 100)
        
        sig_acc_from_vel = sig_vel_raw.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "ACC", plot = False, water_level = 100)
        sig_acc_from_vel.integrate()
        sig_acc_from_vel.detrend('linear')
        sig_acc_from_vel.taper(max_percentage=0.05,type='cosine', side='both')
        sig_acc_from_vel.differentiate()
        
        sig_vel_from_vel = sig_vel_raw_bkup.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "VEL", plot = False, water_level = 100)
        sig_vel_from_vel.detrend('linear')
        sig_vel_from_vel.taper(max_percentage=0.05,type='cosine', side='both')
        
        noi_acc_from_vel = noi_vel_raw.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "ACC", plot = False, water_level = 100)
        noi_acc_from_vel.integrate()
        noi_acc_from_vel.detrend('linear')
        noi_acc_from_vel.taper(max_percentage=0.05,type='cosine', side='both')
        noi_acc_from_vel.differentiate()
        
        noi_vel_from_vel = noi_vel_raw_bkup.remove_response(inventory = resp, taper= True, taper_fraction=0.05, output = "VEL", plot = False, water_level = 100)
        noi_vel_from_vel.detrend('linear')
        noi_vel_from_vel.taper(max_percentage=0.05,type='cosine', side='both')
        
        if gen_fig:
            not_string = "not "
            FigName3 = input_dir+'03_'+eventid+'.'+network + '.' + station +'_aa_av_deconv.png'
            FigName4 = input_dir+'04_'+eventid+'.'+network + '.' + station +'_vv_va_deconv.png'
            FigName5 = input_dir+'05_'+eventid+'.'+network + '.' + station +'_n_av_aa_deconv.png'
            FigName6 = input_dir+'06_'+eventid+'.'+network + '.' + station +'_n_vv_va_deconv.png'
            fig3, fig4, fig5, fig6 = subplot_obspy_deconv(sig_acc_from_acc,sig_acc_from_vel,sig_vel_from_acc,sig_vel_from_vel,noi_acc_from_acc,\
            noi_acc_from_vel,noi_vel_from_vel,noi_vel_from_acc,not_string,FigName3,FigName4,FigName5,FigName6)


        # # Ricampionamento e definizione vettore frequenze
        nfft_acc_from_acc = int(np.exp2(np.ceil(np.log2(sig_acc_from_acc[0].stats.npts))))
        nfft_acc_from_vel = int(np.exp2(np.ceil(np.log2(sig_acc_from_vel[0].stats.npts))))
        duration = nfft_acc_from_acc * dt_acc; 
        df = 1 / duration;
        freq_acc = np.arange(0, fnyq_acc, df)
        freq_vel = np.arange(0, fnyq_vel, df)

        nfft_vel_from_vel = int(np.exp2(np.ceil(np.log2(sig_vel_from_vel[0].stats.npts))))
        nfft_vel_from_acc = int(np.exp2(np.ceil(np.log2(sig_vel_from_acc[0].stats.npts))))
        duration_vel=nfft_vel_from_vel*dt_vel 


        # # FFT dell'evento e del Noise registrato dall' accelerometro
        fft_acc_from_acc_e = np.fft.rfft(sig_acc_from_acc[0], nfft_acc_from_acc) * dt_acc
        fft_acc_from_acc_n = np.fft.rfft(sig_acc_from_acc[1], nfft_acc_from_acc) * dt_acc
        fft_acc_from_acc_z = np.fft.rfft(sig_acc_from_acc[2], nfft_acc_from_acc) * dt_acc

        fft_acc_from_acc_e_noise = np.fft.rfft(noi_acc_from_acc[0], nfft_acc_from_acc) * dt_acc
        fft_acc_from_acc_n_noise = np.fft.rfft(noi_acc_from_acc[1], nfft_acc_from_acc) * dt_acc
        fft_acc_from_acc_z_noise = np.fft.rfft(noi_acc_from_acc[2], nfft_acc_from_acc) * dt_acc

        fft_acc_from_vel_e = np.fft.rfft(sig_acc_from_vel[0], nfft_acc_from_vel) * dt_vel
        fft_acc_from_vel_n = np.fft.rfft(sig_acc_from_vel[1], nfft_acc_from_vel) * dt_vel
        fft_acc_from_vel_z = np.fft.rfft(sig_acc_from_vel[2], nfft_acc_from_vel) * dt_vel

        fft_acc_from_vel_e_noise = np.fft.rfft(noi_acc_from_vel[0], nfft_acc_from_vel) * dt_vel
        fft_acc_from_vel_n_noise = np.fft.rfft(noi_acc_from_vel[1], nfft_acc_from_vel) * dt_vel
        fft_acc_from_vel_z_noise = np.fft.rfft(noi_acc_from_vel[2], nfft_acc_from_vel) * dt_vel

        # # FFT dell'evento e del Noise registrato dal velocimetro
        fft_vel_from_vel_e = np.fft.rfft(sig_vel_from_vel[0], nfft_vel_from_vel) * dt_vel
        fft_vel_from_vel_n = np.fft.rfft(sig_vel_from_vel[1], nfft_vel_from_vel) * dt_vel
        fft_vel_from_vel_z = np.fft.rfft(sig_vel_from_vel[2], nfft_vel_from_vel) * dt_vel

        fft_vel_from_vel_e_noise = np.fft.rfft(noi_vel_from_vel[0], nfft_vel_from_vel) * dt_vel
        fft_vel_from_vel_n_noise = np.fft.rfft(noi_vel_from_vel[1], nfft_vel_from_vel) * dt_vel
        fft_vel_from_vel_z_noise = np.fft.rfft(noi_vel_from_vel[2], nfft_vel_from_vel) * dt_vel


        fft_vel_from_acc_e = np.fft.rfft(sig_vel_from_acc[0], nfft_vel_from_acc) * dt_acc
        fft_vel_from_acc_n = np.fft.rfft(sig_vel_from_acc[1], nfft_vel_from_acc) * dt_acc
        fft_vel_from_acc_z = np.fft.rfft(sig_vel_from_acc[2], nfft_vel_from_acc) * dt_acc


        fft_vel_from_acc_e_noise = np.fft.rfft(noi_vel_from_acc[0], nfft_vel_from_acc) * dt_acc
        fft_vel_from_acc_n_noise = np.fft.rfft(noi_vel_from_acc[1], nfft_vel_from_acc) * dt_acc
        fft_vel_from_acc_z_noise = np.fft.rfft(noi_vel_from_acc[2], nfft_vel_from_acc) * dt_acc


        # # Smoothing delle FFT del noise ed evento dei segnali in Accelerazione deconvoluti
        aae = pykooh.smooth(freq_vel, freq_acc, abs(fft_acc_from_acc_e[0:nfft_acc_from_acc//2]), b)
        aan = pykooh.smooth(freq_vel, freq_acc, abs(fft_acc_from_acc_n[0:nfft_acc_from_acc//2]), b)
        aaz = pykooh.smooth(freq_vel, freq_acc, abs(fft_acc_from_acc_z[0:nfft_acc_from_acc//2]), b)

        aae_n = pykooh.smooth(freq_vel, freq_acc, abs(fft_acc_from_acc_e_noise[0:nfft_acc_from_acc//2]), b)
        aan_n = pykooh.smooth(freq_vel, freq_acc, abs(fft_acc_from_acc_n_noise[0:nfft_acc_from_acc//2]), b)
        aaz_n = pykooh.smooth(freq_vel, freq_acc, abs(fft_acc_from_acc_z_noise[0:nfft_acc_from_acc//2]), b)

        ave = pykooh.smooth(freq_vel, freq_vel, abs(fft_acc_from_vel_e[0:nfft_acc_from_vel//2]), b)
        avn = pykooh.smooth(freq_vel, freq_vel, abs(fft_acc_from_vel_n[0:nfft_acc_from_vel//2]), b)
        avz = pykooh.smooth(freq_vel, freq_vel, abs(fft_acc_from_vel_z[0:nfft_acc_from_vel//2]), b)

        ave_n = pykooh.smooth(freq_vel, freq_vel, abs(fft_acc_from_vel_e_noise[0:nfft_acc_from_vel//2]), b)
        avn_n = pykooh.smooth(freq_vel, freq_vel, abs(fft_acc_from_vel_n_noise[0:nfft_acc_from_vel//2]), b)
        avz_n = pykooh.smooth(freq_vel, freq_vel, abs(fft_acc_from_vel_z_noise[0:nfft_acc_from_vel//2]), b)

        # # Salvataggio degli spettri d'ampiezza dell'accelerometro smoothati 
        # ~ save_spe(aae, aae_n, ave, ave_n, freq_vel, 'E', eventid, network, station)
        # ~ save_spe(aan, aan_n, avn, avn_n, freq_vel, 'N', eventid, network, station)
        # ~ save_spe(aaz, aaz_n, avz, avz_n, freq_vel, 'Z', eventid, network, station)


        # # Smoothing delle FFT del noise ed evento dei segnali del Velocimetro deconvoluto
        vve = pykooh.smooth(freq_vel, freq_vel, abs(fft_vel_from_vel_e[0:nfft_vel_from_vel//2]), b)
        vvn = pykooh.smooth(freq_vel, freq_vel, abs(fft_vel_from_vel_n[0:nfft_vel_from_vel//2]), b)
        vvz = pykooh.smooth(freq_vel, freq_vel, abs(fft_vel_from_vel_z[0:nfft_vel_from_vel//2]), b)
                                                    
        vve_n = pykooh.smooth(freq_vel, freq_vel, abs(fft_vel_from_vel_e_noise[0:nfft_vel_from_vel//2]), b)
        vvn_n = pykooh.smooth(freq_vel, freq_vel, abs(fft_vel_from_vel_n_noise[0:nfft_vel_from_vel//2]), b)
        vvz_n = pykooh.smooth(freq_vel, freq_vel, abs(fft_vel_from_vel_z_noise[0:nfft_vel_from_vel//2]), b)
                                                    
        vae = pykooh.smooth(freq_vel, freq_acc, abs(fft_vel_from_acc_e[0:nfft_vel_from_acc//2]), b)
        van = pykooh.smooth(freq_vel, freq_acc, abs(fft_vel_from_acc_n[0:nfft_vel_from_acc//2]), b)
        vaz = pykooh.smooth(freq_vel, freq_acc, abs(fft_vel_from_acc_z[0:nfft_vel_from_acc//2]), b)
                                                    
        vae_n = pykooh.smooth(freq_vel, freq_acc, abs(fft_vel_from_acc_e_noise[0:nfft_vel_from_acc//2]), b)
        van_n = pykooh.smooth(freq_vel, freq_acc, abs(fft_vel_from_acc_n_noise[0:nfft_vel_from_acc//2]), b)
        vaz_n = pykooh.smooth(freq_vel, freq_acc, abs(fft_vel_from_acc_z_noise[0:nfft_vel_from_acc//2]), b)

        # # Salvataggio degli spettri d'ampiezza del velocimetro 
        # ~ save_velocity_spe(vve, vve_n, vae, vae_n, freq_vel, 'E', eventid, network, station)
        # ~ save_velocity_spe(vvn, vvn_n, van, van_n, freq_vel, 'N', eventid, network, station)
        # ~ save_velocity_spe(vvz, vvz_n, vaz, vaz_n, freq_vel, 'Z', eventid, network, station)


        # # Immagine spettri
        if gen_fig:
            FigName7 = input_dir+'07_'+eventid+'.'+network + '.' + station +'.FAS_vector.png'
            fig = spectrum_amp_smooth(freq_vel,aaz,avz,aaz_n,avz_n,aan,avn,aan_n,avn_n,aae,ave,aae_n,ave_n,FigName7,Ftitle)


        # # Parametri accelerazione grezzi ITA10
        pga_ita10 = np.array([sig_acc_from_acc.max()]).flatten()


        # # Confronto PGA calcolate con  legge di attenuazione 
        if mag < 3.5: 
            tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: event magnitude is lower than 3.5, then we can not use ITA10 GMPE'
            Qwrn.put(tmp_wrn_str)
            wrn_var.append(tmp_wrn_str)
        elif epidist > 200: 
            tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: epicentral distance is higher than 200 Km, then we can not use ITA10 GMPE' 
            Qwrn.put(tmp_wrn_str)
            wrn_var.append(tmp_wrn_str)
        else:
            vec_dist = np.linspace(0.1,200,10000)
            stats_array = []
            for i in vec_dist: stats_array.append(ITA10(mag,i,3))
            stats_array = np.array(stats_array)
            pga_gmean=np.sqrt((abs(pga_ita10[0])*(abs(pga_ita10[1]))))
            #Fare grafico
            if gen_fig:
                FigName8 = input_dir + '08_' + eventid + '.' + network + '.' + station + '_ITA10.png'
                fig = ITA10_plot(vec_dist,stats_array,epidist,pga_gmean,FigName8)
            vec_value = ITA10(mag,epidist,3)
            sigma = vec_value[1]
            pga_3sigma_plus  = vec_value[0]*(3*sigma)
            pga_3sigma_minus = vec_value[0]/(3*sigma)
            if pga_gmean < pga_3sigma_minus or pga_gmean > pga_3sigma_plus:
                tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: this waveform has value of PGA outside the +/-3 sigma interval of ITA10 GMPE'
                Qwrn.put(tmp_wrn_str)
                wrn_var.append(tmp_wrn_str)       


        # # Confronto componenti orizzontali
        if abs(pga_ita10[1])/abs(pga_ita10[0]) > 5.0 or abs(pga_ita10[0])/abs(pga_ita10[1]) >  5.0:
            tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: the two horizontal components differs more than 5 times each others'
            Qwrn.put(tmp_wrn_str)
            wrn_var.append(tmp_wrn_str)            
        
        
        # ~ # # Confronto Intensità di Arias
        # ~ I_ar_Z_nor = Arias_Intensity(streem_acc_from_acc,dt_acc)
        # ~ if gen_fig:
            # ~ FigName15= input_dir + '15_' + eventid + '.' + network + '.' + station + 'subplot_arias_Intensity_and_event_trace.png'
            # ~ fig15 = Arias_plot(streem_acc_from_acc,I_ar_Z_nor,Ptime,Etime,FigName15)
        # ~ Ptime_sec = Ptime - start
        # ~ Etime_sec = Etime - start
        # ~ npts = streem_acc_from_acc[2].stats.npts
        # ~ s_rate = streem_acc_from_acc[2].stats.sampling_rate
        # ~ times = np.arange(0,npts/s_rate,1/s_rate)
        # ~ t5_interp_def = np.interp(0.05,I_ar_Z_nor,times[0:len(I_ar_Z_nor)])
        # ~ t95_interp_def = np.interp(0.95,I_ar_Z_nor,times[0:len(I_ar_Z_nor)])
        # ~ if t5_interp_def < Ptime_sec:
            # ~ tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: the Arias T05 value is before the start of signal window (this could mean there is a foreshock within the noise window)'
            # ~ Qwrn.put(tmp_wrn_str)
            # ~ wrn_var.append(tmp_wrn_str)
        # ~ if t95_interp_def > Etime_sec:
            # ~ tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: the Arias T95 value is after the end of signal window (this could mean there is an aftershock)'
            # ~ Qwrn.put(tmp_wrn_str)
            # ~ wrn_var.append(tmp_wrn_str)

        
        # # Confronto Intensità di Arias
        I_ar_Z_nor = Arias_Intensity(streem_acc_from_acc,dt_acc)
        if gen_fig:
            FigName9= input_dir + '09_' + eventid + '.' + network + '.' + station + '_subplot_arias_Intensity_and_event_trace.png'
            fig9 = Arias_plot(streem_acc_from_acc,I_ar_Z_nor,Ptime,Etime,FigName9)
        Ptime_sec = Ptime - start
        Etime_sec = Etime - start
        npts = streem_acc_from_acc[2].stats.npts
        s_rate = streem_acc_from_acc[2].stats.sampling_rate
        times = np.arange(0,npts/s_rate,1/s_rate)
        t5_interp_def = np.interp(0.05,I_ar_Z_nor,times[0:len(I_ar_Z_nor)])
        t95_interp_def = np.interp(0.95,I_ar_Z_nor,times[0:len(I_ar_Z_nor)])
        
        # # Confronto STA/LTA
        sta_test = 1
        lta_test = 10
        
        trig_test_z = copy_deepcopy(streem_acc_from_acc[2])
        
        trig_test_z.trigger("recstalta", sta=sta_test, lta=lta_test)
        
        
        if gen_fig:
            FigName10= input_dir + '10_' + eventid + '.' + network + '.' + station + '_subplot_sta_lta_arias_Intensity_and_event_trace.png'
            fig = sta_lta_arias_subplot(streem_acc_from_acc,trig_test_z,I_ar_Z_nor,Ptime,Etime,FigName10,Ftitle)
         
        if 3.0 < mag < 4.4 and t5_interp_def < Ptime_sec:
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: the Arias T05 value is before the start of signal window'
            if verbose: print(err_var)
            Qlog.put(err_var)    
            return 1, err_var, '', '', wrn_var             
        
        #if t5_interp_def < Ptime_sec:
        #    err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: the Arias T05 value is before the start of signal window'
        #    if verbose: print(err_var)
        #    Qlog.put(err_var)
        #    return 1, err_var, '', '', wrn_var

        if mag > 4.5 and t5_interp_def < Ptime_sec:
            tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: the Arias T05 value is before the start of signal window '
            if verbose: print(tmp_wrn_str)
            Qwrn.put(tmp_wrn_str)
            wrn_var.append(tmp_wrn_str)



        # ~ # # Confronto STA/LTA
        # ~ sta_test = 1
        # ~ lta_test = 10
        
        # ~ trig_test_z = copy_deepcopy(streem_acc_from_acc[2])
        
        # ~ trig_test_z.trigger("recstalta", sta=sta_test, lta=lta_test)
        
        
        # ~ if gen_fig:
            # ~ FigName9= input_dir + '09_' + eventid + '.' + network + '.' + station + ' subplot_sta_lta_arias_Intensity_and_event_trace.png'
            # ~ fig = sta_lta_arias_subplot(streem_acc_from_acc,trig_test_z,I_ar_Z_nor,Ptime,Etime,FigName9)

        
        # # 3 condizione di qualita' - Inviluppo Noise/Segnale (RMS)  su tutte e tre componenti considerando i segnali accelerometrici
        sig_acc_from_acc_enve = copy_deepcopy(sig_acc_from_acc)
        noi_acc_from_acc_enve = copy_deepcopy(noi_acc_from_acc)
        
        
        streem_acc_from_acc_enve = copy_deepcopy(streem_acc_from_acc)
        
        enve   = obspy.signal.filter.envelope(sig_acc_from_acc_enve[0].data)
        envn   = obspy.signal.filter.envelope(sig_acc_from_acc_enve[1].data)
        envz   = obspy.signal.filter.envelope(sig_acc_from_acc_enve[2].data)
                                                                                        
        enve_n = obspy.signal.filter.envelope(noi_acc_from_acc_enve[0].data)
        envn_n = obspy.signal.filter.envelope(noi_acc_from_acc_enve[1].data)
        envz_n = obspy.signal.filter.envelope(noi_acc_from_acc_enve[2].data)
        
        menve = np.sqrt(np.mean(enve**2)) 
        menve_n = np.sqrt(np.mean(enve_n**2))
        
        menvn = np.sqrt(np.mean(envn**2))
        menvn_n = np.sqrt(np.mean(envn_n**2))
        
        menvz = np.sqrt(np.mean(envz**2))
        menvz_n = np.sqrt(np.mean(envz_n**2))
        
        ratioe = menve/menve_n
        ration = menvn/menvn_n
        ratioz = menvz/menvz_n
        
        enve_streem_e = obspy.signal.filter.envelope(streem_acc_from_acc_enve[0].data)
        enve_streem_n = obspy.signal.filter.envelope(streem_acc_from_acc_enve[1].data)
        enve_streem_z = obspy.signal.filter.envelope(streem_acc_from_acc_enve[2].data)
        
        
        
        if gen_fig:
            FigName11 = input_dir+'11_'+eventid+'.'+network + '.' + station +'.envelope_plot.png'
            fig = envelope_plot(streem_acc_from_acc_enve,enve_streem_e,enve_streem_n,enve_streem_z,Ptime, Etime,startN, endN, FigName11,Ftitle)   
   
   
        if verbose: 
            print('RMS Signal/Noise component E:', ratioe)
            print('RMS Signal/Noise component N:', ration)
            print('RMS Signal/Noise component Z:', ratioz)

        if min([ratioe, ration, ratioz]) < th_rms:
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: RMS (' + str(min([ratioe, ration, ratioz])) + ') is lower then ' + str(th_rms)
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var

        # # 4 condizione di qualita' - Integrale Spettri Noise/segnale (RMS) per classi di frequenza (0.1 0.5 1 5 10 15)
        c_freq = []; RINTe = []; RINTn = []; RINTz = []
        for i in range(len(th_freq)-1):
            c_freq.append(np.where((freq_vel > th_freq[i]) & (freq_vel <= th_freq[i+1]))[0])
            RINTe.append(np.trapz(aae[c_freq[i]])/np.trapz(aae_n[c_freq[i]]))
            RINTn.append(np.trapz(aan[c_freq[i]])/np.trapz(aan_n[c_freq[i]]))
            RINTz.append(np.trapz(aaz[c_freq[i]])/np.trapz(aaz_n[c_freq[i]]))
            if verbose: 
                print('ratio between event and noise spectra integrals on the frequency band [', str(th_freq[i]), ', ', str(th_freq[i+1]), '] component E:', str(RINTe[i]))
                print('ratio between event and noise spectra integrals on the frequency band [', str(th_freq[i]), ', ', str(th_freq[i+1]), '] component N:', str(RINTn[i]))
                print('ratio between event and noise spectra integrals on the frequency band [', str(th_freq[i]), ', ', str(th_freq[i+1]), '] component Z:', str(RINTz[i])) 
                
                
                
        if gen_fig:
            FigName12 = input_dir+'12_'+eventid+'.'+network + '.' + station +'.FAS_with_bar_interval_for_integral_ratio_between_event_and_noise.png'
            fig = spectrum_rms_integral(freq_vel,aaz,aaz_n,aan,aan_n,aae,aae_n,th_freq,FigName12,Ftitle)
                
            
        #th_class = 15
        # ~ for i in ['e','n','z']:
            # ~ if max(eval('RINT'+i)) <= th_class:
                # ~ err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: ratio between event and noise spectra integrals of the component ' + upper(i) + ' is lower than ' + str(th_class)
                # ~ if verbose: print(err_var)
                # ~ Qlog.put(err_var)
                # ~ return 1, err_var, '', '', wrn_var
                
        Qletter = []
        sum_tot_Q = 0
        for i in ['e','n','z']:
            exec('Qlist'+i+'=[]')
            for j in range(len(eval('RINT'+i))):
                if eval('RINT'+i+'[j]') < float(th_class[j]):
                    eval('Qlist'+i+'.append(0.)')
                else:
                    eval('Qlist'+i+'.append(1.)')
            exec('sum_Qlist'+i+' = sum(Qlist'+i+')')
            if eval('sum_Qlist'+i) > 2.5: Qletter.append('A')
            elif eval('sum_Qlist'+i) > 1.5: Qletter.append('B')
            elif eval('sum_Qlist'+i) > 0.5: Qletter.append('C')
            else: Qletter.append('D')
            sum_tot_Q += sum(eval('Qlist'+i))
        countD = Qletter.count("D")


        # ~ Qletter=[]
        # ~ for i in ['e','n','z']:
            # ~ count_i = {"A":eval('Qlist'+i).count('A'),"B":eval('Qlist'+i).count('B'), "C":eval('Qlist'+i).count('C')}
            # ~ if max(count_i.values())>=2:
                # ~ Value_letter_i = max(count_i.items(), key=operator.itemgetter(1))[0]
            # ~ else:
                # ~ Value_letter_i = 'C'
            # ~ Qletter.append(Value_letter_i)
            
        
        # 5 condizione di qualita' - calcolo della freq-min considerando l'SNR
        acc_noise_ratio=aaz/aaz_n
        positions = np.where(acc_noise_ratio >= snr)[0]
        if not low_freq_filt:
            if positions.size == 0:
                err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: fmin not found'
                if verbose: print(err_var)
                Qlog.put(err_var)
                return 1, err_var, '', '', wrn_var
            fmin = freq_vel[positions[0]]
        else:
            if positions.size == 0:
                fmin = low_freq_filt
            else:
                fmin = freq_vel[positions[0]]
        
        if lim_lff:
            if fmin >= 0.4: fmin = 0.4
            if mag < 4.5 and fmin < 0.2: fmin = 0.2
            if (4.5 <= mag < 5.5) and fmin < 0.1: fmin = 0.1
            if mag>=5.5 and fmin < 0.05: fmin = 0.05

        # # Immagine della bande di frequenza per le quali avverra' il fitraggio
        fmax=.8 * fnyq_vel
        if gen_fig:
            FigName13 = input_dir+'13_'+eventid+'.'+network + '.' + station +'.FAS_with_bar.png'
            fig = spectrum_with_bar_def(freq_vel,fmin,fmax,aaz,aaz_n,aan,aan_n,aae,aae_n,vvz,vvz_n,vvn,vvn_n,vve,vve_n,FigName13,Ftitle)
            
            
                
        # # Filtraggio dei segnali con un passa banda tra l' fmin e l'80% della freq di Nquist
        order = 3
        
        Tpad = 3*order/fmin  # Formula di Boore 2005 
        pts2add_vel = int(Tpad/dt_vel/2) # punti da aggiungere - parte di codice di Rod 
        pts2add_acc = int(Tpad/dt_acc/2) # punti da aggiungere - parte di codice di Rod 
        
        # Aggiungo padding ai from acc prima del filtraggio
        for i in range(3):
            sig_acc_from_acc[i].data = np.pad(sig_acc_from_acc[i].data,(int(pts2add_acc),int(pts2add_acc)),'constant',constant_values = 0)
            noi_acc_from_acc[i].data = np.pad(noi_acc_from_acc[i].data,(int(pts2add_acc),int(pts2add_acc)),'constant',constant_values = 0)
            sig_vel_from_acc[i].data = np.pad(sig_vel_from_acc[i].data,(int(pts2add_acc),int(pts2add_acc)),'constant',constant_values = 0)
            noi_vel_from_acc[i].data = np.pad(noi_vel_from_acc[i].data,(int(pts2add_acc),int(pts2add_acc)),'constant',constant_values = 0)  
            
        
        # Aggiungo padding ai from acc prima del filtraggio
        for i in range(3):
            streem_acc_from_acc[i].data = np.pad(streem_acc_from_acc[i].data,(int(pts2add_acc),int(pts2add_acc)),'constant',constant_values = 0)
             
        # Aggiungo padding ai fromvel prima del filtraggio
        for i in range(3):
            sig_vel_from_vel[i].data = np.pad(sig_vel_from_vel[i].data,(int(pts2add_vel),int(pts2add_vel)),'constant',constant_values = 0)
            noi_vel_from_vel[i].data = np.pad(noi_vel_from_vel[i].data,(int(pts2add_vel),int(pts2add_vel)),'constant',constant_values = 0)
            sig_acc_from_vel[i].data = np.pad(sig_acc_from_vel[i].data,(int(pts2add_vel),int(pts2add_vel)),'constant',constant_values = 0)
            noi_acc_from_vel[i].data = np.pad(noi_acc_from_vel[i].data,(int(pts2add_vel),int(pts2add_vel)),'constant',constant_values = 0)
        
         
        
        streem_acc_from_acc_bkup = copy_deepcopy(streem_acc_from_acc)
        streem_acc_from_acc_f = streem_acc_from_acc_bkup.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        
        sig_acc_from_acc_bkup = copy_deepcopy(sig_acc_from_acc)
        sig_acc_from_vel_bkup = copy_deepcopy(sig_acc_from_vel)      

        sig_vel_from_vel_bkup = copy_deepcopy(sig_vel_from_vel)
        sig_vel_from_acc_bkup = copy_deepcopy(sig_vel_from_acc) 
            
        sig_acc_from_acc_f = sig_acc_from_acc.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        sig_acc_from_vel_f = sig_acc_from_vel.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)        

        sig_vel_from_vel_f = sig_vel_from_vel.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        sig_vel_from_acc_f = sig_vel_from_acc.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        
        noi_acc_from_acc_f = noi_acc_from_acc.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        noi_acc_from_vel_f = noi_acc_from_vel.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
                                                    
        noi_vel_from_acc_f = noi_vel_from_acc.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        noi_vel_from_vel_f = noi_vel_from_vel.filter("bandpass", freqmin = fmin, freqmax = fmax, corners = order)
        
        
        # Rimuovo il padding dallo streem acc dopo il filtraggio
        for i in range(3):
            sig_acc_from_acc_f[i].data = sig_acc_from_acc_f[i].data[pts2add_acc:-pts2add_acc]
            noi_acc_from_acc_f[i].data = noi_acc_from_acc_f[i].data[pts2add_acc:-pts2add_acc]
            sig_vel_from_acc_f[i].data = sig_vel_from_acc_f[i].data[pts2add_acc:-pts2add_acc]
            noi_vel_from_acc_f[i].data = noi_vel_from_acc_f[i].data[pts2add_acc:-pts2add_acc]  
        
        
        
        
        # Rimuovo il padding dallo streem vel dopo il filtraggio
        for i in range(3):
            #streem_vel[i].data = streem_vel[i].data[pts2add_vel_streem:-pts2add_vel_streem]
            sig_vel_from_vel_f[i].data  = sig_vel_from_vel_f[i].data[pts2add_vel:-pts2add_vel]
            noi_vel_from_vel_f[i].data  = noi_vel_from_vel_f[i].data[pts2add_vel:-pts2add_vel]
            sig_acc_from_vel_f[i].data  = sig_acc_from_vel_f[i].data[pts2add_vel:-pts2add_vel]
            noi_acc_from_vel_f[i].data  = noi_acc_from_vel_f[i].data[pts2add_vel:-pts2add_vel]
        
        
        # Rimuovo il padding dallo streem acc dopo il filtraggio      
        for i in range(3): 
            streem_acc_from_acc_f[i].data = streem_acc_from_acc_f[i].data[pts2add_acc:-pts2add_acc] 
        
            
        
        
        if gen_fig:
            not_string = ""
            FigName14 = input_dir+'14_'+eventid+'.'+network + '.' + station +'_aa_av_deconv_filt.png'
            FigName15 = input_dir+'15_'+eventid+'.'+network + '.' + station +'_vv_va_deconv_filt.png'
            FigName16 = input_dir+'16_'+eventid+'.'+network + '.' + station +'_n_av_aa_deconv_filt.png'
            FigName17 = input_dir+'17_'+eventid+'.'+network + '.' + station +'_n_vv_va_deconv_filt.png'
            fig14, fig15, fig16, fig17 = subplot_obspy_deconv(sig_acc_from_acc_f,sig_acc_from_vel_f,sig_vel_from_acc_f,sig_vel_from_vel_f,\
            noi_acc_from_acc_f,noi_acc_from_vel_f,noi_vel_from_acc_f,noi_vel_from_vel_f,not_string,FigName14,FigName15,FigName16,FigName17)

                
        #Output parametri
        # # Parametri accelerazione grezzi
        pga_aa = np.array([sig_acc_from_acc_bkup.max()]).flatten()
        pga_av = np.array([sig_acc_from_vel_bkup.max()]).flatten()
        
        Ratiopga = np.abs(pga_aa/pga_av)
        NRatiopga = copy_deepcopy(Ratiopga)
        WNR = np.where(NRatiopga > 1.)
        NRatiopga[WNR] = 1./NRatiopga[WNR]
        
        # # Parametri accelerazione filtrati
        pga_aa_f = np.array([sig_acc_from_acc_f.max()]).flatten()
        pga_av_f = np.array([sig_acc_from_vel_f.max()]).flatten()
        
        Ratiopga_f = np.abs(pga_aa_f/pga_av_f)
        NRatiopga_f = copy_deepcopy(Ratiopga_f)
        WNR = np.where(NRatiopga_f > 1.)
        NRatiopga_f[WNR] = 1./NRatiopga_f[WNR]
        
        # # Parametri velocita' grezzi
        pgv_vv = np.array([sig_vel_from_vel_bkup.max()]).flatten()
        pgv_va = np.array([sig_vel_from_acc_bkup.max()]).flatten()
        
        Ratiopgv = np.abs(pgv_vv/pgv_va)
        NRatiopgv = copy_deepcopy(Ratiopgv)
        WNR = np.where(NRatiopgv > 1.)
        NRatiopgv[WNR] = 1./NRatiopgv[WNR]
        
        # # Parametri in velocita' filtrati
        pgv_vv_f = np.array([sig_vel_from_vel_f.max()]).flatten()
        pgv_va_f = np.array([sig_vel_from_acc_f.max()]).flatten()
        
        Ratiopgv_f = np.abs(pgv_vv_f/pgv_va_f)
        NRatiopgv_f = copy_deepcopy(Ratiopgv_f)
        WNR = np.where(NRatiopgv_f > 1.)
        NRatiopgv_f[WNR] = 1./NRatiopgv_f[WNR]
        

        # # Cross-correlazione segnali in accelerazione ottenuti dall'accelerometro e dal velocimetro per tutte le componenti
        cc_test_e = correlate_template(streem_acc_from_acc[0].decimate(factor = int(fnyq_acc / fnyq_vel)),sig_acc_from_vel[0],normalize='full')
        cc_test_n = correlate_template(streem_acc_from_acc[1].decimate(factor = int(fnyq_acc / fnyq_vel)),sig_acc_from_vel[1],normalize='full')
        cc_test_z = correlate_template(streem_acc_from_acc[2].decimate(factor = int(fnyq_acc / fnyq_vel)),sig_acc_from_vel[2],normalize='full')
        
        
        
        delay_z, value_z = xcorr_max(cc_test_z)
        delay_e, value_e = xcorr_max(cc_test_e)
        delay_n, value_n = xcorr_max(cc_test_n)
       
        if value_e > -0.95: delay_e, value_e = xcorr_max(cc_test_e, abs_max=False)
        if value_n > -0.95: delay_n, value_n = xcorr_max(cc_test_n, abs_max=False)
        if value_z > -0.95: delay_z, value_z = xcorr_max(cc_test_z, abs_max=False) 


        if verbose: print('Time lag_z = ', delay_z)
        if verbose: print('Max CC_z = ', value_z)

        if verbose: print('Time lag_e = ', delay_e)
        if verbose: print('Max CC_e = ', value_e)

        if verbose: print('Time lag_n = ', delay_n)
        if verbose: print('Max CC_n = ', value_n)

        # # Cross-correlazione segnali filtrati in accelerazione ottenuti dall'accelerometro e dal velocimetro per tutte le componenti
        cc_test_e_f = correlate_template(streem_acc_from_acc_f[0].decimate(factor = int(fnyq_acc / fnyq_vel)),sig_acc_from_vel_f[0],normalize='full')
        cc_test_n_f = correlate_template(streem_acc_from_acc_f[1].decimate(factor = int(fnyq_acc / fnyq_vel)),sig_acc_from_vel_f[1],normalize='full')
        cc_test_z_f = correlate_template(streem_acc_from_acc_f[2].decimate(factor = int(fnyq_acc / fnyq_vel)),sig_acc_from_vel_f[2],normalize='full')
        
        delay_z_f, value_z_f = xcorr_max(cc_test_z_f)
        delay_e_f, value_e_f = xcorr_max(cc_test_e_f)
        delay_n_f, value_n_f = xcorr_max(cc_test_n_f)
        if value_e_f > -0.95: delay_e_f, value_e_f = xcorr_max(cc_test_e_f, abs_max=False) 
        if value_n_f > -0.95: delay_n_f, value_n_f = xcorr_max(cc_test_n_f, abs_max=False) 
        if value_z_f > -0.95: delay_z_f, value_z_f = xcorr_max(cc_test_z_f, abs_max=False) 

            
        if verbose: print('Time lag_z_f = ', delay_z_f)
        if verbose: print('Max CC_z_f= ', value_z_f)

        if verbose: print('Time lag_e_f= ', delay_e_f)
        if verbose: print('Max CC_e_f= ', value_e_f)

        if verbose: print('Time lag_n_f = ', delay_n_f)
        if verbose: print('Max CC_n_f = ', value_n_f)
            
        cc_array = np.array([value_e, value_n, value_z])
        cc_array_f = np.array([value_e_f, value_n_f, value_z_f])
        
        vec_cc_rpga   = abs(cc_array) * NRatiopga
        vec_cc_rpga_f = abs(cc_array_f) * NRatiopga_f
                      
        vec_cc_rpgv   = abs(cc_array) * NRatiopgv
        vec_cc_rpgv_f = abs(cc_array_f) * NRatiopgv_f

        
          
        
        
        
        
        # # Condizione probabili spike (Norm_Ratio << 1 or  Norm_CC << 1)
        spike_val = 0.01
        if min(NRatiopga) < spike_val or min(NRatiopga_f) < spike_val or min(NRatiopgv) < spike_val or min(NRatiopgv_f) < spike_val \
        or min(vec_cc_rpga) < spike_val or min(vec_cc_rpga_f) < spike_val or min(vec_cc_rpgv) < spike_val or min(vec_cc_rpgv_f) < spike_val:
            err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: possible spike, plot the traces and verify on them'
            if verbose: print(err_var)
            Qlog.put(err_var)
            return 1, err_var, '', '', wrn_var            
                 
        # # Output 

        # ~ header_list = ['ID','Net','STATION','Channel','PGA_AA','PGA_AA_F','PGA_AV','PGA_AV_F','PGV_VV','PGV_VV_F','PGV_VA','PGV_VA_F','RPGA_AA/PGA_AV','RPGA_AAF/PGA_AVF','RPGV_VV/PGV_VA','RPGV_VVF/PGV_VAF','CC', 'CC_F','Repi','Mag','S/N_RMS']
        header_list = ['EVID','Date Time','NETCODE','STACODE','Stream_acc','Sensitivity_Acc','Stream_vel','Channel','PGA_AA','PGA_AA_F','PGA_AV','PGA_AV_F','PGV_VV','PGV_VV_F',\
        'PGV_VA','PGV_VA_F','Ratio_PGA_AA_AV','Ratio_PGA_AAF_AVF','Ratio_PGV_VV_VA','Ratio_PGV_VVF_VAF','Norm_Ratio_PGA_AA_AV','Norm_Ratio_PGA_AAF_AVF','Norm_Ratio_PGV_VV_VA','Norm_Ratio_PGV_VVF_VAF',\
        'CC', 'CCF','Prod_CC_Norm_Ratio_PGA_AA_AV','Prod_CCF_Norm_Ratio_PGA_AAF_AVF','Prod_CC_Norm_Ratio_PGV_VV_VA','Prod_CCF_Norm_Ratio_PGV_VVF_VAF','Repi','Ripo','Mag','Signal_to_Noise_RMS']
        for i in range(len(RINTe)):
            header_list.append('RINT_'+str(th_freq[i])+'_'+str(th_freq[i+1]))
        header_list.append('Qletter')
        header_list.append('fmin')
        header_list.append('fmax')

        header_string = ','.join(header_list)

        table_str = ''
        dict_enz = {'e': 0, 'n': 1, 'z': 2}
        for c_comp in ['e', 'n', 'z']:
            c_v = dict_enz[c_comp]
            c_list = [eventid,ot.strftime('%Y-%m-%d %H:%M:%S'),network,station,streem_acc[0].stats.channel[0:2],str(round(sensitivity_values_list[c_v])),\
            streem_vel[0].stats.channel[0:2], sig_acc_from_acc[c_v].stats.channel[-1],\
            "{:.5f}".format(np.abs(pga_aa[c_v])),\
            "{:.5f}".format(np.abs(pga_aa_f[c_v])),\
            "{:.5f}".format(np.abs(pga_av[c_v])),\
            "{:.5f}".format(np.abs(pga_av_f[c_v])),\
            "{:.6f}".format(np.abs(pgv_vv[c_v])),\
            "{:.6f}".format(np.abs(pgv_vv_f[c_v])),\
            "{:.6f}".format(np.abs(pgv_va[c_v])),\
            "{:.6f}".format(np.abs(pgv_va_f[c_v])),\
            "{:.2f}".format(Ratiopga[c_v]),\
            "{:.2f}".format(Ratiopga_f[c_v]),\
            "{:.2f}".format(Ratiopgv[c_v]),\
            "{:.2f}".format(Ratiopgv_f[c_v]),\
            "{:.2f}".format(NRatiopga[c_v]),\
            "{:.2f}".format(NRatiopga_f[c_v]),\
            "{:.2f}".format(NRatiopgv[c_v]),\
            "{:.2f}".format(NRatiopgv_f[c_v]),\
            "{:.3f}".format(cc_array[c_v]),\
            "{:.3f}".format(cc_array_f[c_v]),\
            "{:.3f}".format(vec_cc_rpga[c_v]),\
            "{:.3f}".format(vec_cc_rpga_f[c_v]),\
            "{:.3f}".format(vec_cc_rpgv[c_v]),\
            "{:.3f}".format(vec_cc_rpgv_f[c_v]),\
            "{:.1f}".format(epidist),\
            "{:.1f}".format(ipodist),\
            "{:.1f}".format(mag),\
            "{:.1f}".format(eval('ratio'+c_comp)),\
            ]
            for i in range(len(RINTe)):
                c_list.append("{:.1f}".format(eval('RINT'+c_comp+'[i]')))
            c_list.append("{:s}".format(Qletter[c_v]))
            c_list.append("{:.2f}".format(fmin))
            c_list.append("{:.1f}".format(fmax))
            c_string = ','.join(c_list) + '\n'
            table_str += c_string
        if verbose: print(header_string)
        if verbose: print(table_str)

        OK_str = '(' + eventid + ', ' + network + ', ' + station + ') OK: output table has been created'
        if countD > 0.5:
            tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: class is "D" for at least one component (E: ' + Qletter[0] + '; N: ' + Qletter[1] + '; Z: ' + Qletter[2] + ')'
            Qwrn.put(tmp_wrn_str)
            wrn_var.append(tmp_wrn_str)
        if min(cc_array_f) < 0:
            tmp_wrn_str = '(' + eventid + ', ' + network + ', ' + station + ') WARNING: orientation might be reversed between the accelerometric (' + streem_acc[0].stats.channel[0:2] + ') and velocimetric (' + streem_vel[0].stats.channel[0:2] + ') sensors (cross-correlation values, among filtered signals, is close to -1.0; (E: ' + str(cc_array_f[0]) + '; N: ' + str(cc_array_f[1]) + '; Z: ' + str(cc_array_f[2]) + ')'
            Qwrn.put(tmp_wrn_str)
            wrn_var.append(tmp_wrn_str)
        # ~ ratio between event and noise spectra integrals of the component ' + upper(i) + ' is lower than ' + str(th_class)
        Qlog.put(OK_str)
        Qcsv.put(table_str)
        Qhdr.put(header_string)
        return 0, OK_str, header_string, table_str, wrn_var

    except Exception as e: 

        err_code = 1
        err_var = '(' + eventid + ', ' + network + ', ' + station + ') ERROR: ' + ' '.join(line.strip() for line in str(e).splitlines())
        Qlog.put(err_var)
        return err_code, err_var, '', '', wrn_var


def required_length(nmin,nmax):
    class RequiredLength(argparse.Action):
        def __call__(self, parser, args, values, option_string=None):
            if not nmin<=len(values)<=nmax:
                msg='argument "{f}" requires between {nmin} and {nmax} arguments'.format(f=self.dest,nmin=nmin,nmax=nmax)
                raise argparse.ArgumentTypeError(msg)
            setattr(args, self.dest, values)
    return RequiredLength

            
if __name__ == '__main__':
    cli_exec_multi_event()


# ~ from multiprocessing import Pool, Queue

# ~ Q1 = Queue()
# ~ Q2 = Queue()

# ~ def co_refresh(a, b, c, d):
    # ~ Q1.put(a+b+c+d)
    # ~ if b == 'b1' or b == 'b4': Q2.put(a+b+'\n'+d)

# ~ num_lines = 10

# ~ input_list = [f'a{i} b{i} c{i} d{i}'.split() for i in range(num_lines)]
# ~ # [['a0', 'b0', 'c0', 'd0'], ['a1', 'b1', 'c1', 'd1'], ['a2', 'b2', 'c2', 'd2'], ['a3', 'b3', 'c3', 'd3']]

# ~ pool = Pool(processes=3)
# ~ pool.starmap(co_refresh, input_list)
# ~ pool.close()

# ~ for i in range(Q1.qsize()):
    # ~ print("result", i, ":", Q1.get())

# ~ for i in range(Q2.qsize()):
    # ~ print("result", i, ":", Q2.get())

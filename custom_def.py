import matplotlib
import matplotlib.pylab as plt
import matplotlib.dates as dates
import numpy as np
matplotlib.use('Agg')

def subplot_obspy_with_cut_def(streem_acc, streem_vel, startN, endN, Ptime, Etime, FigName):

    
    Title='Start-time: ' + str(streem_acc[0].stats.starttime).split('.')[0] + ' - End-time: ' + str(streem_acc[0].stats.endtime).split('.')[0]
    
    fig1, axs = plt.subplots(3, 2,figsize=(10,10))
    fig1.suptitle(Title)
    axs[0,0].plot(streem_acc[2].times("matplotlib"), streem_acc[2].data, "k-",label=str(streem_acc[2].stats.network+'.'+streem_acc[2].stats.station+'.'+streem_acc[2].stats.location+'.'+streem_acc[2].stats.channel))
    axs[0,0].xaxis_date()
    axs[0,0].set_ylabel('[counts]')
    axs[0,0].set_title('Accelerometer')
    axs[0,0].axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='r')
    axs[0,0].axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='r')
    axs[0,0].axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    axs[0,0].axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    leg = axs[0,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)
    axs[0,0].text(0.61,0.9,'event window', horizontalalignment='center',verticalalignment='center', transform=axs[0,0].transAxes,color='r')
    axs[0,0].text(0.2,0.6,'noise window', horizontalalignment='center',verticalalignment='center', transform=axs[0,0].transAxes,color='m')
    axs[1,0].plot(streem_acc[1].times("matplotlib"), streem_acc[1].data, "k-",label=str(streem_acc[1].stats.network+'.'+streem_acc[1].stats.station+'.'+streem_acc[1].stats.location+'.'+streem_acc[1].stats.channel))
    axs[1,0].xaxis_date()
    axs[1,0].set_ylabel('[counts]')
    axs[1,0].axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='r')
    axs[1,0].axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='r')
    axs[1,0].axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    axs[1,0].axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    leg= axs[1,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,0].text(0.61,0.9,'event window', horizontalalignment='center',verticalalignment='center', transform=axs[1,0].transAxes,color='r')
    axs[1,0].text(0.2,0.6,'noise window', horizontalalignment='center',verticalalignment='center', transform=axs[1,0].transAxes,color='m')
    axs[2,0].plot(streem_acc[0].times("matplotlib"), streem_acc[0].data, "k-",label=str(streem_acc[0].stats.network+'.'+streem_acc[0].stats.station+'.'+streem_acc[0].stats.location+'.'+streem_acc[0].stats.channel))
    axs[2,0].xaxis_date()
    axs[2,0].set_ylabel('[counts]')
    axs[2,0].set_xlabel('time (UTC)')
    axs[2,0].axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='r')
    axs[2,0].axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='r')
    axs[2,0].axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    axs[2,0].axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    leg= axs[2,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,0].text(0.61,0.9,'event window', horizontalalignment='center',verticalalignment='center', transform=axs[2,0].transAxes,color='r')
    axs[2,0].text(0.2,0.6,'noise window', horizontalalignment='center',verticalalignment='center', transform=axs[2,0].transAxes,color='m')
    axs[0,1].plot(streem_vel[2].times("matplotlib"), streem_vel[2].data, "k-",label=str(streem_vel[2].stats.network+'.'+streem_vel[2].stats.station+'.'+streem_vel[2].stats.location+'.'+streem_vel[2].stats.channel))
    axs[0,1].xaxis_date()
    axs[0,1].set_title('Velocimeter')
    axs[0,1].set_ylabel('[counts]')
    axs[0,1].axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='r')
    axs[0,1].axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='r')
    axs[0,1].axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    axs[0,1].axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    leg= axs[0,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[0,1].text(0.61,0.9,'event window', horizontalalignment='center',verticalalignment='center', transform=axs[0,1].transAxes,color='r')
    axs[0,1].text(0.2,0.6,'noise window', horizontalalignment='center',verticalalignment='center', transform=axs[0,1].transAxes,color='m')
    axs[1,1].plot(streem_vel[1].times("matplotlib"), streem_vel[1].data, "k-",label=str(streem_vel[1].stats.network+'.'+streem_vel[1].stats.station+'.'+streem_vel[1].stats.location+'.'+streem_vel[1].stats.channel))
    axs[1,1].xaxis_date()
    axs[1,1].set_ylabel('[counts]')
    axs[1,1].axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='r')
    axs[1,1].axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='r')
    axs[1,1].axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    axs[1,1].axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    leg= axs[1,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,1].text(0.61,0.9,'event window', horizontalalignment='center',verticalalignment='center', transform=axs[1,1].transAxes,color='r')
    axs[1,1].text(0.2,0.6,'noise window', horizontalalignment='center',verticalalignment='center', transform=axs[1,1].transAxes,color='m')
    axs[2,1].plot(streem_vel[0].times("matplotlib"), streem_vel[0].data, "k-",label=str(streem_vel[0].stats.network+'.'+streem_vel[0].stats.station+'.'+streem_vel[0].stats.location+'.'+streem_vel[0].stats.channel))
    axs[2,1].xaxis_date()
    axs[2,1].set_xlabel('time (UTC)')
    axs[2,1].set_ylabel('[counts]')
    axs[2,1].axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='r')
    axs[2,1].axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='r')
    axs[2,1].axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    axs[2,1].axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    leg= axs[2,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,1].text(0.61,0.9,'event window', horizontalalignment='center',verticalalignment='center', transform=axs[2,1].transAxes,color='r')    
    axs[2,1].text(0.2,0.6,'noise window', horizontalalignment='center',verticalalignment='center', transform=axs[2,1].transAxes,color='m')
    fig1.autofmt_xdate()
    fig1.tight_layout()
    fig1.savefig(FigName, dpi = 300)
    fig1.clear()
    plt.close(fig1)
    return 0

# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

def subplot_obspy_deconv(sig_acc_from_acc,sig_acc_from_vel,sig_vel_from_acc,sig_vel_from_vel,noi_acc_from_acc,noi_acc_from_vel,noi_vel_from_acc,noi_vel_from_vel,not_string,FigName_1,FigName_2,FigName_3,FigName_4):
    
    Title = 'Start-time: ' + str(sig_acc_from_vel[0].stats.starttime).split('.')[0] + ' - End-time: ' + str(sig_acc_from_vel[0].stats.endtime).split('.')[0]
    fig1, axs = plt.subplots(3, 2,figsize=(10,10))
    fig1.suptitle(Title)
    axs[0,0].plot(sig_acc_from_acc[2].times("matplotlib"), sig_acc_from_acc[2].data, "k-",label=str(sig_acc_from_acc[2].stats.network+'.'+\
    sig_acc_from_acc[2].stats.station+'.'+sig_acc_from_acc[2].stats.location+'.'+sig_acc_from_acc[2].stats.channel))
    axs[0,0].xaxis_date()
    axs[0,0].set_ylabel('[$m/s^2$]')
    axs[0,0].set_title('Acc from Acc ' + not_string + 'filtered')
    leg= axs[0,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,0].plot(sig_acc_from_acc[1].times("matplotlib"), sig_acc_from_acc[1].data, "k-",label=str(sig_acc_from_acc[1].stats.network+'.'+\
    sig_acc_from_acc[1].stats.station+'.'+sig_acc_from_acc[1].stats.location+'.'+sig_acc_from_acc[1].stats.channel))
    axs[1,0].set_ylabel('[$m/s^2$]')
    axs[1,0].xaxis_date()
    leg= axs[1,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,0].plot(sig_acc_from_acc[0].times("matplotlib"), sig_acc_from_acc[0].data, "k-",label=str(sig_acc_from_acc[0].stats.network+'.'+\
    sig_acc_from_acc[0].stats.station+'.'+sig_acc_from_acc[0].stats.location+'.'+sig_acc_from_acc[0].stats.channel))
    axs[2,0].set_xlabel('time (UTC)')
    axs[2,0].set_ylabel('[$m/s^2$]')
    axs[2,0].xaxis_date()
    leg= axs[2,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[0,1].plot(sig_acc_from_vel[2].times("matplotlib"), sig_acc_from_vel[2].data, "k-",label=str(sig_acc_from_vel[2].stats.network+'.'+\
    sig_acc_from_vel[2].stats.station+'.'+sig_acc_from_vel[2].stats.location+'.'+sig_acc_from_vel[2].stats.channel))
    axs[0,1].set_ylabel('[$m/s^2$]')
    axs[0,1].xaxis_date()
    axs[0,1].set_title('Acc from Vel ' + not_string + 'filtered')
    leg= axs[0,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,1].plot(sig_acc_from_vel[1].times("matplotlib"), sig_acc_from_vel[1].data, "k-",label=str(sig_acc_from_vel[1].stats.network+'.'+\
    sig_acc_from_vel[1].stats.station+'.'+sig_acc_from_vel[1].stats.location+'.'+sig_acc_from_vel[1].stats.channel))
    axs[1,1].set_ylabel('[$m/s^2$]')
    axs[1,1].xaxis_date()
    leg= axs[1,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,1].plot(sig_acc_from_vel[0].times("matplotlib"), sig_acc_from_vel[0].data, "k-",label=str(sig_acc_from_vel[0].stats.network+'.'+\
    sig_acc_from_vel[0].stats.station+'.'+sig_acc_from_vel[0].stats.location+'.'+sig_acc_from_vel[0].stats.channel))
    axs[2,1].set_ylabel('[$m/s^2$]')
    axs[2,1].set_xlabel('time (UTC)')
    axs[2,1].xaxis_date()
    leg= axs[2,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    fig1.autofmt_xdate()
    fig1.tight_layout()
     
    Title='Start-time: ' + str(sig_vel_from_vel[0].stats.starttime).split('.')[0] + ' - End-time: ' + str(sig_vel_from_vel[0].stats.endtime).split('.')[0]
    fig2, axs = plt.subplots(3, 2,figsize=(10,10))
    fig2.suptitle(Title)
    axs[0,0].plot(sig_vel_from_acc[2].times("matplotlib"), sig_vel_from_acc[2].data, "k-",label=str(sig_vel_from_acc[2].stats.network+'.'+\
    sig_vel_from_acc[2].stats.station+'.'+sig_vel_from_acc[2].stats.location+'.'+sig_vel_from_acc[2].stats.channel))
    axs[0,0].set_ylabel('[m/s]')
    axs[0,0].xaxis_date()
    axs[0,0].set_title('Vel from Acc ' + not_string + 'filtered')
    leg= axs[0,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,0].plot(sig_vel_from_acc[1].times("matplotlib"), sig_vel_from_acc[1].data, "k-",label=str(sig_vel_from_acc[1].stats.network+'.'+\
    sig_vel_from_acc[1].stats.station+'.'+sig_vel_from_acc[1].stats.location+'.'+sig_vel_from_acc[1].stats.channel))
    axs[1,0].set_ylabel('[m/s]')
    axs[1,0].xaxis_date()
    leg= axs[1,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,0].plot(sig_vel_from_acc[0].times("matplotlib"), sig_vel_from_acc[0].data, "k-",label=str(sig_vel_from_acc[0].stats.network+'.'+\
    sig_vel_from_acc[0].stats.station+'.'+sig_vel_from_acc[0].stats.location+'.'+sig_vel_from_acc[0].stats.channel))
    axs[2,0].set_ylabel('[m/s]')
    axs[2,0].set_xlabel('time (UTC)')
    axs[2,0].xaxis_date()
    leg= axs[2,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[0,1].plot(sig_vel_from_vel[2].times("matplotlib"), sig_vel_from_vel[2].data, "k-",label=str(sig_vel_from_vel[2].stats.network+'.'+\
    sig_vel_from_vel[2].stats.station+'.'+sig_vel_from_vel[2].stats.location+'.'+sig_vel_from_vel[2].stats.channel))
    axs[0,1].set_ylabel('[m/s]')
    axs[0,1].xaxis_date()
    axs[0,1].set_title('Vel from Vel ' + not_string + 'filtered')
    leg= axs[0,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,1].plot(sig_vel_from_vel[1].times("matplotlib"), sig_vel_from_vel[1].data, "k-",label=str(sig_vel_from_vel[1].stats.network+'.'+\
    sig_vel_from_vel[1].stats.station+'.'+sig_vel_from_vel[1].stats.location+'.'+sig_vel_from_vel[1].stats.channel))
    axs[1,1].set_ylabel('[m/s]')
    axs[1,1].xaxis_date()
    leg= axs[1,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,1].plot(sig_vel_from_vel[0].times("matplotlib"), sig_vel_from_vel[0].data, "k-",label=str(sig_vel_from_vel[0].stats.network+'.'+\
    sig_vel_from_vel[0].stats.station+'.'+sig_vel_from_vel[0].stats.location+'.'+sig_vel_from_vel[0].stats.channel))
    axs[2,1].set_xlabel('time (UTC)')
    axs[2,1].set_ylabel('[m/s]')
    axs[2,1].xaxis_date()
    leg= axs[2,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    fig2.autofmt_xdate()
    fig2.tight_layout()
        
    
    Title='Start-time: ' + str(noi_acc_from_vel[0].stats.starttime).split('.')[0] + ' - End-time: ' + str(noi_acc_from_vel[0].stats.endtime).split('.')[0]
    fig3, axs = plt.subplots(3, 2,figsize=(10,10))
    fig3.suptitle(Title)
    axs[0,0].plot(noi_acc_from_acc[2].times("matplotlib"), noi_acc_from_acc[2].data, "k-",label=str(noi_acc_from_acc[2].stats.network+'.'+\
    noi_acc_from_acc[2].stats.station+'.'+noi_acc_from_acc[2].stats.location+'.'+noi_acc_from_acc[2].stats.channel))
    axs[0,0].set_ylabel('[$m/s^2$]')
    axs[0,0].xaxis_date()
    axs[0,0].set_title('Noise Acc from Acc ' + not_string + 'filtered')
#axs[0,0].legend(loc='upper left')
    leg= axs[0,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,0].plot(noi_acc_from_acc[1].times("matplotlib"), noi_acc_from_acc[1].data, "k-",label=str(noi_acc_from_acc[1].stats.network+'.'+\
    noi_acc_from_acc[1].stats.station+'.'+noi_acc_from_acc[1].stats.location+'.'+noi_acc_from_acc[1].stats.channel))
    axs[1,0].set_ylabel('[$m/s^2$]')
    axs[1,0].xaxis_date()
#axs[1,0].legend(loc='upper left')
    leg= axs[1,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,0].plot(noi_acc_from_acc[0].times("matplotlib"), noi_acc_from_acc[0].data, "k-",label=str(noi_acc_from_acc[0].stats.network+'.'+\
    noi_acc_from_acc[0].stats.station+'.'+noi_acc_from_acc[0].stats.location+'.'+noi_acc_from_acc[0].stats.channel))
    axs[2,0].set_xlabel('time (UTC)')
    axs[2,0].set_ylabel('[$m/s^2$]')
    axs[2,0].xaxis_date()
#axs[2,0].legend(loc='upper left')
    leg= axs[2,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[0,1].plot(noi_acc_from_vel[2].times("matplotlib"), noi_acc_from_vel[2].data, "k-",label=str(noi_acc_from_vel[2].stats.network+'.'+\
    noi_acc_from_vel[2].stats.station+'.'+noi_acc_from_vel[2].stats.location+'.'+noi_acc_from_vel[2].stats.channel))
    axs[0,1].set_ylabel('[$m/s^2$]')
    axs[0,1].xaxis_date()
    axs[0,1].set_title('Noise Acc from Vel ' + not_string + 'filtered')
#axs[0,1].legend(loc='upper left')
    leg= axs[0,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,1].plot(noi_acc_from_vel[1].times("matplotlib"), noi_acc_from_vel[1].data, "k-",label=str(noi_acc_from_vel[1].stats.network+'.'+\
    noi_acc_from_vel[1].stats.station+'.'+noi_acc_from_vel[1].stats.location+'.'+noi_acc_from_vel[1].stats.channel))
    axs[1,1].set_ylabel('[$m/s^2$]')
    axs[1,1].xaxis_date()
#axs[1,1].legend(loc='upper left')
    leg= axs[1,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,1].plot(noi_acc_from_vel[0].times("matplotlib"), noi_acc_from_vel[0].data, "k-",label=str(noi_acc_from_vel[0].stats.network+'.'+\
    noi_acc_from_vel[0].stats.station+'.'+noi_acc_from_vel[0].stats.location+'.'+noi_acc_from_vel[0].stats.channel))
    axs[2,1].set_xlabel('time (UTC)')
    axs[2,1].set_ylabel('[$m/s^2$]')
    axs[2,1].xaxis_date()
#axs[2,1].legend(loc='upper left')
    leg= axs[2,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False) 
    fig3.autofmt_xdate()
    fig3.tight_layout()
    
    Title='Start-time: ' + str(noi_vel_from_vel[0].stats.starttime).split('.')[0] + ' - End-time: ' + str(noi_vel_from_vel[0].stats.endtime).split('.')[0]
    fig4, axs = plt.subplots(3, 2,figsize=(10,10))
    fig4.suptitle(Title)
    axs[0,0].plot(noi_vel_from_acc[2].times("matplotlib"), noi_vel_from_acc[2].data, "k-",label=str(noi_vel_from_acc[2].stats.network+'.'+\
    noi_vel_from_acc[2].stats.station+'.'+noi_vel_from_acc[2].stats.location+'.'+noi_vel_from_acc[2].stats.channel))
    axs[0,0].set_ylabel('[m/s]')
    axs[0,0].xaxis_date()
    axs[0,0].set_title('Noise Vel from Acc ' + not_string + 'filtered')
#axs[0,0].legend(loc='upper left')
    leg= axs[0,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,0].plot(noi_vel_from_acc[1].times("matplotlib"), noi_vel_from_acc[1].data, "k-",label=str(noi_vel_from_acc[1].stats.network+'.'+\
    noi_vel_from_acc[1].stats.station+'.'+noi_vel_from_acc[1].stats.location+'.'+noi_vel_from_acc[1].stats.channel))
    axs[1,0].set_ylabel('[m/s]')
    axs[1,0].xaxis_date()
#axs[1,0].legend(loc='upper left')
    leg= axs[1,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,0].plot(noi_vel_from_acc[0].times("matplotlib"), noi_vel_from_acc[0].data, "k-",label=str(noi_vel_from_acc[0].stats.network+'.'+\
    noi_vel_from_acc[0].stats.station+'.'+noi_vel_from_acc[0].stats.location+'.'+noi_vel_from_acc[0].stats.channel))
    axs[2,0].set_xlabel('time (UTC)')
    axs[2,0].set_ylabel('[m/s]')
    axs[2,0].xaxis_date()
#axs[2,0].legend(loc='upper left')
    leg= axs[2,0].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[0,1].plot(noi_vel_from_vel[2].times("matplotlib"), noi_vel_from_vel[2].data, "k-",label=str(noi_vel_from_vel[2].stats.network+'.'+\
    noi_vel_from_vel[2].stats.station+'.'+noi_vel_from_vel[2].stats.location+'.'+noi_vel_from_vel[2].stats.channel))
    axs[0,1].set_xlabel('time (UTC)')
    axs[0,1].set_ylabel('[m/s]')
    axs[0,1].xaxis_date()
    axs[0,1].set_title('Noise Vel from Vel ' + not_string + 'filtered')
#axs[0,1].legend(loc='upper left')
    leg= axs[0,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[1,1].plot(noi_vel_from_vel[1].times("matplotlib"), noi_vel_from_vel[1].data, "k-",label=str(noi_vel_from_vel[1].stats.network+'.'+\
    noi_vel_from_vel[1].stats.station+'.'+noi_vel_from_vel[1].stats.location+'.'+noi_vel_from_vel[1].stats.channel))
    axs[1,1].set_ylabel('[m/s]')
    axs[1,1].xaxis_date()
#axs[1,1].legend(loc='upper left')
    leg= axs[1,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)  
    axs[2,1].plot(noi_vel_from_vel[0].times("matplotlib"), noi_vel_from_vel[0].data, "k-",label=str(noi_vel_from_vel[0].stats.network+'.'+\
    noi_vel_from_vel[0].stats.station+'.'+noi_vel_from_vel[0].stats.location+'.'+noi_vel_from_vel[0].stats.channel))
    axs[2,1].set_xlabel('time (UTC)')
    axs[2,1].set_ylabel('[m/s]')
    axs[2,1].xaxis_date()
#axs[2,1].legend(loc='upper left')
    leg= axs[2,1].legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False) 
    fig4.autofmt_xdate()
    fig4.tight_layout()

    fig1.savefig(FigName_1, dpi = 300)
    fig2.savefig(FigName_2, dpi = 300)
    fig3.savefig(FigName_3, dpi = 300)
    fig4.savefig(FigName_4, dpi = 300)
    fig1.clear()
    fig2.clear()
    fig3.clear()
    fig4.clear()
    plt.close(fig1)    
    plt.close(fig2)
    plt.close(fig3)
    plt.close(fig4)

    return 0, 0, 0, 0

# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

def spectrum_amp_smooth(freq_vel,aaz,avz,aaz_n,avz_n,aan,avn,aan_n,avn_n,aae,ave,aae_n,ave_n,FigName,Ftitle):
    fig, axs = plt.subplots(3, 2,figsize=(10,10))
    fig.suptitle(Ftitle)
    axs[0,0].loglog(freq_vel,aaz,'r',label = "Acc from Acc (AA)")
    axs[0,0].loglog(freq_vel, avz, 'r--',label = "Acc from Vel (AV)")
    axs[0,0].loglog(freq_vel,aaz_n, 'k',label=" Noise - Acc from Acc  (NAA)") 
    axs[0,0].loglog(freq_vel, avz_n, 'k--',label = " Noise - Acc from Vel (NAV)")
    # ~ axs[0,0].legend(loc='upper left')
    axs[0,0].set_ylabel('FAS-HNZ')
    plt.text(1, 1, 'left title', transform=fig.transFigure, horizontalalignment='center') 
    axs[1,0].loglog(freq_vel,aan,'r')
    axs[1,0].loglog(freq_vel,avn,'r--')
    axs[1,0].loglog(freq_vel,aan_n, 'k')
    axs[1,0].loglog(freq_vel,avn_n, 'k--')
    axs[1,0].set_ylabel('FAS-HNN')
    axs[2,0].loglog(freq_vel,aae,'r')
    axs[2,0].loglog(freq_vel,ave,'r--')
    axs[2,0].loglog(freq_vel,aae_n, 'k') 
    axs[2,0].loglog(freq_vel,ave_n, 'k--')
    axs[2,0].set_xlabel('frequency [Hz]')
    axs[2,0].set_ylabel('FAS-HNE')
    axs[0,1].loglog(freq_vel,aaz/aaz_n,'C1',label = "Ratio AA/NAA")
    axs[0,1].loglog(freq_vel, avz / avz_n, 'C1--', label = "Ratio AV/NAV")
    axs[0,1].loglog(freq_vel, aaz / avz, 'g', label = "Ratio AA/AV")
    axs[0,1].set_ylabel('RATIO ACC ON NOISE-COMPONENT Z')
    axs[0,1].axhline(10**0,linewidth=1.5,color='g',linestyle='--')
    # ~ axs[0,1].legend(loc='upper left')
    plt.text(1, 1, 'left title', transform=fig.transFigure, horizontalalignment='center') 
    axs[1,1].loglog(freq_vel,aan/aan_n,'C1')
    axs[1,1].loglog(freq_vel,avn/avn_n,'C1--')
    axs[1,1].loglog(freq_vel, aan / avn, 'g')
    axs[1,1].axhline(10**0,linewidth=1.5,color='g',linestyle='--')
    axs[1,1].set_ylabel('RATIO ACC ON NOISE-COMPONENT N')
    axs[2,1].loglog(freq_vel,aae/aae_n,'C1')
    axs[2,1].loglog(freq_vel,ave/ave_n,'C1--')
    axs[2,1].loglog(freq_vel,aae/ave,'g')
    axs[2,1].axhline(10**0,linewidth=1.5,color='g',linestyle='--')
    axs[2,1].set_xlabel('frequency [Hz]')
    axs[2,1].set_ylabel('RATIO ACC ON NOISE-COMPONENT E')
    plt.tight_layout()
    fig.legend(ncol=4,loc='lower center', bbox_to_anchor=(0.5, 0.0))
    fig.savefig(FigName, dpi = 300)
    fig.clear()
    plt.close(fig)
    
    return 0

# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

def envelope_plot(streem_acc_from_acc_enve,enve_streem_e,enve_streem_n,enve_streem_z,Ptime, Etime,startN, endN, FigName,Ftitle):
    fig,(ax1,ax2,ax3) = plt.subplots(3,1, figsize=(7,10))
    fig.suptitle(Ftitle + ' - Accelerometer Envelope')
    ax1.plot(streem_acc_from_acc_enve[0].times('matplotlib'),streem_acc_from_acc_enve[0].data,'k')
    ax1.plot(streem_acc_from_acc_enve[0].times('matplotlib'),enve_streem_e,'r-')
    ax1.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g')
    ax1.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='g')
    ax1.axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    ax1.axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    ax1.set_title('HNE')
    ax1.set_ylabel('[$m/s^2$]')
    ax1.xaxis_date()
    ax2.plot(streem_acc_from_acc_enve[1].times('matplotlib'),streem_acc_from_acc_enve[1].data,'k')
    ax2.plot(streem_acc_from_acc_enve[1].times('matplotlib'),enve_streem_n,'r-')
    ax2.set_title('HNN')
    ax2.set_ylabel('[$m/s^2$]')
    ax2.xaxis_date()
    ax2.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g')
    ax2.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='g')
    ax2.axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    ax2.axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    ax3.plot(streem_acc_from_acc_enve[2].times('matplotlib'),streem_acc_from_acc_enve[2].data,'k')
    ax3.plot(streem_acc_from_acc_enve[2].times('matplotlib'),enve_streem_z,'r-')
    ax3.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g')
    ax3.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='g')
    ax3.axvline(x=dates.date2num(startN.datetime),linewidth=2,c='m')
    ax3.axvline(x=dates.date2num(endN.datetime),linewidth=2,c='m')
    ax3.set_title('HNZ')
    ax3.set_xlabel('time (UTC)')
    ax3.set_ylabel('[$m/s^2$]')
    ax3.xaxis_date()
    fig.autofmt_xdate()
    fig.tight_layout()
    fig.savefig(FigName, dpi = 300)
    fig.clear()
    plt.close(fig)    
    return 0
    
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

def spectrum_rms_integral(freq_vel,aaz,aaz_n,aan,aan_n,aae,aae_n,th_freq,FigName,Ftitle):
    fig,(ax1,ax2,ax3) = plt.subplots(3,1, figsize=(7,10))
    fig.suptitle(Ftitle)
    ax1.loglog(freq_vel, aaz,'r', label = "Acc from Acc (AA)")
    ax1.loglog(freq_vel, aaz_n, 'k', label = "Noise - Acc from Acc  (NAA)")
    for th in th_freq:
            ax1.axvline(th,c ='0.8') # noise-acc from SM
    ax1.legend(loc = 'best')
    ax1.set_ylabel('FAS-HNZ')
    ax2.loglog(freq_vel, aan,'r', label = "Acc from Acc (AA)")
    ax2.loglog(freq_vel, aan_n, 'k' , label = "Noise - Acc from Acc  (NAA)")
    for th in th_freq:
            ax2.axvline(th,c='0.8')
    ax2.legend(loc = 'best')
    ax2.set_ylabel('FAS-HNN')
    ax3.loglog(freq_vel, aae,'r', label = "Acc from Acc (AA)") 
    ax3.loglog(freq_vel, aae_n, 'k', label = "Noise - Acc from Acc  (NAA)")
    for th in th_freq:
            ax3.axvline(th,c = '0.8')# noise-acc from SM
    ax3.legend(loc = 'best')
    ax3.set_xlabel('frequency [Hz]')
    ax3.set_ylabel('FAS-HNE')
    fig.tight_layout()
    fig.savefig(FigName, dpi = 300)        
    fig.clear()
    plt.close(fig)
    return 0

# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

def spectrum_with_bar_def(freq_vel,fmin,fmax,aaz,aaz_n,aan,aan_n,aae,aae_n,vvz,vvz_n,vvn,vvn_n,vve,vve_n,FigName,Ftitle):

    fig, axs = plt.subplots(3, 2,figsize=(10,10))
    fig.suptitle(Ftitle)
    axs[0,0].loglog(freq_vel,aaz,'r')
    axs[0,0].loglog(freq_vel,aaz_n, 'k') # noise-acc from SM
    axs[0,0].axvline(x=fmin,linewidth=2,c='g',label="fmin")
    axs[0,0].axvline(x=fmax,linewidth=2,c='0.8',label="fmax")
    axs[0,0].legend(loc='best')
    axs[0,0].set_ylabel('FAS-HNZ')
    plt.text(1, 1, 'left title', transform=fig.transFigure, horizontalalignment='center') 
    axs[1,0].loglog(freq_vel,aan,'r')
    axs[1,0].loglog(freq_vel,aan_n, 'k')
    axs[1,0].axvline(x=fmin,linewidth=2,c='g',label="fmin")
    axs[1,0].axvline(x=fmax,linewidth=2,c='0.8',label="fmax")
    axs[1,0].legend(loc='best')
    axs[1,0].set_ylabel('FAS-HNN')
    axs[2,0].loglog(freq_vel,aae,'r')
    axs[2,0].loglog(freq_vel,aae_n, 'k') # noise-acc from SM
    axs[2,0].axvline(x=fmin,linewidth=2,c='g',label="fmin")
    axs[2,0].axvline(x=fmax,linewidth=2,c='0.8',label="fmax")
    axs[2,0].legend(loc='best')
    axs[2,0].set_ylabel('FAS-HNE')
    axs[2,0].set_xlabel('frequency [Hz]')
    axs[0,1].loglog(freq_vel,vvz,'r')
    axs[0,1].loglog(freq_vel,vvz_n, 'k') # noise-acc from SM
    axs[0,1].axvline(x=fmin,linewidth=2,c='g',label="fmin")
    axs[0,1].axvline(x=fmax,linewidth=2,c='0.8',label="fmax")
    axs[0,1].legend(loc='upper left')
    axs[0,1].set_ylabel('FAS-HHZ')
    plt.text(1, 1, 'left title', transform=fig.transFigure, horizontalalignment='center') 
    axs[1,1].loglog(freq_vel,vvn,'r')
    axs[1,1].loglog(freq_vel,vvn_n, 'k')
    axs[1,1].axvline(x=fmin,linewidth=2,c='g',label="fmin")
    axs[1,1].axvline(x=fmax,linewidth=2,c='0.8',label="fmax")
    axs[1,1].legend(loc='upper left')
    axs[1,1].set_ylabel('FAS-HHN')
    axs[2,1].loglog(freq_vel,vve, 'r') # noise-acc from SM
    axs[2,1].loglog(freq_vel,vve_n, 'k') # noise-acc from SM
    axs[2,1].axvline(x=fmin,linewidth=2,c='g',label="fmin")  
    axs[2,1].axvline(x=fmax,linewidth=2,c='0.8',label="fmax")
    axs[2,1].legend(loc='upper left')
    axs[2,1].set_xlabel('frequency [Hz]')
    axs[2,1].set_ylabel('FAS-HHE')
    fig.tight_layout()
    fig.savefig(FigName, dpi = 300)
    fig.clear()
    plt.close(fig)
    return 0

# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

# ~ [LPGAg, Sigma] = ITA10_PGA(LONG, LAT, M, Rjb, SUOLO)                                                     
# ~ La funzione calcola la PGA attraverso la legge di attenuazione ITA10

# ~ LONG, LAT Rjb sono: la longitudine, la latitudine e la distanza Joyner-Boore
# ~ dalla faglia dell'elemento del quale si vuole conoscere la PGA.
# ~ SUOLO è il vettore dell'ID della tipologia del suolo (1=Suolo A; 2=Suolo
# ~ B; 3=Suolo C; 4=Suolo D; 5=Suolo E
# ~ LPGAg     mediana PGA in g
# ~ Sigma     deviazione standard logaritmica PGA

# ~ Soil = ['A','B','C','D','E']


def ITA10(mag,epidist,Soil):

    e1 = 3.672
    c1 = -1.94
    c2 = 0.413
    h  = 10.322
    c3 = 0.000134
    b1 =-0.262
    b2 =-0.0707
    sA = 0
    sB = 0.162
    sC = 0.24
    sD = 0.105
    sE = 0.57
    f1 = -0.0503
    Mref = 5
    Mh = 6.75
    Rref =1

    S_Inter=0.172
    S_Intra=0.290

    # Funzione sulla distanza

    FD = 0

    FD = ((c1+c2*(mag-Mref))*np.log10((np.sqrt((epidist)**2+h**2))/Rref)-c3*(np.sqrt((epidist)**2+h**2)-Rref))

    # Funzione sulla magnitudo
    FM = 0

    FM=(b1*(mag-Mh)+b2*((mag-Mh)**2))
            
    # Funzione stile faglia

    Fsof=f1
  
    if Soil== 1:
        LPGAms2=(e1+FD+FM+Fsof+sA)-np.log10(100)
    elif Soil== 2:
        LPGAms2=(e1+FD+FM+Fsof+sB)-np.log10(100)
    elif Soil== 3:                     
        LPGAms2=(e1+FD+FM+Fsof+sC)-np.log10(100)
    elif Soil== 4:                     
        LPGAms2=(e1+FD+FM+Fsof+sD)-np.log10(100)
    elif Soil ==5:
        LPGAms2=(e1+FD+FM+Fsof+sE)-np.log10(100) 
    else:
        return [0,0,0]      


    LPGAg=LPGAms2-np.log10(9.81)
    Sigma=(S_Inter**2+S_Intra**2)**0.5    

    expSigma=10**Sigma
     
    PGAms2 = 10**(LPGAms2)  
    # ~ print('PGAms2',PGAms2)
    # ~ print('Sigma', expSigma)
    # ~ print(PGAms2*expSigma)
    # ~ print(PGAms2/expSigma)
    
    # ~ PGAms2_minus_sigma = PGAms2/expSigma
    # ~ PGAms2_major_sigma = PGAms2*expSigma


    # ~ return [PGAms2_minus_sigma, PGAms2 , PGAms2_major_sigma]
    return [PGAms2, expSigma]

# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

def ITA10_plot(vec_dist,stats_array,epidist,pga_gmean,FigName):
    sigma = stats_array[0][1]
    PGA_stats= stats_array[:,0]
    PGA_minus_stats = PGA_stats/sigma 
    PGA_major_stats = PGA_stats*sigma
    PGA_minus_stats_3 = PGA_stats/(3*sigma)
    PGA_major_stats_3 = PGA_stats*3*sigma
    fig = plt.figure()
    plt.loglog(vec_dist,PGA_minus_stats,'r--')
    plt.loglog(vec_dist,PGA_stats,'r')
    plt.loglog(vec_dist,PGA_major_stats,'r--')
    plt.loglog(vec_dist,PGA_minus_stats_3,'b--')
    plt.loglog(vec_dist,PGA_stats,'r')
    plt.loglog(vec_dist,PGA_major_stats_3,'b--')
    plt.scatter(epidist,float(pga_gmean),c='g',marker='o')
    plt.xlabel('Distance [Km]')
    plt.ylabel('PGA[$m/s^2$]')
    plt.title('Bindi Attenuation Law (ITA10)')
    fig.savefig(FigName , dpi = 300)
    fig.clear()
    plt.close(fig)
    return 0
    
    
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

    
def Arias_Intensity(streem_acc_from_acc,dt_acc):
    pi = np.pi
    const = pi/(2*9.81)
    Int_cumsum = np.cumsum((streem_acc_from_acc[2].data**2*dt_acc))
    I_ar_Z = const * Int_cumsum
    I_ar_Z_nor = I_ar_Z/max(I_ar_Z )
    return I_ar_Z_nor
    
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------
# # ------------------------------------------------------------

    
def Arias_plot(streem_acc_from_acc,I_ar_Z_nor,Ptime,Etime,FigName):
    t5_interp = np.interp(0.05,I_ar_Z_nor,streem_acc_from_acc[2].times("matplotlib"))
    t95_interp = np.interp(0.95,I_ar_Z_nor,streem_acc_from_acc[2].times("matplotlib"))
    fig,(ax1,ax2) = plt.subplots(2,1)
    ax1.plot(streem_acc_from_acc[2].times("matplotlib"), streem_acc_from_acc[2].data, "k-",label=str(streem_acc_from_acc[2].stats.network+'.'+\
    streem_acc_from_acc[2].stats.station+'.'+streem_acc_from_acc[2].stats.location+'.'+streem_acc_from_acc[2].stats.channel))
    ax1.axvline(t5_interp,c='m', linestyle='--')
    ax1.axvline(t95_interp,c='m', linestyle='--')
    ax1.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g')
    ax1.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='b')
    leg = ax1.legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)
    ax1.xaxis_date()
    ax1.set_ylabel('[$m/s^2$]')
    ax2.plot(streem_acc_from_acc[2].times("matplotlib"), I_ar_Z_nor, "r-",label='Arias Intensity')
    ax2.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g',label='Start-time')
    ax2.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='b',label='End-time')
    ax2.axvline(t5_interp,c='m', linestyle='--',label='T5%')
    ax2.axvline(t95_interp,c='m', linestyle='--',label='T95%')
    ax2.set_xlabel('time (UTC)')
    ax2.set_ylabel('Arias Intensity')
    ax2.legend(loc='best')
    ax2.xaxis_date()
    fig.autofmt_xdate()
    fig.tight_layout()    
    fig.savefig(FigName, dpi = 300)
    fig.clear()
    plt.close(fig)
    return 0



def sta_lta_arias_subplot(streem_acc_from_acc,trig_test_z,I_ar_Z_nor,Ptime,Etime,FigName,Ftitle):
    t5_interp = np.interp(0.05,I_ar_Z_nor,streem_acc_from_acc[2].times("matplotlib"))
    t95_interp = np.interp(0.95,I_ar_Z_nor,streem_acc_from_acc[2].times("matplotlib"))
    fig,(ax1,ax2,ax3) = plt.subplots(3,1, figsize= (10,10))
    fig.suptitle(Ftitle)
    ax1.plot(streem_acc_from_acc[2].times("matplotlib"), streem_acc_from_acc[2].data, "k-",label=str(streem_acc_from_acc[2].stats.network+'.'+\
    streem_acc_from_acc[2].stats.station+'.'+streem_acc_from_acc[2].stats.location+'.'+streem_acc_from_acc[2].stats.channel))
    ax1.axvline(t5_interp,c='m', linestyle='--')
    ax1.axvline(t95_interp,c='m', linestyle='--')
    ax1.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g')
    ax1.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='b')
    leg = ax1.legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)
    ax1.xaxis_date()
    ax1.set_ylabel('[$m/s^2$]')
    ax2.plot(trig_test_z.times("matplotlib"), trig_test_z.data, "k-",label=str(trig_test_z.stats.network+'.'+trig_test_z.stats.station+'.'+trig_test_z.stats.location+'.'+trig_test_z.stats.channel))
    ax2.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g')
    ax2.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='b')
    ax2.axvline(t5_interp,c='m', linestyle='--')
    ax2.axvline(t95_interp,c='m', linestyle='--')
    leg = ax2.legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg.legendHandles:
        item.set_visible(False)
    ax2.set_ylabel('STA/LTA')
    ax2.xaxis_date()
    ax3.plot(streem_acc_from_acc[2].times("matplotlib"), I_ar_Z_nor, "r-",label='Arias Intensity')
    ax3.axvline(x=dates.date2num(Ptime.datetime),linewidth=2,c='g',label='Start-time')
    ax3.axvline(x=dates.date2num(Etime.datetime),linewidth=2,c='b',label='End-time')
    ax3.axvline(t5_interp,c='m', linestyle='--',label='T5%')
    ax3.axvline(t95_interp,c='m', linestyle='--',label='T95%')
    ax3.set_xlabel('time (UTC)')
    ax3.set_ylabel('Normalized Arias Intensity')
    ax3.legend(loc='best')
    ax3.xaxis_date()
    fig.autofmt_xdate()
    fig.tight_layout()
    fig.savefig(FigName, dpi = 300)
    fig.clear()
    plt.close(fig)
    return 0











def clipp_image(streem_vel,clip, pgcounts,a, FigName,Ftitle):

    pgcounts_equiv_Z = a[2]
    pgcounts_equiv_N = a[1]
    pgcounts_equiv_E = a[0]
    
      
    fig,(ax1,ax2,ax3) = plt.subplots(3,1, figsize= (10,10))
    fig.suptitle(Ftitle)
    ax1.plot(streem_vel[2].times("matplotlib"), streem_vel[2].data, "k-",label=str(streem_vel[2].stats.network+'.'+\
    streem_vel[2].stats.station+'.'+streem_vel[2].stats.location+'.'+streem_vel[2].stats.channel))
    leg1_ax1 = ax1.legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg1_ax1.legendHandles:
        item.set_visible(False)
    ax1.xaxis_date()
    line_counts = ax1.axhline(pgcounts[2],c='m', linestyle='--',label = 'counts_interval')
    ax1.axhline(pgcounts_equiv_Z, c='m', linestyle='--')
    ax1.set_ylabel('counts')
    ax1.set_xlabel('time [UTC]')
    line_clip = ax1.axhline(clip,c='r', linestyle='--',label= 'clip')
    ax1.axhline(-clip,c='r', linestyle='--')
    # Crea la seconda legenda per le linee orizzontali con specifica delle linestyle e colori
    leg2 = ax1.legend([line_counts, line_clip], ['counts_interval', 'clip interval'], loc='upper right')
    # Aggiungi entrambe le legende al grafico
    ax1.add_artist(leg1_ax1)
    ax2.plot(streem_vel[1].times("matplotlib"), streem_vel[1].data, "k-",label=str(streem_vel[1].stats.network+'.'+\
    streem_vel[1].stats.station+'.'+streem_vel[1].stats.location+'.'+streem_vel[1].stats.channel))
    leg1_ax2 = ax2.legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True)
    for item in leg1_ax2.legendHandles:
        item.set_visible(False) 
    ax2.xaxis_date() 
    line_counts_ax2 = ax2.axhline(pgcounts[1],c='m', linestyle='--',label = 'counts_interval') 
    ax2.axhline(pgcounts_equiv_N, c='m', linestyle='--') 
    ax2.set_ylabel('counts') 
    ax2.set_xlabel('time [UTC]') 
    line_clip_ax2 = ax2.axhline(clip,c='r', linestyle='--',label= 'clip')
    ax2.axhline(-clip,c='r', linestyle='--') 
    leg2_ax2 = ax2.legend([line_counts_ax2, line_clip_ax2], ['counts_interval', 'clip interval'], loc='upper right')
    ax2.add_artist(leg1_ax2)
    ax3.plot(streem_vel[0].times("matplotlib"), streem_vel[0].data, "k-",label=str(streem_vel[0].stats.network+'.'+\
    streem_vel[0].stats.station+'.'+streem_vel[0].stats.location+'.'+streem_vel[0].stats.channel)) 
    leg1_ax3 = ax3.legend(loc='upper left',handlelength=0, handletextpad=0, fancybox=True) 
    for item in leg1_ax3.legendHandles: 
        item.set_visible(False) 
    ax3.xaxis_date() 
    line_counts_ax3 = ax3.axhline(pgcounts[0],c='m', linestyle='--',label = 'counts_interval') 
    ax3.axhline(pgcounts_equiv_E, c='m', linestyle='--')
    ax3.set_ylabel('counts')    
    ax3.set_xlabel('time [UTC]')      
    line_clip_ax3 = ax3.axhline(clip,c='r', linestyle='--',label= 'clip')        
    ax3.axhline(-clip,c='r', linestyle='--')       
    leg3_ax3 = ax3.legend([line_counts_ax3, line_clip_ax3], ['counts_interval', 'clip interval'], loc='upper right')
    ax3.add_artist(leg1_ax3)
    fig.autofmt_xdate()        
    fig.tight_layout()
    fig.savefig(FigName, dpi = 300)
    fig.clear()
    plt.close(fig)
    
    
    
    
    
    
    return 0 

import matplotlib
import matplotlib.pylab as plt
import numpy as np
matplotlib.use('Agg')

def summary_plot(table):
    
    Pga_AA_E=table[(table['Channel']=='E')]['PGA_AA'].to_numpy()
    Pga_AA_N=table[(table['Channel']=='N')]['PGA_AA'].to_numpy()
    Pga_AA_Z=table[(table['Channel']=='Z')]['PGA_AA'].to_numpy()
    
    Pga_AV_E=table[(table['Channel']=='E')]['PGA_AV'].to_numpy()
    Pga_AV_N=table[(table['Channel']=='N')]['PGA_AV'].to_numpy()
    Pga_AV_Z=table[(table['Channel']=='Z')]['PGA_AV'].to_numpy()
    
    Pga_AA_f_E=table[(table['Channel']=='E')]['PGA_AA_F'].to_numpy()
    Pga_AA_f_N=table[(table['Channel']=='N')]['PGA_AA_F'].to_numpy()
    Pga_AA_f_Z=table[(table['Channel']=='Z')]['PGA_AA_F'].to_numpy()
    
    Pga_AV_f_E=table[(table['Channel']=='E')]['PGA_AV_F'].to_numpy()
    Pga_AV_f_N=table[(table['Channel']=='N')]['PGA_AV_F'].to_numpy()
    Pga_AV_f_Z=table[(table['Channel']=='Z')]['PGA_AV_F'].to_numpy()
    
    Pga_ratio_E=table[(table['Channel']=='E')]['Ratio_PGA_AA_AV'].to_numpy()
    Pga_ratio_N=table[(table['Channel']=='N')]['Ratio_PGA_AA_AV'].to_numpy()
    Pga_ratio_Z=table[(table['Channel']=='Z')]['Ratio_PGA_AA_AV'].to_numpy()
    
    Pga_ratio_f_E=table[(table['Channel']=='E')]['Ratio_PGA_AAF_AVF'].to_numpy()
    Pga_ratio_f_N=table[(table['Channel']=='N')]['Ratio_PGA_AAF_AVF'].to_numpy()
    Pga_ratio_f_Z=table[(table['Channel']=='Z')]['Ratio_PGA_AAF_AVF'].to_numpy()

    Pgv_ratio_E=table[(table['Channel']=='E')]['Ratio_PGV_VV_VA'].to_numpy()
    Pgv_ratio_N=table[(table['Channel']=='N')]['Ratio_PGV_VV_VA'].to_numpy()
    Pgv_ratio_Z=table[(table['Channel']=='Z')]['Ratio_PGV_VV_VA'].to_numpy()
    
    Pgv_ratio_f_E=table[(table['Channel']=='E')]['Ratio_PGV_VVF_VAF'].to_numpy()
    Pgv_ratio_f_N=table[(table['Channel']=='N')]['Ratio_PGV_VVF_VAF'].to_numpy()
    Pgv_ratio_f_Z=table[(table['Channel']=='Z')]['Ratio_PGV_VVF_VAF'].to_numpy()
    
    N_Pga_ratio_E=table[(table['Channel']=='E')]['Norm_Ratio_PGA_AA_AV'].to_numpy()
    N_Pga_ratio_N=table[(table['Channel']=='N')]['Norm_Ratio_PGA_AA_AV'].to_numpy()
    N_Pga_ratio_Z=table[(table['Channel']=='Z')]['Norm_Ratio_PGA_AA_AV'].to_numpy()
    
    N_Pga_ratio_f_E=table[(table['Channel']=='E')]['Norm_Ratio_PGA_AAF_AVF'].to_numpy()
    N_Pga_ratio_f_N=table[(table['Channel']=='N')]['Norm_Ratio_PGA_AAF_AVF'].to_numpy()
    N_Pga_ratio_f_Z=table[(table['Channel']=='Z')]['Norm_Ratio_PGA_AAF_AVF'].to_numpy()
    
    Pgv_VV_E=table[(table['Channel']=='E')]['PGV_VV'].to_numpy()
    Pgv_VV_N=table[(table['Channel']=='N')]['PGV_VV'].to_numpy()
    Pgv_VV_Z=table[(table['Channel']=='Z')]['PGV_VV'].to_numpy()
    
    Pgv_VA_E=table[(table['Channel']=='E')]['PGV_VA'].to_numpy()
    Pgv_VA_N=table[(table['Channel']=='N')]['PGV_VA'].to_numpy()
    Pgv_VA_Z=table[(table['Channel']=='Z')]['PGV_VA'].to_numpy()
    
    Pgv_VV_f_E=table[(table['Channel']=='E')]['PGV_VV_F'].to_numpy()
    Pgv_VV_f_N=table[(table['Channel']=='N')]['PGV_VV_F'].to_numpy()
    Pgv_VV_f_Z=table[(table['Channel']=='Z')]['PGV_VV_F'].to_numpy()
        
    Pgv_VA_f_E=table[(table['Channel']=='E')]['PGV_VA_F'].to_numpy()
    Pgv_VA_f_N=table[(table['Channel']=='N')]['PGV_VA_F'].to_numpy()
    Pgv_VA_f_Z=table[(table['Channel']=='Z')]['PGV_VA_F'].to_numpy()
    
    N_Pgv_ratio_E=table[(table['Channel']=='E')]['Norm_Ratio_PGV_VV_VA'].to_numpy()
    N_Pgv_ratio_N=table[(table['Channel']=='N')]['Norm_Ratio_PGV_VV_VA'].to_numpy()
    N_Pgv_ratio_Z=table[(table['Channel']=='Z')]['Norm_Ratio_PGV_VV_VA'].to_numpy()
    
    N_Pgv_ratio_f_E=table[(table['Channel']=='E')]['Norm_Ratio_PGV_VVF_VAF'].to_numpy()
    N_Pgv_ratio_f_N=table[(table['Channel']=='N')]['Norm_Ratio_PGV_VVF_VAF'].to_numpy()
    N_Pgv_ratio_f_Z=table[(table['Channel']=='Z')]['Norm_Ratio_PGV_VVF_VAF'].to_numpy()
    
    cross_corr_E = table[(table['Channel']=='E')]['CC'].to_numpy()
    cross_corr_N = table[(table['Channel']=='N')]['CC'].to_numpy()
    cross_corr_Z = table[(table['Channel']=='Z')]['CC'].to_numpy()
    
    cross_corr_E_F = table[(table['Channel']=='E')]['CCF'].to_numpy()
    cross_corr_N_F = table[(table['Channel']=='N')]['CCF'].to_numpy()
    cross_corr_Z_F = table[(table['Channel']=='Z')]['CCF'].to_numpy()
    
    Vec_cc_nrpga_e   = table[(table['Channel']=='E')] ['Prod_CC_Norm_Ratio_PGA_AA_AV']
    Vec_cc_nrpga_n   = table[(table['Channel']=='N')] ['Prod_CC_Norm_Ratio_PGA_AA_AV']
    Vec_cc_nrpga_z   = table[(table['Channel']=='Z')] ['Prod_CC_Norm_Ratio_PGA_AA_AV']
    
    Vec_ccf_nrpga_e_f = table[(table['Channel']=='E')] ['Prod_CCF_Norm_Ratio_PGA_AAF_AVF']
    Vec_ccf_nrpga_n_f = table[(table['Channel']=='N')] ['Prod_CCF_Norm_Ratio_PGA_AAF_AVF']
    Vec_ccf_nrpga_z_f = table[(table['Channel']=='Z')] ['Prod_CCF_Norm_Ratio_PGA_AAF_AVF']
    
    Vec_cc_nrpgv_e   = table[(table['Channel']=='E')] ['Prod_CC_Norm_Ratio_PGV_VV_VA']
    Vec_cc_nrpgv_n   = table[(table['Channel']=='N')] ['Prod_CC_Norm_Ratio_PGV_VV_VA']
    Vec_cc_nrpgv_z   = table[(table['Channel']=='Z')] ['Prod_CC_Norm_Ratio_PGV_VV_VA']
    
    Vec_ccf_nrpgv_e_f = table[(table['Channel']=='E')] ['Prod_CCF_Norm_Ratio_PGV_VVF_VAF']
    Vec_ccf_nrpgv_n_f = table[(table['Channel']=='N')] ['Prod_CCF_Norm_Ratio_PGV_VVF_VAF']
    Vec_ccf_nrpgv_z_f = table[(table['Channel']=='Z')] ['Prod_CCF_Norm_Ratio_PGV_VVF_VAF']    
    

    pga_aa       = {'e':Pga_AA_E       ,'n':Pga_AA_N        ,'z':Pga_AA_Z}
    pga_av       = {'e':Pga_AV_E       ,'n':Pga_AV_N        ,'z':Pga_AV_Z}
    pga_aa_f     = {'e':Pga_AA_f_E     ,'n':Pga_AA_f_N      ,'z':Pga_AA_f_Z}
    pga_av_f     = {'e':Pga_AV_f_E     ,'n':Pga_AV_f_N      ,'z':Pga_AV_f_Z}
    
    
    ratiopga    = {'e':Pga_ratio_E  ,'n':Pga_ratio_N   ,'z':Pga_ratio_Z}
    ratiopga_f  = {'e':Pga_ratio_f_E,'n':Pga_ratio_f_N ,'z':Pga_ratio_f_Z}
    
    Nratiopga    = {'e':N_Pga_ratio_E  ,'n':N_Pga_ratio_N   ,'z':N_Pga_ratio_Z}
    Nratiopga_f  = {'e':N_Pga_ratio_f_E,'n':N_Pga_ratio_f_N ,'z':N_Pga_ratio_f_Z}
    
    pgv_vv       = {'e':Pgv_VV_E        ,'n':Pgv_VV_N        ,'z':Pgv_VV_Z}
    pgv_va       = {'e':Pgv_VA_E        ,'n':Pgv_VA_N        ,'z':Pgv_VA_Z}
    pgv_vv_f     = {'e':Pgv_VV_f_E      ,'n':Pgv_VV_f_N      ,'z':Pgv_VV_f_Z}
    pgv_va_f     = {'e':Pgv_VA_f_E      ,'n':Pgv_VA_f_N      ,'z':Pgv_VA_f_Z}
    
    ratiopgv     = {'e':Pgv_ratio_E     ,'n':Pgv_ratio_N     ,'z':Pgv_ratio_Z}   
    ratiopgv_f   = {'e':Pgv_ratio_f_E   ,'n':Pgv_ratio_f_N   ,'z':Pgv_ratio_f_Z}
    
    Nratiopgv    = {'e':N_Pgv_ratio_E   ,'n':N_Pgv_ratio_N   ,'z':N_Pgv_ratio_Z}
    Nratiopgv_f  = {'e':N_Pgv_ratio_f_E ,'n':N_Pgv_ratio_f_N ,'z':N_Pgv_ratio_f_Z}
    
    epidist     = table[(table['Channel']=='E')]['Repi'].to_numpy()
    mag         = table[(table['Channel']=='E')]['Mag'].to_numpy()
    
    prod_cc_rpga    = {'e': abs(Vec_cc_nrpga_e)     ,'n': abs(Vec_cc_nrpga_n   )  ,'z': abs(Vec_cc_nrpga_z)}
    prod_ccf_rpga_f = {'e': abs(Vec_ccf_nrpga_e_f)  ,'n': abs(Vec_ccf_nrpga_n_f)  ,'z': abs(Vec_ccf_nrpga_z_f)}
    
    prod_cc_rpgv    = {'e': abs(Vec_cc_nrpgv_e   ) ,'n': abs(Vec_cc_nrpgv_n   )  ,'z': abs(Vec_cc_nrpgv_z)}
    prod_ccf_rpgv_f = {'e': abs(Vec_ccf_nrpgv_e_f) ,'n': abs(Vec_ccf_nrpgv_n_f)  ,'z': abs(Vec_ccf_nrpgv_z_f)}
    

    min_value_ratiopga = (min([10**np.floor(np.log10(min((min(ratiopga_f[key]) for key in ratiopga_f)))), 10**np.floor(np.log10(min((min(ratiopga[key]) for key in ratiopga))))]))/10
    max_value_ratiopga = (max([10**np.ceil(np.log10(max((max(ratiopga_f[key]) for key in ratiopga_f)))), 10**np.ceil(np.log10(max((max(ratiopga[key]) for key in ratiopga))))]))*5
    
    min_value_ratiopgv = min([10**np.floor(np.log10(min((min(ratiopgv_f[key]) for key in ratiopgv_f)))), 10**np.floor(np.log10(min((min(ratiopgv[key]) for key in ratiopgv))))])/100
    max_value_ratiopgv = (max([10**np.ceil(np.log10(max((max(ratiopgv_f[key]) for key in ratiopgv_f)))), 10**np.ceil(np.log10(max((max(ratiopgv[key]) for key in ratiopgv))))]))*5
    
    min_value_prod_cc_rpga = min([10**np.floor(np.log10(min((min(prod_ccf_rpga_f[key]) for key in prod_ccf_rpga_f)))), 10**np.floor(np.log10(min((min(prod_cc_rpga[key]) for key in prod_cc_rpga))))])
    min_value_prod_cc_rpgv = (min([10**np.floor(np.log10(min((min(prod_ccf_rpgv_f[key]) for key in prod_ccf_rpgv_f)))), 10**np.floor(np.log10(min((min(prod_cc_rpgv[key]) for key in prod_cc_rpgv))))]))/10
  
    # ~ min_value_epidist=min([10**np.floor(np.log10(min((min(epidist[key]) for key in epidist))))])
    
    
    
    top_norm = 1.5  
    # ~ bottom_epidist = None
    bottom_epidist = (10**np.floor(np.log10(min(epidist))))-10
    top_epidist = max(epidist)+10
    # ~ top_epidist = 10**np.ceil(np.log10(max(epidist)))
    

    fig1, axs = plt.subplots(2, 2,figsize=(8.25,8.25))

    xlim1 = 1e-6
    xlim2 = 10
    ylim1 = 1e-6
    ylim2 = 10

    axs[0,0].loglog(np.abs(pga_aa.get('e')), np.abs(pga_av.get('e')),'Pr', markersize=10,markeredgecolor='black',label='E') #E                             
    axs[0,0].loglog(np.abs(pga_aa.get('n')), np.abs(pga_av.get('n')),'^b', markersize=10,markeredgecolor='black',label='N') #N
    axs[0,0].loglog(np.abs(pga_aa.get('z')), np.abs(pga_av.get('z')),'og', markersize=10,markeredgecolor='black',label='Z') #Z
    axs[0,0].plot([xlim1, xlim2],[ylim1, ylim2],'-k', linewidth = 2)
    axs[0,0].set_xlabel('PGA from AA [$m/s^2$]')
    axs[0,0].set_ylabel('PGA from AV [$m/s^2$]')
    axs[0,0].set_ylim([ylim1, ylim2])
    axs[0,0].set_xlim([xlim1, xlim2])
    axs[0,0].set_aspect('equal')
    axs[0,0].grid(True, which='both')
    axs[0,0].legend(loc='best')
    axs[0,1].loglog(np.abs(pga_aa_f.get('e')), np.abs(pga_av_f.get('e')),'Pr', markersize=10,markeredgecolor='black',label='E')
    axs[0,1].loglog(np.abs(pga_aa_f.get('n')), np.abs(pga_av_f.get('n')),'^b', markersize=10,markeredgecolor='black',label='N')
    axs[0,1].loglog(np.abs(pga_aa_f.get('z')), np.abs(pga_av_f.get('z')),'og', markersize=10,markeredgecolor='black',label='Z')
    axs[0,1].plot([xlim1, xlim2],[ylim1, ylim2],'-k', linewidth = 2)
    axs[0,1].set_xlabel('PGA$f$ from AA [$m/s^2$]')
    axs[0,1].set_ylabel('PGA$f$ from AV [$m/s^2$]')
    axs[0,1].set_ylim([ylim1, ylim2])
    axs[0,1].set_xlim([xlim1, xlim2])
    axs[0,1].set_aspect('equal')
    axs[0,1].grid(True, which='both')
    # ~ axs[0,1].legend(loc='best')
    axs[1,0].loglog(np.abs(pga_aa.get('e')), ratiopga.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E') #E
    axs[1,0].loglog(np.abs(pga_aa.get('n')), ratiopga.get('n'),'^b', markersize=10,markeredgecolor='black',label='N') #N
    axs[1,0].loglog(np.abs(pga_aa.get('z')), ratiopga.get('z'),'og', markersize=10,markeredgecolor='black',label='Z') #Z
    axs[1,0].plot([xlim1, xlim2],[1, 1],'-k', linewidth = 2)
    axs[1,0].set_xlabel('PGA from AA [$m/s^2$]')
    axs[1,0].set_ylabel('RPGA')
    # ~ axs[1,0].set_ylim(bottom=10**np.floor(np.log10(min((min(ratiopga[key]) for key in ratiopga)))),top = 10**np.ceil(np.log10(max((max(ratiopga[key]) for key in ratiopga)))))
    axs[1,0].set_ylim(bottom=min_value_ratiopga,top = max_value_ratiopga)
    axs[1,0].set_xlim([xlim1, xlim2])
    axs[1,0].grid(True, which='both')
    # ~ axs[1,0].legend(loc='best')
    axs[1,1].loglog(np.abs(pga_aa_f.get('e')), ratiopga_f.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E') #E
    axs[1,1].loglog(np.abs(pga_aa_f.get('n')), ratiopga_f.get('n'),'^b', markersize=10,markeredgecolor='black',label='N') #N
    axs[1,1].loglog(np.abs(pga_aa_f.get('z')), ratiopga_f.get('z'),'og', markersize=10,markeredgecolor='black',label='Z') #Z
    axs[1,1].plot([xlim1, xlim2],[1, 1],'-k', linewidth = 2)
    axs[1,1].set_xlabel('PGA$f$ from AA [$m/s^2$]')
    axs[1,1].set_ylabel('RPGA$f$')
    # ~ axs[1,1].set_ylim([ylim1, ylim2])
    axs[1,1].set_ylim(bottom=min_value_ratiopga,top = max_value_ratiopga)
    axs[1,1].set_xlim([xlim1, xlim2])
    # ~ axs[1,1].legend(loc='best')
    # ~ axs[1,1].set_aspect('equal')
    axs[1,1].grid(True, which='both')
    fig1.tight_layout()
    


    # Figure 2


    fig2, axs = plt.subplots(2, 2,figsize=(8.25,8.25))

    xlim1 = 1e-7
    xlim2 = 1
    ylim1 = 1e-7
    ylim2 = 1

    axs[0,0].loglog(np.abs(pgv_vv.get('e')), np.abs(pgv_va.get('e')),'Pr', markersize=10,markeredgecolor='black',label='E') #E
    axs[0,0].loglog(np.abs(pgv_vv.get('n')), np.abs(pgv_va.get('n')),'^b', markersize=10,markeredgecolor='black',label='N') #N
    axs[0,0].loglog(np.abs(pgv_vv.get('z')), np.abs(pgv_va.get('z')),'og', markersize=10,markeredgecolor='black',label='Z') #Z
    axs[0,0].plot([xlim1, xlim2],[ylim1, ylim2],'-k', linewidth = 2)
    axs[0,0].set_xlabel('PGV from VV [m/s]')
    axs[0,0].set_ylabel('PGV from VA [m/s]')
    axs[0,0].set_ylim([ylim1, ylim2])
    axs[0,0].set_xlim([xlim1, xlim2])
    axs[0,0].set_aspect('equal')
    axs[0,0].grid(True, which='both')
    axs[0,0].legend(loc='best')
    axs[0,1].loglog(np.abs(pgv_vv_f.get('e')), np.abs(pgv_va_f.get('e')),'Pr', markersize=10,markeredgecolor='black',label='E')
    axs[0,1].loglog(np.abs(pgv_vv_f.get('n')), np.abs(pgv_va_f.get('n')),'^b', markersize=10,markeredgecolor='black',label='N')
    axs[0,1].loglog(np.abs(pgv_vv_f.get('z')), np.abs(pgv_va_f.get('z')),'og', markersize=10,markeredgecolor='black',label='Z')
    axs[0,1].plot([xlim1, xlim2],[ylim1, ylim2],'-k', linewidth = 2)
    axs[0,1].set_xlabel('PGV$f$ from VV [m/s]')
    axs[0,1].set_ylabel('PGV$f$ from VA [m/s]')
    axs[0,1].set_ylim([ylim1, ylim2])
    axs[0,1].set_xlim([xlim1, xlim2])
    axs[0,1].set_aspect('equal')
    axs[0,1].grid(True, which='both')
    # ~ axs[0,1].legend(loc='best')

    # ~ xlim1 = 1e-5
    # ~ xlim2 = 1
    # ~ ylim1 = 1e-5
    # ~ ylim2 = 1

    axs[1,0].loglog(np.abs(pgv_vv.get('e')), ratiopgv.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E') #E
    axs[1,0].loglog(np.abs(pgv_vv.get('n')), ratiopgv.get('n'),'^b', markersize=10,markeredgecolor='black',label='N') #N
    axs[1,0].loglog(np.abs(pgv_vv.get('z')), ratiopgv.get('z'),'og', markersize=10,markeredgecolor='black',label='Z') #Z
    axs[1,0].plot([xlim1, xlim2],[1, 1],'-k', linewidth = 2)
    axs[1,0].set_xlabel('PGV from VV [m/s]')
    axs[1,0].set_ylabel('RPGV')
    axs[1,0].set_ylim(bottom=min_value_ratiopgv,top = max_value_ratiopgv)
    axs[1,0].set_xlim([xlim1, xlim2])
    # ~ axs[1,0].legend(loc='best')
    # ~ axs[1,0].set_aspect('equal')
    axs[1,0].grid(True, which='both')
    axs[1,1].loglog(np.abs(pgv_vv_f.get('e')), ratiopgv_f.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E') #E
    axs[1,1].loglog(np.abs(pgv_vv_f.get('n')), ratiopgv_f.get('n'),'^b', markersize=10,markeredgecolor='black',label='N') #N
    axs[1,1].loglog(np.abs(pgv_vv_f.get('z')), ratiopgv_f.get('z'),'og', markersize=10,markeredgecolor='black',label='Z') #Z
    axs[1,1].plot([xlim1, xlim2],[1, 1],'-k', linewidth = 2)
    axs[1,1].set_xlabel('PGV$f$ from VV [m/s]')
    axs[1,1].set_ylabel('RPGV$f$')
    axs[1,1].set_ylim(bottom=min_value_ratiopgv,top = max_value_ratiopgv)
    axs[1,1].set_xlim([xlim1, xlim2])
    # ~ axs[1,1].legend(loc='best')
    # ~ axs[1,1].set_aspect('equal')
    axs[1,1].grid(True, which='both')
    fig2.tight_layout()

    fig3, axs = plt.subplots(2, 2,figsize=(8,8))
    axs[0,0].semilogy(epidist,ratiopga.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E')
    axs[0,0].semilogy(epidist,ratiopga.get('n'),'^b', markersize=10,markeredgecolor='black',label='N')
    axs[0,0].semilogy(epidist,ratiopga.get('z'),'og', markersize=10,markeredgecolor='black',label='Z')
    axs[0,0].axhline(1,linewidth=2,c='k')
    axs[0,0].set_xlabel('Repi [$Km$]')
    axs[0,0].set_ylabel('RPGA')
    axs[0,0].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,0].set_ylim(bottom=min_value_ratiopga,top = max_value_ratiopga)
    axs[0,0].grid(True, which='both')
    axs[1,0].semilogy(mag,ratiopga.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,ratiopga.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,ratiopga.get('z'),'og', markersize=10,markeredgecolor='black')
    axs[1,0].axhline(1,linewidth=2,c='k')
    axs[1,0].set_xlabel('Mag')
    axs[1,0].set_ylabel('RPGA')
    axs[1,0].set_ylim(bottom=min_value_ratiopga,top = max_value_ratiopga)
    axs[1,0].set_xlim(left=2,right=8)
    axs[1,0].grid(True, which='both')
    axs[0,1].semilogy(epidist,ratiopga_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,ratiopga_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,ratiopga_f.get('z'),'og', markersize=10,markeredgecolor='black')
    axs[0,1].axhline(1,linewidth=2,c='k')
    axs[0,1].set_xlabel('Repi [$Km$]')
    axs[0,1].set_ylabel('RPGA$f$')
    axs[0,1].set_ylim(bottom=min_value_ratiopga,top = max_value_ratiopga)
    axs[0,1].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,1].grid(True, which='both')
    axs[1,1].semilogy(mag,ratiopga_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,ratiopga_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,ratiopga_f.get('z'),'og', markersize=10,markeredgecolor='black')
    axs[1,1].axhline(1,linewidth=2,c='k')
    axs[1,1].set_xlabel('Mag')
    axs[1,1].set_ylabel('RPGA$f$')
    axs[1,1].set_ylim(bottom=min_value_ratiopga,top = max_value_ratiopga)
    axs[1,1].set_xlim(left=2,right=8)
    axs[1,1].grid(True, which='both')
    fig3.legend(ncol=3,loc='lower center', bbox_to_anchor=(0.5, 0.0))
    fig3.tight_layout()

    fig4, axs = plt.subplots(2, 2,figsize=(8,8))
    axs[0,0].semilogy(epidist,ratiopgv.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E')
    axs[0,0].semilogy(epidist,ratiopgv.get('n'),'^b', markersize=10,markeredgecolor='black',label='N')
    axs[0,0].semilogy(epidist,ratiopgv.get('z'),'og', markersize=10,markeredgecolor='black',label='Z')
    axs[0,0].axhline(1,linewidth=2,c='k')
    axs[0,0].set_xlabel('Repi [$Km$]')
    axs[0,0].set_ylabel('RPGV')
    axs[0,0].set_ylim(bottom=min_value_ratiopgv,top = max_value_ratiopgv)
    axs[0,0].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,0].grid(True, which='both')
    axs[1,0].semilogy(mag,ratiopgv.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,ratiopgv.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,ratiopgv.get('z'),'og', markersize=10,markeredgecolor='black')
    # ~ axs[1,0].legend(loc='upper left')
    axs[1,0].axhline(1,linewidth=2,c='k')
    axs[1,0].set_xlabel('Mag')
    axs[1,0].set_ylabel('RPGV')
    axs[1,0].set_xlim(left=2,right=8)
    axs[1,0].set_ylim(bottom=min_value_ratiopgv,top = max_value_ratiopgv)
    axs[1,0].grid(True, which='both')
    # ~ axs[1,0].legend(loc='upper left')
    axs[0,1].semilogy(epidist,ratiopgv_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,ratiopgv_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,ratiopgv_f.get('z'),'og', markersize=10,markeredgecolor='black')
    # ~ axs[0,1].legend(loc='upper left')
    axs[0,1].axhline(1,linewidth=2,c='k')
    axs[0,1].set_xlabel('Repi [$Km$]')
    axs[0,1].set_ylabel('RPGV$f$')
    axs[0,1].set_ylim(bottom=min_value_ratiopgv,top = max_value_ratiopgv)
    axs[0,1].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,1].grid(True, which='both')
    axs[1,1].semilogy(mag,ratiopgv_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,ratiopgv_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,ratiopgv_f.get('z'),'og', markersize=10,markeredgecolor='black')
    axs[1,1].axhline(1,linewidth=2,c='k')
    # ~ axs[1,1].legend(loc='upper left')
    axs[1,1].set_xlabel('Mag')
    axs[1,1].set_ylabel('RPGV$f$')
    axs[1,1].set_xlim(left=2,right=8)
    axs[1,1].set_ylim(bottom=min_value_ratiopgv,top = max_value_ratiopgv)
    axs[1,1].grid(True, which='both')
    fig4.legend(ncol=3,loc='lower center', bbox_to_anchor=(0.5, 0.0))
    fig4.tight_layout()
    
    fig5, axs = plt.subplots(2, 2,figsize=(8,8))
    axs[0,0].semilogy(epidist,prod_cc_rpga.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E')
    axs[0,0].semilogy(epidist,prod_cc_rpga.get('n'),'^b', markersize=10,markeredgecolor='black',label='N')
    axs[0,0].semilogy(epidist,prod_cc_rpga.get('z'),'og', markersize=10,markeredgecolor='black',label='Z')
    # ~ axs[0,0].legend(loc='upper left')
    axs[0,0].axhline(1,linewidth=2,c='k')
    axs[0,0].set_xlabel('Repi [$Km$]')
    axs[0,0].set_ylabel('CC*NRPGA')
    axs[0,0].set_ylim(bottom=min_value_prod_cc_rpga,top= top_norm)
    axs[0,0].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,0].grid(True, which='both')
    axs[1,0].semilogy(mag,prod_cc_rpga.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,prod_cc_rpga.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,prod_cc_rpga.get('z'),'og', markersize=10,markeredgecolor='black')
    # ~ axs[1,0].legend(loc='upper left')
    axs[1,0].axhline(1,linewidth=2,c='k')
    axs[1,0].set_xlabel('Mag')
    axs[1,0].set_ylabel('CC*NRPGA')
    axs[1,0].grid(True, which='both')
    # ~ axs[1,0].legend(loc='upper left')
    axs[1,0].set_xlim(left=2,right=8)
    axs[1,0].set_ylim(bottom=min_value_prod_cc_rpga,top=top_norm)
    axs[0,1].semilogy(epidist,prod_ccf_rpga_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,prod_ccf_rpga_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,prod_ccf_rpga_f.get('z'),'og', markersize=10,markeredgecolor='black')
    # ~ axs[0,1].legend(loc='upper left')
    axs[0,1].axhline(1,linewidth=2,c='k')
    axs[0,1].set_xlabel('Repi [$Km$]')
    axs[0,1].set_ylabel('CC$f$*NRPGA$f$')
    axs[0,1].set_ylim(bottom=min_value_prod_cc_rpga,top=top_norm)
    axs[0,1].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,1].grid(True, which='both')
    axs[1,1].semilogy(mag,prod_ccf_rpga_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,prod_ccf_rpga_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,prod_ccf_rpga_f.get('z'),'og', markersize=10,markeredgecolor='black')
    axs[1,1].axhline(1,linewidth=2,c='k')
    # ~ axs[1,1].legend(loc='upper left')
    axs[1,1].set_xlabel('Mag')
    axs[1,1].set_ylabel('CC$f$*NRPGA$f$')
    axs[1,1].set_xlim(left=2,right=8)
    axs[1,1].set_ylim(bottom=min_value_prod_cc_rpga,top=top_norm)
    axs[1,1].grid(True, which='both')
    fig5.legend(ncol=3,loc='lower center', bbox_to_anchor=(0.5, 0.0))
    fig5.tight_layout()
    
    fig6, axs = plt.subplots(2, 2,figsize=(8,8))
    axs[0,0].semilogy(epidist,prod_cc_rpgv.get('e'),'Pr', markersize=10,markeredgecolor='black',label='E')
    axs[0,0].semilogy(epidist,prod_cc_rpgv.get('n'),'^b', markersize=10,markeredgecolor='black',label='N')
    axs[0,0].semilogy(epidist,prod_cc_rpgv.get('z'),'og', markersize=10,markeredgecolor='black',label='Z')
    # ~ axs[0,0].legend(loc='upper left')
    axs[0,0].axhline(1,linewidth=2,c='k')
    axs[0,0].set_xlabel('Repi [$Km$]')
    axs[0,0].set_ylabel('CC*NRPGV')
    axs[0,0].set_ylim(bottom=min_value_prod_cc_rpgv,top=top_norm)
    axs[0,0].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,0].grid(True, which='both')
    axs[1,0].semilogy(mag,prod_cc_rpgv.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,prod_cc_rpgv.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,0].semilogy(mag,prod_cc_rpgv.get('z'),'og', markersize=10,markeredgecolor='black')
    # ~ axs[1,0].legend(loc='upper left')
    axs[1,0].axhline(1,linewidth=2,c='k')
    axs[1,0].set_xlim(left=2,right=8)
    axs[1,0].set_ylim(bottom=min_value_prod_cc_rpgv,top=top_norm)
    axs[1,0].set_xlabel('Mag')
    axs[1,0].set_ylabel('CC*NRPGV')
    axs[1,0].grid(True, which='both')
    # ~ axs[1,0].legend(loc='upper left')
    axs[0,1].semilogy(epidist,prod_ccf_rpgv_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,prod_ccf_rpgv_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[0,1].semilogy(epidist,prod_ccf_rpgv_f.get('z'),'og', markersize=10,markeredgecolor='black')
    # ~ axs[0,1].legend(loc='upper left')
    axs[0,1].axhline(1,linewidth=2,c='k')
    axs[0,1].set_xlabel('Repi [$Km$]')
    axs[0,1].set_ylabel('CC$f$*NRPGV$f$')
    axs[0,1].set_ylim(bottom=min_value_prod_cc_rpgv,top=top_norm)
    axs[0,1].set_xlim(left=bottom_epidist,right=top_epidist)
    axs[0,1].grid(True, which='both')
    axs[1,1].semilogy(mag,prod_ccf_rpgv_f.get('e'),'Pr', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,prod_ccf_rpgv_f.get('n'),'^b', markersize=10,markeredgecolor='black')
    axs[1,1].semilogy(mag,prod_ccf_rpgv_f.get('z'),'og', markersize=10,markeredgecolor='black')
    axs[1,1].axhline(1,linewidth=2,c='k')
    # ~ axs[1,1].legend(loc='upper left')
    axs[1,1].set_xlabel('Mag')
    axs[1,1].set_ylabel('CC$f$*NRPGV$f$')
    axs[1,1].set_xlim(left=2,right=8)
    axs[1,1].set_ylim(bottom=min_value_prod_cc_rpgv,top=top_norm)
    axs[1,1].grid(True, which='both')
    fig6.legend(ncol=3,loc='lower center', bbox_to_anchor=(0.5, 0.0))
    fig6.tight_layout()



    return fig1, fig2, fig3, fig4, fig5, fig6







#!/usr/bin/python3

import argparse
import matplotlib.pylab as plt
import numpy as np
from sys import argv as sys_argv
from os import mkdir as os_mkdir
from os import remove as os_remove
from os.path import isdir as os_path_isdir
from os.path import isfile as os_path_isfile
from os.path import basename as os_path_basename
import pandas as pd

from summary_plot import summary_plot



synopsis="Il codice genera i summary plot del csv che si ottiene lanciando il cli_multi_event"
epilog_str = 'Written by Fabio Varchetta'
p = argparse.ArgumentParser('summary_def', description=synopsis, epilog=epilog_str)
p.add_argument("-f", "--file", action = "store", type = argparse.FileType('r'), required = True, dest = "opened_file", help = 'Pagellone.csv, proveniente dal cli_multi_event')
# ~ p.add_argument("-e", "--event-id", action = "store", type = int, required = False, dest = "evid", help = 'eventID per filtrare il tabellone')
# ~ p.add_argument("-n", "--network", action = "store", type = str, required = ("-s" or "--station") in sys_argv, dest = "netcode", help = 'Codice network per filtrare il tabellone')
# ~ p.add_argument("-s", "--station", action = "store",type = str, required = ("-n" or "--network") in sys_argv, dest = "stacode", help = 'Codice Station per filtrare il tabellone')
p.add_argument("-a", "--all-station", action = "store_true",  default = False, required = False, dest = "all_netsta", help = 'if option is given, generate summary for all the network-station present in the input table. The options "-e", "-n" and "-s", if given, will be ignored.  Default: False')

opts = p.parse_args()

csv_name = opts.opened_file.name

# # Leggo il pagellone
table=pd.read_csv(csv_name)

if opts.all_netsta:
    net_sta = (table['NETCODE'] + ' ' + table['STACODE']).unique().tolist()
else: net_sta = ['all']

for cur_net_sta in net_sta:
    
    if cur_net_sta != 'all': 
        cur_net_sta_splitted = cur_net_sta.split(' ')
        opts.netcode = cur_net_sta_splitted[0]
        opts.stacode = cur_net_sta_splitted[1]
        print(cur_net_sta)
    else: 
        opts.netcode = ''
        opts.stacode = ''
    
    #if opts.evid: table=table.loc[(table['EVID']==opts.evid)]



    # ~ S='FIAM'
    if opts.stacode:
        cur_table=table.loc[(table['STACODE']==opts.stacode)]
        cur_table=cur_table.loc[(cur_table['NETCODE']==opts.netcode)]
    else:
        cur_table=table
        
    summ_dir='summary'
    if not os_path_isdir(summ_dir): os_mkdir(summ_dir)
    input_summ = summ_dir + '/'
    if opts.stacode:
        input_summ += opts.netcode + "." + opts.stacode + '/'
        if not os_path_isdir(input_summ): os_mkdir(input_summ)
    outfilname = input_summ + os_path_basename(csv_name.replace('.csv',''))
    # ~ if opts.evid: outfilname += '_' + str(opts.evid)
    if opts.netcode: outfilname += '_' + opts.netcode
    if opts.stacode: outfilname += '_' + opts.stacode
    outfilename01 = outfilname + '_01_pga_pgaf_compare_summary.png'
    outfilename02 = outfilname + '_02_pgv_pgvf_compare_summary.png'
    outfilename03 = outfilname + '_03_Rpga_Rpgaf_Repi_Mag_summary.png'
    outfilename04 = outfilname + '_04_Rpgv_Rpgvf_Repi_Mag-summary.png'    
    outfilename05 = outfilname + '_05_CC_Rpga_Rpgaf_Repi_Mag-summary.png'    
    outfilename06 = outfilname + '_06_CC_Rpgv_Rpgvf_Repi_Mag-summary.png'    
    outfilenameCSV = outfilname + '.csv'    
    outfilenameLOG = outfilname + '.log'    

    fig14, fig15, fig16, fig17,fig18, fig19 = summary_plot(cur_table)
    fig14.savefig(outfilename01)
    fig15.savefig(outfilename02)
    fig16.savefig(outfilename03)
    fig17.savefig(outfilename04)
    fig18.savefig(outfilename05)  
    fig19.savefig(outfilename06)
    
    fig14.clear(); plt.close(fig14)
    fig15.clear(); plt.close(fig15)
    fig16.clear(); plt.close(fig16)
    fig17.clear(); plt.close(fig17)
    fig18.clear(); plt.close(fig18)
    fig19.clear(); plt.close(fig19)


    result = cur_table.to_csv(outfilenameCSV) 

    # # Log_file filtered
    Log_file = csv_name.replace('.csv','.log')
    if os_path_isfile(Log_file):
        with open (Log_file) as f: lines = f.readlines()
        f_log = open(outfilenameLOG,'w')
        for line in lines:
            # ~ if opts.evid and not opts.stacode:
                # ~ if '(' + str(opts.evid) in line: 
                    # ~ if ') OK: ' in line: f_log.write(line)
            # ~ elif not opts.evid and opts.stacode:
                # ~ if ', ' + str(opts.netcode) + ', ' + str(opts.stacode) + ') OK: ' in line: f_log.write(line)
            # ~ elif opts.evid and opts.stacode:
                # ~ if '(' + str(opts.evid) + ', ' + str(opts.netcode) + ', ' + str(opts.stacode) + ') OK: ' in line: f_log.write(line)
            if opts.stacode:
                if ', ' + str(opts.netcode) + ', ' + str(opts.stacode) + ') OK: ' in line: f_log.write(line)
            else:
                if ') OK: ' in line: f_log.write(line)
            if './cli_multi_event' in line: f_log.write(line)
        f_log.close()
